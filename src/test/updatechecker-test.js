/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    constants = require('../constants.js'),
    expect = require('expect.js'),
    nock = require('nock'),
    paths = require('../paths.js'),
    safe = require('safetydance'),
    semver = require('semver'),
    settings = require('../settings.js'),
    updatechecker = require('../updatechecker.js');

const UPDATE_VERSION = semver.inc(constants.VERSION, 'major');

describe('updatechecker', function () {
    const { setup, cleanup, app, appstoreToken, mockApiServerOrigin } = common;

    before(setup);
    after(cleanup);

    describe('box', function () {
        before(async function () {
            safe.fs.unlinkSync(paths.UPDATE_CHECKER_FILE);

            await settings.setAutoupdatePattern(constants.AUTOUPDATE_PATTERN_NEVER);
        });

        it('no updates', async function () {
            nock.cleanAll();

            const scope = nock(mockApiServerOrigin)
                .get('/api/v1/boxupdate')
                .query({ boxVersion: constants.VERSION, accessToken: appstoreToken, automatic: false })
                .reply(204, { } );

            await updatechecker.checkForUpdates({ automatic: false });
            expect(updatechecker.getUpdateInfo().box).to.not.be.ok();
            expect(scope.isDone()).to.be.ok();
        });

        it('new version', async function () {
            nock.cleanAll();

            const scope = nock(mockApiServerOrigin)
                .get('/api/v1/boxupdate')
                .query({ boxVersion: constants.VERSION, accessToken: appstoreToken, automatic: false })
                .reply(200, { version: UPDATE_VERSION, changelog: [''], sourceTarballUrl: 'box.tar.gz', sourceTarballSigUrl: 'box.tar.gz.sig', boxVersionsUrl: 'box.versions', boxVersionsSigUrl: 'box.versions.sig' } );

            await updatechecker.checkForUpdates({ automatic: false });
            expect(updatechecker.getUpdateInfo().box.version).to.be(UPDATE_VERSION);
            expect(updatechecker.getUpdateInfo().box.sourceTarballUrl).to.be('box.tar.gz');
            expect(scope.isDone()).to.be.ok();
        });

        it('bad response offers whatever was last valid', async function () {
            nock.cleanAll();

            const scope = nock(mockApiServerOrigin)
                .get('/api/v1/boxupdate')
                .query({ boxVersion: constants.VERSION, accessToken: appstoreToken, automatic: false })
                .reply(404, { version: '2.0.0-pre.0', changelog: [''], sourceTarballUrl: 'box-pre.tar.gz' } );

            await updatechecker.checkForUpdates({ automatic: false });
            expect(updatechecker.getUpdateInfo().box.version).to.be(UPDATE_VERSION);
            expect(updatechecker.getUpdateInfo().box.sourceTarballUrl).to.be('box.tar.gz');
            expect(scope.isDone()).to.be.ok();
        });
    });

    describe('app', function () {
        before(async function () {
            safe.fs.unlinkSync(paths.UPDATE_CHECKER_FILE);

            await settings.setAutoupdatePattern(constants.AUTOUPDATE_PATTERN_NEVER);
        });

        it('no updates', async function () {
            nock.cleanAll();

            const scope = nock(mockApiServerOrigin)
                .get('/api/v1/appupdate')
                .query({ boxVersion: constants.VERSION, accessToken: appstoreToken, appId: app.appStoreId, appVersion: app.manifest.version, automatic: false })
                .reply(204, { } );

            await updatechecker._checkAppUpdates({ automatic: false });
            expect(updatechecker.getUpdateInfo()).to.eql({});
            expect(scope.isDone()).to.be.ok();
        });

        it('bad response', async function () {
            nock.cleanAll();

            const scope = nock(mockApiServerOrigin)
                .get('/api/v1/appupdate')
                .query({ boxVersion: constants.VERSION, accessToken: appstoreToken, appId: app.appStoreId, appVersion: app.manifest.version, automatic: false })
                .reply(500, { update: { manifest: { version: '1.0.0', changelog: '* some changes' } } } );

            await updatechecker._checkAppUpdates({ automatic: false });
            expect(updatechecker.getUpdateInfo()).to.eql({});
            expect(scope.isDone()).to.be.ok();
        });

        it('offers new version', async function () {
            nock.cleanAll();

            const scope = nock(mockApiServerOrigin)
                .get('/api/v1/appupdate')
                .query({ boxVersion: constants.VERSION, accessToken: appstoreToken, appId: app.appStoreId, appVersion: app.manifest.version, automatic: false })
                .reply(200, { manifest: { version: '2.0.0', changelog: '* some changes' } } );

            await updatechecker._checkAppUpdates({ automatic: false });
            expect(updatechecker.getUpdateInfo()).to.eql({ 'appid': { manifest: { version: '2.0.0', changelog: '* some changes' }, unstable: false } });
            expect(scope.isDone()).to.be.ok();
        });

        it('does not offer old version', async function () {
            nock.cleanAll();

            await updatechecker._checkAppUpdates({ automatic: false });
            expect(updatechecker.getUpdateInfo()).to.eql({ });
        });
    });
});
