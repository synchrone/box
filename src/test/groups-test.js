/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    groups = require('../groups.js'),
    safe = require('safetydance');

describe('Groups', function () {
    const { setup, cleanup, admin, user } = common;

    before(setup);
    after(cleanup);

    describe('add/get/del', function () {
        let group0Name = 'administrators', group0Object;
        let group1Name = 'externs', group1Object;

        it('cannot add group - too small', async function () {
            const [error] = await safe(groups.add({ name: '' }));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - too big', async function () {
            const [error] = await safe(groups.add({ name: new Array(256).join('a') }));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - bad name', async function () {
            const [error] = await safe(groups.add({ name: 'bad:name' }));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - reserved', async function () {
            const [error] = await safe(groups.add({ name: 'users' }));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - invalid', async function () {
            const [error] = await safe(groups.add({ name: 'cloudron+admin' }));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('cannot add group - invalid source', async function () {
            const [error] = await safe(groups.add({ name: 'somegroup', source: 'unknownsource' }));
            expect(error.reason).to.be(BoxError.BAD_FIELD);
        });

        it('can add valid groups', async function () {
            let [error, result] = await safe(groups.add({ name: group0Name }));
            expect(error).to.be(null);
            group0Object = result;

            [error, result] = await safe(groups.add({ name: group1Name }));
            expect(error).to.be(null);
            group1Object = result;
        });

        it('cannot add existing group with mixed case', async function () {
            const name = group0Name[0].toUpperCase() + group0Name.substr(1);
            const [error] = await safe(groups.add({ name }));
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
        });

        it('cannot add existing group', async function () {
            const [error] = await safe(groups.add({name: group0Name, source: 'ldap' }));
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
        });

        it('cannot get invalid group', async function () {
            const result = await groups.get('sometrandom');
            expect(result).to.be(null);
        });

        it('can get valid group', async function () {
            const result = await groups.get(group0Object.id);
            expect(result.name).to.equal(group0Name);
        });

        it('isMember returns false', async function () {
            const isMember = await groups.isMember(group0Object.id, admin.id);
            expect(isMember).to.be(false);
        });

        it('can add member to the group', async function () {
            await groups.addMember(group0Object.id, admin.id);
        });

        it('cannot add same member to the group', async function () {
            const [error] = await safe(groups.addMember(group0Object.id, admin.id));
            expect(error.reason).to.be(BoxError.ALREADY_EXISTS);
        });

        it('isMember returns true', async function () {
            const isMember = await groups.isMember(group0Object.id, admin.id);
            expect(isMember).to.be(true);
        });

        it('cannot add invalid user to group', async function () {
            const [error] = await safe(groups.addMember(group0Object.id, 'random'));
            expect(error.reason).to.be(BoxError.NOT_FOUND);
        });

        it('cannot add non-existent group', async function () {
            const [error] = await safe(groups.addMember('randomgroup', admin.id));
            expect(error.reason).to.be(BoxError.NOT_FOUND);
        });

        it('can set members', async function () {
            await groups.setMembers(group0Object.id, [ admin.id, user.id ]);
        });

        it('cannot set duplicate members', async function () {
            const [error] = await safe(groups.setMembers(group0Object.id, [ admin.id, user.id, admin.id ]));
            expect(error.reason).to.be(BoxError.CONFLICT);
        });

        it('can list users of group', async function () {
            const result = await groups.getMembers(group0Object.id);
            expect(result.sort()).to.eql([ admin.id, user.id ].sort());
        });

        it('cannot list members of non-existent group', async function () {
            const result = await groups.getMembers('randomgroup');
            expect(result.length).to.be(0); // currently, we cannot differentiate invalid groups and empty groups
        });

        it('cannot delete non-existent member', async function () {
            const [error] = await safe(groups.removeMember(group0Object.id, 'random'));
            expect(error.reason).to.be(BoxError.NOT_FOUND);
        });

        it('cannot remove member from non-existent group', async function () {
            const [error] = await safe(groups.removeMember('randomgroup', admin.id));
            expect(error.reason).to.be(BoxError.NOT_FOUND);
        });

        it('can remove existing member', async function () {
            await groups.removeMember(group0Object.id, user.id);
        });

        it('can getWithMembers', async function () {
            const result = await groups.getWithMembers(group0Object.id);
            expect(result.name).to.be(group0Name);
            expect(result.userIds).to.eql([ admin.id ]);
        });

        it('can set groups', async function () {
            await groups.setMembership(admin.id, [ group0Object.id ]);
        });

        it('cannot set user to same group twice', async function () {
            const [error] = await safe(groups.setMembership(admin.id, [ group0Object.id, group0Object.id ]));
            expect(error.reason).to.be(BoxError.CONFLICT);
        });

        it('can set user to multiple groups', async function () {
            await groups.setMembership(admin.id, [ group0Object.id, group1Object.id ]);
        });

        it('can get groups membership', async function () {
            const groupIds = await groups.getMembership(admin.id);
            expect(groupIds.length).to.be(2);
            expect(groupIds.sort()).to.eql([ group0Object.id, group1Object.id ].sort());
        });

        it('can list', async function () {
            const result = await groups.list();
            expect(result.length).to.be(2);
            expect(result[0].name).to.be(group0Name);
            expect(result[1].name).to.be(group1Name);
        });

        it('can listWithMembers', async function () {
            const result = await groups.listWithMembers();
            expect(result.length).to.be(2);
            expect(result[0].name).to.be(group0Name);
            expect(result[1].userIds).to.eql([ admin.id ]);
            expect(result[1].name).to.be(group1Name);
        });

        it('cannot delete invalid group', async function () {
            const [error] = await safe(groups.remove('random'));
            expect(error.reason).to.be(BoxError.NOT_FOUND);
        });

        it('can delete valid group', async function () {
            await groups.setMembers(group0Object.id, [ admin.id, user.id ]); // ensure group has some members
            await groups.remove(group0Object.id);
        });
    });
});
