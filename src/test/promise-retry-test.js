/* jslint node:true */
/* global it:false */
/* global describe:false */

'use strict';

const expect = require('expect.js'),
    promiseRetry = require('../promise-retry.js'),
    safe = require('safetydance');

describe('promiseRetry', function () {
    this.timeout(0);

    it('normal return', async function () {
        const result = await promiseRetry({ times: 5, interval: 1000 }, async () => {
            return 42;
        });

        expect(result).to.be(42);
    });

    it('throws error', async function () {
        const [error] = await safe(promiseRetry({ times: 5, interval: 1000 }, async () => {
            throw new Error('42');
        }));

        expect(error.message).to.be('42');
    });

    it('3 tries', async function () {
        let tryCount = 0;
        const result = await promiseRetry({ times: 5, interval: 1000 }, async () => {
            if (++tryCount == 3) return 42; else throw new Error('42');
        });

        expect(result).to.be(42);
    });

    it('can abort with 1 try only', async function () {
        let tryCount = 0;
        const [error] = await safe(promiseRetry({ times: 5, interval: 1000, retry: () => false }, async () => {
            ++tryCount;
            throw tryCount;
        }));

        expect(tryCount).to.be(1);
        expect(error).to.be(1);
    });
});
