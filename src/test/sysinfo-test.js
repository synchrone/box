/* global it:false */
/* global describe:false */

'use strict';

const expect = require('expect.js'),
    sysinfo = require('../sysinfo.js');

describe('config', function () {
    it('test machine has IPv6 support', function () {
        expect(sysinfo.hasIPv6()).to.equal(true);
    });
});
