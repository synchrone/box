/* jslint node:true */
/* global it:false */
/* global before:false */
/* global after:false */
/* global describe:false */

'use strict';

const common = require('./common.js'),
    docker = require('../docker.js'),
    expect = require('expect.js');

describe('docker', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    it('can df', async function () {
        const output = await docker.df();
        expect(output).to.be.ok();
    });
});
