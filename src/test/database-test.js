/* global it:false */
/* global describe:false */
/* global before:false */

'use strict';

const database = require('../database'),
    expect = require('expect.js'),
    fs = require('fs'),
    safe = require('safetydance');

describe('database', function () {
    describe('init', function () {
        it('can init database', async function () {
            await database.initialize();
        });

        it('can clear database', async function () {
            await database._clear();
        });

        it('can uninitialize database', async function () {
            await database.uninitialize();
        });
    });

    describe('importFromFile', function () {
        before(async function () {
            await database.initialize();
            await database._clear();
        });

        it('cannot import from non-existent file', async function () {
            const [error] = await safe(database.importFromFile('/does/not/exist'));
            expect(error).to.be.ok();
        });

        it('can export to file', async function () {
            await database.exportToFile('/tmp/box.mysqldump');
        });

        it('can import from file', async function () {
            await database.importFromFile('/tmp/box.mysqldump');
        });
    });
});
