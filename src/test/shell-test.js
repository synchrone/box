/* jslint node:true */
/* global it:false */
/* global describe:false */

'use strict';

const expect = require('expect.js'),
    path = require('path'),
    safe = require('safetydance'),
    shell = require('../shell.js');

describe('shell', function () {
    it('can run valid program', function (done) {
        let cp = shell.spawn('test', 'ls', [ '-l' ], { }, function (error) {
            expect(cp).to.be.ok();
            expect(error).to.be(null);
            done();
        });
    });

    it('fails on invalid program', function (done) {
        shell.spawn('test', 'randomprogram', [ ], { }, function (error) {
            expect(error).to.be.ok();
            done();
        });
    });

    it('fails on failing program', function (done) {
        shell.spawn('test', '/usr/bin/false', [ ], { }, function (error) {
            expect(error).to.be.ok();
            done();
        });
    });

    it('cannot sudo invalid program', function (done) {
        shell.sudo('test', [ 'randomprogram' ], {}, function (error) {
            expect(error).to.be.ok();
            done();
        });
    });

    it('can sudo valid program', function (done) {
        let RELOAD_NGINX_CMD = path.join(__dirname, '../src/scripts/restartservice.sh');
        shell.sudo('test', [ RELOAD_NGINX_CMD, 'nginx' ], {}, function (error) {
            expect(error).to.be.ok();
            done();
        });
    });

    it('can run valid program (promises)', async function () {
        let RELOAD_NGINX_CMD = path.join(__dirname, '../src/scripts/restartservice.sh');
        await safe(shell.promises.sudo('test', [ RELOAD_NGINX_CMD, 'nginx' ], {}));
    });

    it('execSync a valid shell program', function (done) {
        shell.exec('test', 'ls -l | wc -c', function (error) {
            done(error);
        });
    });

    it('execSync a valid shell program (promises)', async function () {
        await shell.promises.exec('test', 'ls -l | wc -c');
    });

    it('execSync throws for invalid program', function (done) {
        shell.exec('test', 'cannotexist', function (error) {
            expect(error).to.be.ok();
            done();
        });
    });

    it('execSync throws for failed program', function (done) {
        shell.exec('test', 'false', function (error) {
            expect(error).to.be.ok();
            done();
        });
    });
});
