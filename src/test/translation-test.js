/* jslint node:true */
/* global it:false */
/* global describe:false */

'use strict';

const expect = require('expect.js'),
    translation = require('../translation.js');

describe('translation', function () {

    describe('translate', function () {
        it('nonexisting token', function () {
            const  out = translation.translate('Foo {{ bar }}', {}, {});
            expect(out).to.contain('{{ bar }}');
        });

        it('existing token', function () {
            const  out = translation.translate('Foo {{ bar }}', { bar: 'here' }, {});
            expect(out).to.contain('here');
        });

        it('existing token as fallback', function () {
            const  out = translation.translate('Foo {{ bar }}', {}, { bar: 'here' });
            expect(out).to.contain('here');
        });

        it('existing token deep', function () {
            const  out = translation.translate('Foo {{ bar.baz.foo }}', { bar: { baz: { foo: 'here' }}}, {});
            expect(out).to.contain('here');
        });

        it('existing token deep as fallback', function () {
            const  out = translation.translate('Foo {{ bar.baz.foo }}', { bar: '' }, { bar: { baz: { foo: 'here' }}});
            expect(out).to.contain('here');
        });

        it('with whitespace tokens', function () {
            const  obj = {
                something: {
                    missing: {
                        there: '1'
                    }
                },
                here: '2',
                there: '3',
                foo: '4',
                bar: '5'
            };
            const  input = 'Hello {{ something.missing.there}} and some more {{here}} and {{ there }} with odd spacing {{foo }} lots of{{       bar     }}';

            const  out = translation.translate(input, obj, {});
            expect(out).to.contain('1');
            expect(out).to.contain('2');
            expect(out).to.contain('3');
            expect(out).to.contain('4');
            expect(out).to.contain('5');
        });
    });
});
