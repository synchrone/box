/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const acme2 = require('../acme2.js'),
    common = require('./common.js'),
    expect = require('expect.js');

describe('Acme2', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    describe('getChallengeSubdomain', function () {
        it('non-wildcard', function () {
            expect(acme2._getChallengeSubdomain('example.com', 'example.com')).to.be('_acme-challenge');
            expect(acme2._getChallengeSubdomain('git.example.com', 'example.com')).to.be('_acme-challenge.git');
        });

        it('wildcard', function () {
            expect(acme2._getChallengeSubdomain('*.example.com', 'example.com')).to.be('_acme-challenge');
            expect(acme2._getChallengeSubdomain('*.git.example.com', 'example.com')).to.be('_acme-challenge.git');
            expect(acme2._getChallengeSubdomain('*.example.com', 'customer.example.com')).to.be('_acme-challenge');
        });
    });
});
