/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const branding = require('../branding.js'),
    common = require('./common.js'),
    constants = require('../constants.js'),
    expect = require('expect.js');

describe('Branding', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    it('can render default footer', async function () {
        expect(branding.renderFooter(constants.FOOTER)).to.contain('(https://cloudron.io)');
    });

    it('can render footer', async function () {
        expect(branding.renderFooter('BigFoot Inc')).to.be('BigFoot Inc');
    });

    it('can render footer with YEAR', async function () {
        expect(branding.renderFooter('BigFoot Inc %YEAR%')).to.be('BigFoot Inc 2022');
    });
});
