/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const apptask = require('../apptask.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    paths = require('../paths.js'),
    safe = require('safetydance'),
    _ = require('underscore');

describe('apptask', function () {
    const { setup, cleanup, app } = common;

    before(setup);
    after(cleanup);

    it('create volume', async function () {
        await apptask._createAppDir(app);
        expect(fs.existsSync(paths.APPS_DATA_DIR + '/' + app.id)).to.be(true);
        expect(fs.existsSync(paths.APPS_DATA_DIR + '/' + app.id + '/data')).to.be(false);
    });

    it('delete volume - removeDirectory (false) ', async function () {
        await apptask._deleteAppDir(app, { removeDirectory: false });
        expect(fs.existsSync(paths.APPS_DATA_DIR + '/' + app.id)).to.be(true);
        expect(fs.readdirSync(paths.APPS_DATA_DIR + '/' + app.id).length).to.be(0); // empty
    });

    it('delete volume - removeDirectory (true) ', async function () {
        await apptask._deleteAppDir(app, { removeDirectory: true });
        expect(!fs.existsSync(paths.APPS_DATA_DIR + '/' + app.id)).to.be(true);
    });

    it('barfs on empty manifest', async function () {
        const badApp = _.extend({ }, app);
        badApp.manifest = { };

        const [error] = await safe(apptask._verifyManifest(badApp.manifest));
        expect(error).to.be.ok();
    });

    it('fails on bad manifest', async function () {
        const badApp = _.extend({ }, app);
        badApp.manifest = _.extend({ }, app.manifest);
        delete badApp.manifest.httpPort;

        const [error] = await safe(apptask._verifyManifest(badApp.manifest));
        expect(error).to.be.ok();
    });

    it('barfs on incompatible manifest', async function () {
        const badApp = _.extend({ }, app);
        badApp.manifest = _.extend({ }, app.manifest);
        badApp.manifest.maxBoxVersion = '0.0.0'; // max box version is too small

        const [error] = await safe(apptask._verifyManifest(badApp.manifest));
        expect(error).to.be.ok();
    });

    it('verifies manifest', async function () {
        const goodApp = _.extend({ }, app);

        await apptask._verifyManifest(goodApp.manifest);
    });
});
