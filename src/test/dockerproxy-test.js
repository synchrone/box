/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const child_process = require('child_process'),
    constants = require('../constants.js'),
    dockerProxy = require('../dockerproxy.js'),
    expect = require('expect.js');

const DOCKER = `docker -H tcp://172.18.0.1:${constants.DOCKER_PROXY_PORT} `;

async function exec(cmd) {
    return new Promise((resolve, reject) => {
        child_process.exec(cmd, { encoding: 'utf8' }, function (error, stdout) {
            if (error) return reject(error);

            resolve(stdout);
        });
    });
}

describe('Dockerproxy', function () {
    let containerId;

    // create a container to test against
    before(async function () {
        await dockerProxy.start();

        const stdout = await exec(`${DOCKER} run -d cloudron/base:3.0.0 "bin/bash" "-c" "while true; do echo 'perpetual walrus'; sleep 1; done"`);
        containerId = stdout.slice(0, -1); // removes the trailing \n
    });

    after(async function () {
        await exec(`${DOCKER} rm -f ${containerId}`);
        await dockerProxy.stop();
    });

    // uncomment this to run the proxy for manual testing
    // it('wait', function (done) {} );

    it('can get info', async function () {
        const stdout = await exec(DOCKER + ' info');
        expect(stdout).to.contain('Containers:');
        // expect(stderr).to.be.empty(); // on some machines, i get 'No swap limit support'
    });

    it('can create container', async function () {
        const cmd = `${DOCKER} run cloudron/base:3.0.0 "/bin/bash" "-c" "echo 'hello'"`;
        const stdout = await exec(cmd);
        expect(stdout).to.contain('hello');
    });

    it('proxy overwrites the container network option', async function () {
        const cmd = `${DOCKER} run --network ifnotrewritethiswouldfail cloudron/base:3.0.0 "/bin/bash" "-c" "echo 'hello'"`;
        const stdout = await exec(cmd);
        expect(stdout).to.contain('hello');
    });

    xit('cannot see logs through docker logs, since syslog is configured', function (done) {
        exec(`${DOCKER} logs ${containerId}`, function (error, stdout, stderr) {
            expect(error.message).to.contain('configured logging driver does not support reading');
            expect(stderr).to.contain('configured logging driver does not support reading');
            expect(stdout).to.be.empty();

            done();
        });
    });

    it('can use PUT to upload archive into a container', async function () {
        const stdout = await exec(`${DOCKER} cp ${__dirname}/proxytestarchive.tar ${containerId}:/tmp/`);
        expect(stdout).to.be.empty();
    });

    it('can exec into a container', async function () {
        const stdout = await exec(`${DOCKER} exec ${containerId} ls`);
        expect(stdout).to.equal('bin\nboot\ndev\netc\nhome\nlib\nlib32\nlib64\nlibx32\nmedia\nmnt\nopt\nproc\nroot\nrun\nsbin\nsrv\nsys\ntmp\nusr\nvar\n');
    });
});
