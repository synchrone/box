/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    delay = require('../delay.js'),
    expect = require('expect.js'),
    notifications = require('../notifications.js'),
    safe = require('safetydance');

const EVENT_0 = {
    id: 'event_0',
    action: 'action',
    source: {},
    data: {}
};

describe('Notifications', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    let notificationIds = [];

    it('can add notifications', async function () {
        for (let i = 0; i < 3; i++) {
            const [error, id] = await safe(notifications._add(EVENT_0.id, `title ${i}`, `message ${i}`));
            expect(error).to.equal(null);
            expect(id).to.be.a('string');
            notificationIds.push(id);
            await delay(1000);
        }
    });

    it('can get by id', async function () {
        const [error, result] = await safe(notifications.get(notificationIds[0]));
        expect(error).to.be(null);
        expect(result.title).to.be('title 0');
        expect(result.message).to.be('message 0');
        expect(result.acknowledged).to.be(false);
    });

    it('cannot get non-existent id', async function () {
        const result = await notifications.get('random');
        expect(result).to.be(null);
    });

    it('can list notifications', async function () {
        const result = await notifications.list({}, 1, 10);
        expect(result.length).to.be(3);
        expect(result[0].title).to.be('title 2');
        expect(result[1].title).to.be('title 1');
        expect(result[2].title).to.be('title 0');
    });

    it('can update notification', async function () {
        await notifications.update({ id: notificationIds[0] }, { title: 'updated title 0', message: 'updated message 0', acknowledged: true });

        const result = await notifications.get(notificationIds[0]);
        expect(result.title).to.be('updated title 0');
        expect(result.message).to.be('updated message 0');
        expect(result.acknowledged).to.be(true);
    });

    it('cannot update non-existent notification', async function () {
        const [error] = await safe(notifications.update({ id: 'random' }, { title: 'updated title 0', message: 'updated message 0', acknowledged: true }));
        expect(error.reason).to.be(BoxError.NOT_FOUND);

    });

    it('can delete', async function () {
        await notifications.del(notificationIds[0]);
    });

    it('cannot delete non-existent notification', async function () {
        const [error] = await safe(notifications.del('random'));
        expect(error.reason).to.be(BoxError.NOT_FOUND);
    });

    let alertId;
    it('can add alert', async function () {
        alertId = await notifications.alert(notifications.ALERT_BOX_UPDATE, 'Cloudron xx is available', 'Awesome changelog');

        const result = await notifications.get(alertId);
        expect(result.title).to.be('Cloudron xx is available');
        expect(result.message).to.be('Awesome changelog');
        expect(result.acknowledged).to.be(false);
    });

    it('can update the alert', async function () {
        await notifications.update({ id: alertId }, { acknowledged: true }); // ack the alert

        const id = await notifications.alert(notifications.ALERT_BOX_UPDATE, 'Cloudron xx is available', 'Awesome new changelog');
        expect(id).to.be(alertId);

        const result = await notifications.get(alertId);
        expect(result.title).to.be('Cloudron xx is available');
        expect(result.message).to.be('Awesome new changelog');
        expect(result.acknowledged).to.be(false); // notification resurfaces
    });

    it('can delete the alert', async function () {
        const id = await notifications.alert(notifications.ALERT_BOX_UPDATE, 'Cloudron xx is available', '');
        expect(id).to.be(null);

        const result = await notifications.get(alertId);
        expect(result).to.be(null);
    });
});
