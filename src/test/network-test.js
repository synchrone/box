/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    network = require('../network.js'),
    paths = require('../paths.js'),
    safe = require('safetydance');

describe('Network', function () {
    const { setup, cleanup, auditSource } = common;

    before(setup);
    after(cleanup);

    describe('Blocklist', function () {
        before(function () {
            fs.writeFileSync(paths.FIREWALL_BLOCKLIST_FILE, '', 'utf8');
        });

        it('can get empty blocklist', async function () {
            const result = await network.getBlocklist();
            expect(result).to.equal('');
        });

        it('can set empty blocklist', async function () {
            await network.setBlocklist('', auditSource);
            const result = await network.getBlocklist();
            expect(result).to.equal('');
        });

        it('can set single IPv4 in blocklist', async function () {
            await network.setBlocklist('192.168.178.1', auditSource);
            const result = await network.getBlocklist();
            expect(result).to.equal('192.168.178.1');
        });

        it('can set single IPv6 in blocklist', async function () {
            await network.setBlocklist('2a02:8106:2f:bb00:7afc:5703:ee71:3ef8', auditSource);
            const result = await network.getBlocklist();
            expect(result).to.equal('2a02:8106:2f:bb00:7afc:5703:ee71:3ef8');
        });

        it('can set mixed IPs with comment in blocklist', async function () {
            await network.setBlocklist('2a02:8106:2f:bb00:7afc:5703:ee71:3ef8\n# some comment\n192.168.178.1', auditSource);
            const result = await network.getBlocklist();
            expect(result).to.equal('2a02:8106:2f:bb00:7afc:5703:ee71:3ef8\n# some comment\n192.168.178.1');
        });

        it('can set single IPv4 range in blocklist', async function () {
            await network.setBlocklist('192.168.178.1/24', auditSource);
            const result = await network.getBlocklist();
            expect(result).to.equal('192.168.178.1/24');
        });

        it('can set single IPv6 range in blocklist', async function () {
            await network.setBlocklist('2001:db8::', auditSource);
            const result = await network.getBlocklist();
            expect(result).to.equal('2001:db8::');
        });

        it('cannot set IPv4 in blocklist if source is same', async function () {
            const [error] = await safe(network.setBlocklist('127.0.0.1', { ip: '127.0.0.1' }));
            expect(error.reason).to.equal(BoxError.BAD_FIELD);
        });

        it('cannot set IPv6 in blocklist if source is same', async function () {
            const [error] = await safe(network.setBlocklist('2001:db8:1234::1', { ip: '2001:db8:1234::1' }));
            expect(error.reason).to.equal(BoxError.BAD_FIELD);
        });

        it('cannot set IPv4 range in blocklist if source is same', async function () {
            const [error] = await safe(network.setBlocklist('127.0.0.1/32', { ip: '127.0.0.1' }));
            expect(error.reason).to.equal(BoxError.BAD_FIELD);
        });

        it('cannot set IPv6 range in blocklist if source is same', async function () {
            const [error] = await safe(network.setBlocklist('2001:db8:1234:::', { ip: '2001:db8:1234::1' }));
            expect(error.reason).to.equal(BoxError.BAD_FIELD);
        });

        it('can set IPv4 in blocklist if source is IPv6', async function () {
            await network.setBlocklist('192.168.178.1', { ip: '2001:db8:1234::1' });
            const result = await network.getBlocklist();
            expect(result).to.equal('192.168.178.1');
        });

        it('can set IPv6 in blocklist if source is IPv4', async function () {
            await network.setBlocklist('2001:db8:1234::1', auditSource);
            const result = await network.getBlocklist();
            expect(result).to.equal('2001:db8:1234::1');
        });

        it('can set IPv4 range in blocklist if source is IPv6', async function () {
            await network.setBlocklist('192.168.178.1/32', { ip: '2001:db8:1234::1' });
            const result = await network.getBlocklist();
            expect(result).to.equal('192.168.178.1/32');
        });

        it('can set IPv6 range in blocklist if source is IPv4', async function () {
            await network.setBlocklist('2001:db8:1234::', auditSource);
            const result = await network.getBlocklist();
            expect(result).to.equal('2001:db8:1234::');
        });
    });
});
