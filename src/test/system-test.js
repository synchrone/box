/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    system = require('../system.js');

describe('System', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    it('can get disks', async function () {
        // does not work on archlinux 8!
        if (require('child_process').execSync('uname -a').toString().indexOf('-arch') !== -1) return;

        const disks = await system.getDisks();
        expect(disks).to.be.ok();
        expect(Object.keys(disks).some(fs => disks[fs].mountpoint === '/')).to.be.ok();
    });

    it('can get swaps', async function () {
        // does not work on archlinux 8!
        if (require('child_process').execSync('uname -a').toString().indexOf('-arch') !== -1) return;

        const swaps = await system.getSwaps();
        expect(swaps).to.be.ok();
        expect(Object.keys(swaps).some(n => swaps[n].type === 'partition')).to.be.ok();
    });

    it('can check for disk space', async function () {
        // does not work on archlinux 8!
        if (require('child_process').execSync('uname -a').toString().indexOf('-arch') !== -1) return;

        await system.checkDiskSpace();
    });

    it('can get memory', async function () {
        const memory = await system.getMemory();
        expect(memory.memory).to.be.a('number');
        expect(memory.swap).to.be.a('number');
    });

    it('can get diskUsage', async function () {
        const usage = await system.getDiskUsage();
        expect(usage).to.be(null); // nothing cached
    });
});
