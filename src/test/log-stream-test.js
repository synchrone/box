/* global it:false */
/* global describe:false */

'use strict';

const expect = require('expect.js'),
    fs = require('fs'),
    LogStream = require('../log-stream.js'),
    stream = require('stream');

describe('log stream', function () {
    it('can create stream', function (done) {
        fs.writeFileSync('/tmp/test-input.log', '2022-10-09T15:19:48.740Z message', 'utf8');
        const input = fs.createReadStream('/tmp/test-input.log');
        const log = new LogStream({ format: 'json', source: 'test' });
        const output = fs.createWriteStream('/tmp/test-output.log');

        stream.pipeline(input, log, output, function (error) {
            expect(error).to.not.be.ok();

            const out = fs.readFileSync('/tmp/test-output.log', 'utf8');
            const firstLine = JSON.parse(out.split('\n')[0]);
            expect(firstLine.realtimeTimestamp).to.be.a('number');
            expect(firstLine.message).to.be('message');
            expect(firstLine.source).to.be('test');
            done();
        });
    });
});
