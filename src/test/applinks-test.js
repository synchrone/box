/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const applinks = require('../applinks.js'),
    BoxError = require('../boxerror.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    safe = require('safetydance');

describe('Applinks', function () {
    const { setup, cleanup } = common;

    before(setup);
    after(cleanup);

    const APPLINK_0 = {
        upstreamUri: 'https://cloudron.io'
    };

    const APPLINK_1 = {
        upstreamUri: 'https://www.digitalocean.com/',
        label: 'Digitalocean YaY',
        accessRestriction: { users: [], groups: [] },
        tags: [ 'vps', 'vservers' ],
        icon: 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX/TQBcNTh/AAAAAXRSTlPM0jRW/QAAAApJREFUeJxjYgAAAAYAAzY3fKgAAAAASUVORK5CYII='
    };

    const APPLINK_2 = {
        upstreamUri: 'https://google.com'
    };

    const APPLINK_3 = {
        upstreamUri: 'http://example.com'
    };

    it('can add applink with redirect', async function () {
        APPLINK_0.id = await applinks.add(JSON.parse(JSON.stringify(APPLINK_0)));
    });

    it('can add second applink with attributes', async function () {
        APPLINK_1.id = await applinks.add(JSON.parse(JSON.stringify(APPLINK_1)));
    });

    it('can add third applink to test google.com favicon', async function () {
        APPLINK_2.id = await applinks.add(JSON.parse(JSON.stringify(APPLINK_2)));

        const result = await applinks.get(APPLINK_2.id);
        expect(result.upstreamUri).to.eql(APPLINK_2.upstreamUri); // should not have changed
        expect(result.icon.length).to.not.eql(0);
    });

    it('can add fourth applink to test no favicon', async function () {
        APPLINK_3.id = await applinks.add(JSON.parse(JSON.stringify(APPLINK_3)));

        const result = await applinks.get(APPLINK_3.id);
        expect(result.upstreamUri).to.eql('http://example.com');
        expect(result.icon).to.eql(null);
    });

    it('can list all without accessRestriction', async function () {
        const result = await applinks.list();

        expect(result.length).to.equal(4);
        expect(result[1].id).to.eql(APPLINK_0.id);
        expect(result[1].upstreamUri).to.eql(APPLINK_0.upstreamUri);
        expect(result[3].id).to.eql(APPLINK_1.id);
        expect(result[3].upstreamUri).to.eql(APPLINK_1.upstreamUri);
        expect(result[3].label).to.eql(APPLINK_1.label);
        expect(result[3].accessRestriction).to.eql(APPLINK_1.accessRestriction);
        expect(result[3].tags).to.eql(APPLINK_1.tags);
        expect(result[3].icon.toString('base64')).to.eql(APPLINK_1.icon);
    });

    it('cannot get applink with wrong id', async function () {
        const result = await applinks.get('doesnotexist');
        expect(result).to.be(null);
    });

    it('can get applink', async function () {
        const result = await applinks.get(APPLINK_0.id);
        expect(result.upstreamUri).to.eql(APPLINK_0.upstreamUri);
    });

    it('can get second applink', async function () {
        const result = await applinks.get(APPLINK_1.id);

        expect(result.id).to.eql(APPLINK_1.id);
        expect(result.upstreamUri).to.eql(APPLINK_1.upstreamUri);
        expect(result.label).to.eql(APPLINK_1.label);
        expect(result.accessRestriction).to.eql(APPLINK_1.accessRestriction);
        expect(result.tags).to.eql(APPLINK_1.tags);
        expect(result.icon.toString('base64')).to.eql(APPLINK_1.icon);
    });

    it('can update applink', async function () {
        APPLINK_0.upstreamUri = 'https://duckduckgo.com';
        APPLINK_0.icon = APPLINK_1.icon;

        await applinks.update(APPLINK_0.id, JSON.parse(JSON.stringify(APPLINK_0)));

        const result = await applinks.get(APPLINK_0.id);
        expect(result.upstreamUri).to.equal('https://duckduckgo.com');
        expect(result.icon.toString('base64')).to.eql(APPLINK_1.icon);
    });

    it('can get applink icon', async function () {
        const result = await applinks.getIcon(APPLINK_0.id);
        expect(result.toString('base64')).to.eql(APPLINK_1.icon);
    });

    it('cannot remove applink with wrong id', async function () {
        const [error] = await safe(applinks.remove('doesnotexist'));
        expect(error).to.be.a(BoxError);
        expect(error.reason).to.eql(BoxError.NOT_FOUND);
    });

    it('can remove applink', async function () {
        await applinks.remove(APPLINK_0.id);

        const result = await applinks.get(APPLINK_0.id);
        expect(result).to.be(null);
    });
});
