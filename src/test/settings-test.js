/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    settings = require('../settings.js'),
    tokens = require('../tokens.js');

describe('Settings', function () {
    const { setup, cleanup, admin } = common;

    before(setup);
    after(cleanup);

    it('can get default timezone', async function () {
        const tz = await settings.getTimeZone();
        expect(tz.length).to.not.be(0);
    });

    it('can get default autoupdate_pattern', async function () {
        const pattern = await settings.getAutoupdatePattern();
        expect(pattern).to.be('00 00 1,3,5,23 * * *');
    });

    it ('can get default cloudron name', async function () {
        const name = await settings.getCloudronName();
        expect(name).to.be('Cloudron');
    });

    it('can get default cloudron avatar', async function () {
        const avatar = await settings.getCloudronAvatar();
        expect(avatar).to.be.a(Buffer);
    });

    it('can get backup config', async function () {
        const backupConfig = await settings.getBackupConfig();
        expect(backupConfig.provider).to.be('filesystem');
        expect(backupConfig.backupFolder).to.be('/var/backups');
    });

    it('can get default unstable apps setting', async function () {
        const enabled = await settings.getUnstableAppsConfig();
        expect(enabled).to.be(true);
    });

    it('can set unstable apps setting', async function () {
        await settings.setUnstableAppsConfig(false);

        const enabled = await settings.getUnstableAppsConfig();
        expect(enabled).to.be(false);
    });

    it('can get default IPv6 setting', async function () {
        const config = await settings.getIPv6Config();
        expect(config.provider).to.be('noop');
    });

    it('can set IPv6 setting', async function () {
        await settings.setIPv6Config({ provider: 'generic' });

        const config = await settings.getIPv6Config();
        expect(config.provider).to.be('generic');
    });

    it('can get default profile config', async function () {
        const profileConfig = await settings.getProfileConfig();
        expect(profileConfig.lockUserProfiles).to.be(false);
        expect(profileConfig.mandatory2FA).to.be(false);
    });

    it('can set default profile config', async function () {
        await tokens.add({ name: 'token1', identifier: admin.id, clientId: tokens.ID_WEBADMIN, expires: Number.MAX_SAFE_INTEGER, lastUsedTime: null });
        let result = await tokens.listByUserId(admin.id);
        expect(result.length).to.be(1); // just confirm the token was really added!

        await settings.setProfileConfig({ mandatory2FA: true, lockUserProfiles: true });
        result = await tokens.listByUserId(admin.id);
        expect(result.length).to.be(0); // should have been removed by mandatory 2fa setting change
    });

    it('can get all values', async function () {
        const allSettings = await settings.list();
        expect(allSettings[settings.TIME_ZONE_KEY]).to.be.a('string');
        expect(allSettings[settings.AUTOUPDATE_PATTERN_KEY]).to.be.a('string');
        expect(allSettings[settings.CLOUDRON_NAME_KEY]).to.be.a('string');
        expect(allSettings[settings.UNSTABLE_APPS_KEY]).to.be.a('boolean');
        expect(allSettings[settings.IPV6_CONFIG_KEY]).to.be.an('object');
    });
});
