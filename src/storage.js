'use strict';

exports = module.exports = {
    api,
};

// choose which storage backend we use for test purpose we use s3
function api(provider) {
    switch (provider) {
    case 'nfs': return require('./storage/filesystem.js');
    case 'cifs': return require('./storage/filesystem.js');
    case 'sshfs': return require('./storage/filesystem.js');
    case 'mountpoint': return require('./storage/filesystem.js');
    case 'ext4': return require('./storage/filesystem.js');
    case 's3': return require('./storage/s3.js');
    case 'gcs': return require('./storage/gcs.js');
    case 'filesystem': return require('./storage/filesystem.js');
    case 'minio': return require('./storage/s3.js');
    case 's3-v4-compat': return require('./storage/s3.js');
    case 'digitalocean-spaces': return require('./storage/s3.js');
    case 'exoscale-sos': return require('./storage/s3.js');
    case 'wasabi': return require('./storage/s3.js');
    case 'scaleway-objectstorage': return require('./storage/s3.js');
    case 'backblaze-b2': return require('./storage/s3.js');
    case 'cloudflare-r2': return require('./storage/s3.js');
    case 'linode-objectstorage': return require('./storage/s3.js');
    case 'ovh-objectstorage': return require('./storage/s3.js');
    case 'ionos-objectstorage': return require('./storage/s3.js');
    case 'vultr-objectstorage': return require('./storage/s3.js');
    case 'upcloud-objectstorage': return require('./storage/s3.js');
    case 'noop': return require('./storage/noop.js');
    default: return null;
    }
}
