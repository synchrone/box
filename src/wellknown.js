'use strict';

exports = module.exports = {
    get
};

const assert = require('assert'),
    BoxError = require('./boxerror.js'),
    domains = require('./domains.js'),
    ejs = require('ejs'),
    fs = require('fs'),
    mail = require('./mail.js'),
    settings = require('./settings.js');

const MAIL_AUTOCONFIG_EJS = fs.readFileSync(__dirname + '/autoconfig.xml.ejs', { encoding: 'utf8' });

async function get(domain, location) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof location, 'string');

    if (location === 'autoconfig/mail/config-v1.1.xml') { // this also gets a ?emailaddress

        const mailDomain = await mail.getDomain(domain);
        if (!mailDomain) throw new BoxError(BoxError.NOT_FOUND, 'Domain not found');
        if (!mailDomain.enabled) throw new BoxError(BoxError.NOT_FOUND, 'Email not enabled');

        const autoconfig = ejs.render(MAIL_AUTOCONFIG_EJS, { domain, mailFqdn: settings.mailFqdn() });

        return { type: 'text/xml', body: autoconfig };
    } else if (location === 'host-meta' || location === 'matrix/server' || location === 'matrix/client') {
        const type = location === 'host-meta' ? 'text/xml' : 'application/json';

        const domainObject = await domains.get(domain);
        if (!domainObject) throw new BoxError(BoxError.NOT_FOUND, 'Domain not found');
        if (!domainObject.wellKnown || !(location in domainObject.wellKnown)) throw new BoxError(BoxError.NOT_FOUND, 'No custom well-known config');

        return { type, body: domainObject.wellKnown[location] };
    } else {
        throw new BoxError(BoxError.NOT_FOUND, 'No custom well-known config');
    }
}
