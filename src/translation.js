'use strict';

exports = module.exports = {
    translate,
    getTranslations,
    getLanguages
};

const assert = require('assert'),
    debug = require('debug')('box:translation'),
    fs = require('fs'),
    path = require('path'),
    paths = require('./paths.js'),
    safe = require('safetydance'),
    settings = require('./settings.js');

const TRANSLATION_FOLDER = path.join(paths.DASHBOARD_DIR, 'translation');

// to be used together with getTranslations() => { translations, fallback }
function translate(input, translations, fallbackTranslations) {
    assert.strictEqual(typeof input, 'string');
    assert.strictEqual(typeof translations, 'object');
    assert.strictEqual(typeof fallbackTranslations, 'object');

    const tokens = input.match(/{{(.*?)}}/gm);
    if (!tokens) return input;

    let output = input;
    tokens.forEach(function (token) {
        const key = token.slice(2).slice(0, -2).trim();
        let value = key.split('.').reduce(function (acc, cur) {
            if (acc === null) return null;
            return typeof acc[cur] !== 'undefined' ? acc[cur] : null;
        }, translations);

        // try fallback
        if (value === null) value = key.split('.').reduce(function (acc, cur) {
            if (acc === null) return null;
            return typeof acc[cur] !== 'undefined' ? acc[cur] : null;
        }, fallbackTranslations);

        if (value === null) value = token;

        output = output.replace(token, value);
    });

    return output;
}

async function getTranslations() {
    let fallback = safe.JSON.parse(fs.readFileSync(path.join(TRANSLATION_FOLDER, 'en.json'), 'utf8'));
    if (!fallback) {
        debug('getTranslations: Fallback language en not found', safe.error);
        fallback = {};
    }

    const lang = await settings.getLanguage();

    let translations = safe.JSON.parse(fs.readFileSync(path.join(TRANSLATION_FOLDER, lang + '.json'), 'utf8'));
    if (!translations) {
        debug(`getTranslations: Requested language ${lang} not found`, safe.error);
        translations = {};
    }

    return { translations, fallback };
}

async function getLanguages() {
    // we always return english to avoid dashboard breakage
    let languages = ['en'];

    const [error, result] = await safe(fs.promises.readdir(TRANSLATION_FOLDER));
    if (error) {
        debug('getLanguages: Failed to list translations', error);
        return languages;
    }

    const jsonFiles = result.filter(function (file) { return path.extname(file) === '.json'; });
    languages = jsonFiles.map(function (file) { return path.basename(file, '.json'); });

    return languages;
}
