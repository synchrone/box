#!/bin/bash

set -eu -o pipefail

if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

if [[ $# -eq 0 ]]; then
    echo "No arguments supplied"
    exit 1
fi

if [[ "$1" == "--check" ]]; then
    echo "OK"
    exit 0
fi

mailbox="$1"

if [[ "${BOX_ENV}" == "cloudron" ]]; then
    readonly mailbox_dir="${HOME}/boxdata/mail/vmail/$1"
else
    readonly mailbox_dir="${HOME}/.cloudron_test/boxdata/mail/vmail/$1"
fi

rm -rf "${mailbox_dir}"

