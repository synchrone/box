#!/bin/bash

set -eu -o pipefail

if [[ ${EUID} -ne 0 ]]; then
    echo "This script should be run as root." > /dev/stderr
    exit 1
fi

if [[ $# -eq 0 ]]; then
    echo "No arguments supplied"
    exit 1
fi

if [[ "$1" == "--check" ]]; then
    echo "OK"
    exit 0
fi

[[ "${BOX_ENV}" != "cloudron" ]] && exit

service="$1"

if [[ "${service}" == "unbound" ]]; then
    unbound-anchor -a /var/lib/unbound/root.key
    systemctl restart --no-block unbound
elif [[ "${service}" == "nginx" ]]; then
    nginx -s reload
elif [[ "${service}" == "docker" ]]; then
    systemctl restart --no-block docker
elif [[ "${service}" == "collectd" ]]; then
    systemctl restart --no-block collectd
else
    echo "Unknown service ${service}"
    exit 1
fi

