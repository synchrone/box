'use strict';

exports = module.exports = {
    updateToLatest,
    update
};

const apps = require('./apps.js'),
    assert = require('assert'),
    BoxError = require('./boxerror.js'),
    backuptask = require('./backuptask.js'),
    constants = require('./constants.js'),
    crypto = require('crypto'),
    debug = require('debug')('box:updater'),
    df = require('./df.js'),
    eventlog = require('./eventlog.js'),
    fs = require('fs'),
    locker = require('./locker.js'),
    os = require('os'),
    path = require('path'),
    paths = require('./paths.js'),
    promiseRetry = require('./promise-retry.js'),
    safe = require('safetydance'),
    semver = require('semver'),
    settings = require('./settings.js'),
    shell = require('./shell.js'),
    tasks = require('./tasks.js'),
    updateChecker = require('./updatechecker.js');

const RELEASES_PUBLIC_KEY = path.join(__dirname, 'releases.gpg');
const UPDATE_CMD = path.join(__dirname, 'scripts/update.sh');

async function downloadUrl(url, file) {
    assert.strictEqual(typeof file, 'string');

    // do not assert since it comes from the appstore
    if (typeof url !== 'string')  throw new BoxError(BoxError.EXTERNAL_ERROR, `url cannot be download to ${file} as it is not a string`);

    safe.fs.unlinkSync(file);

    await promiseRetry({ times: 10, interval: 5000, debug }, async function () {
        debug(`Downloading ${url} to ${file}`);

        const args = `-s --fail ${url} -o ${file}`;

        debug(`downloadUrl: curl ${args}`);

        const [error] = await safe(shell.promises.spawn('downloadUrl', '/usr/bin/curl', args.split(' '), {}));
        if (error) throw new BoxError(BoxError.NETWORK_ERROR, `Failed to download ${url}: ${error.message}`);

        debug(`downloadUrl: downloaded ${url} to ${file}`);
    });
}

async function gpgVerify(file, sig) {
    assert.strictEqual(typeof file, 'string');
    assert.strictEqual(typeof sig, 'string');

    const cmd = `/usr/bin/gpg --status-fd 1 --no-default-keyring --keyring ${RELEASES_PUBLIC_KEY} --verify ${sig} ${file}`;

    debug(`gpgVerify: ${cmd}`);

    const [error, stdout] = await safe(shell.promises.exec('gpgVerify', cmd));
    if (error) {
        debug(`gpgVerify: command failed. error: ${error}\n stdout: ${error.stdout}\n stderr: ${error.stderr}`);
        throw new BoxError(BoxError.NOT_SIGNED, `The signature in ${path.basename(sig)} could not be verified (command failed)`);
    }

    if (stdout.indexOf('[GNUPG:] VALIDSIG 0EADB19CDDA23CD0FE71E3470A372F8703C493CC') !== -1) return; // success

    debug(`gpgVerify: verification of ${sig} failed: ${stdout}\n`);

    throw new BoxError(BoxError.NOT_SIGNED, `The signature in ${path.basename(sig)} could not be verified (bad sig)`);
}

async function extractTarball(tarball, dir) {
    assert.strictEqual(typeof tarball, 'string');
    assert.strictEqual(typeof dir, 'string');

    const args = `-zxf ${tarball} -C ${dir}`;

    debug(`extractTarball: tar ${args}`);

    const [error] = await safe(shell.promises.spawn('extractTarball', '/bin/tar', args.split(' '), {}));
    if (error) throw new BoxError(BoxError.FS_ERROR, `Failed to extract release package: ${error.message}`);

    safe.fs.unlinkSync(tarball);

    debug(`extractTarball: extracted ${tarball} to ${dir}`);
}

async function verifyUpdateInfo(versionsFile, updateInfo) {
    assert.strictEqual(typeof versionsFile, 'string');
    assert.strictEqual(typeof updateInfo, 'object');

    const releases = safe.JSON.parse(safe.fs.readFileSync(versionsFile, 'utf8')) || {};
    if (!releases[constants.VERSION]) throw new BoxError(BoxError.EXTERNAL_ERROR, `No version info for ${constants.VERSION}`);
    if (!releases[constants.VERSION].next) throw new BoxError(BoxError.EXTERNAL_ERROR, `No next version info for ${constants.VERSION}`);
    const nextVersion = releases[constants.VERSION].next;
    if (typeof releases[nextVersion] !== 'object' || !releases[nextVersion]) throw new BoxError(BoxError.EXTERNAL_ERROR, 'No next version info');
    if (releases[nextVersion].sourceTarballUrl !== updateInfo.sourceTarballUrl) throw new BoxError(BoxError.EXTERNAL_ERROR, 'Version info mismatch');
}

async function downloadAndVerifyRelease(updateInfo) {
    assert.strictEqual(typeof updateInfo, 'object');

    const newBoxSource = path.join(os.tmpdir(), 'box-' + crypto.randomBytes(4).readUInt32LE(0));

    await downloadUrl(updateInfo.boxVersionsUrl, `${paths.UPDATE_DIR}/versions.json`);
    await downloadUrl(updateInfo.boxVersionsSigUrl, `${paths.UPDATE_DIR}/versions.json.sig`);
    await gpgVerify(`${paths.UPDATE_DIR}/versions.json`, `${paths.UPDATE_DIR}/versions.json.sig`);
    await verifyUpdateInfo(`${paths.UPDATE_DIR}/versions.json`, updateInfo);
    await downloadUrl(updateInfo.sourceTarballUrl, `${paths.UPDATE_DIR}/box.tar.gz`);
    await downloadUrl(updateInfo.sourceTarballSigUrl, `${paths.UPDATE_DIR}/box.tar.gz.sig`);
    await gpgVerify(`${paths.UPDATE_DIR}/box.tar.gz`, `${paths.UPDATE_DIR}/box.tar.gz.sig`);
    const [mkdirError] = await safe(fs.promises.mkdir(newBoxSource, { recursive: true }));
    if (mkdirError) throw new BoxError(BoxError.FS_ERROR, `Failed to create directory ${newBoxSource}: ${mkdirError.message}`);
    await extractTarball(`${paths.UPDATE_DIR}/box.tar.gz`, newBoxSource);

    return { file: newBoxSource };
}

async function checkFreeDiskSpace(neededSpace) {
    assert.strictEqual(typeof neededSpace, 'number');

    // can probably be a bit more aggressive here since a new update can bring in new docker images
    const [error, diskUsage] = await safe(df.file('/'));
    if (error) throw new BoxError(BoxError.FS_ERROR, error);

    if (diskUsage.available < neededSpace) throw new BoxError(BoxError.FS_ERROR, 'Not enough disk space');
}

async function update(boxUpdateInfo, options, progressCallback) {
    assert(boxUpdateInfo && typeof boxUpdateInfo === 'object');
    assert(options && typeof options === 'object');
    assert.strictEqual(typeof progressCallback, 'function');

    progressCallback({ percent: 1, message: 'Checking disk space' });

    await checkFreeDiskSpace(2*1024*1024*1024);

    progressCallback({ percent: 5, message: 'Downloading and verifying release' });

    const packageInfo = await downloadAndVerifyRelease(boxUpdateInfo);

    if (!options.skipBackup) {
        progressCallback({ percent: 10, message: 'Backing up' });

        await backuptask.fullBackup({ preserveSecs: 3*7*24*60*60 }, (progress) => progressCallback({ percent: 10+progress.percent*70/100, message: progress.message }));

        await checkFreeDiskSpace(2*1024*1024*1024); // check again in case backup is in same disk
    }

    debug(`Updating box with ${boxUpdateInfo.sourceTarballUrl}`);

    progressCallback({ percent: 70, message: 'Installing update' });

    // run installer.sh from new box code as a separate service
    await shell.promises.sudo('update', [ UPDATE_CMD, packageInfo.file ], {});

    // Do not add any code here. The installer script will stop the box code any instant
}

async function canUpdate(boxUpdateInfo) {
    assert.strictEqual(typeof boxUpdateInfo, 'object');

    const result = await apps.list();

    for (let app of result) {
        const maxBoxVersion = app.manifest.maxBoxVersion;
        if (semver.valid(maxBoxVersion) && semver.gt(boxUpdateInfo.version, maxBoxVersion)) {
            throw new BoxError(BoxError.BAD_STATE, `Cannot update to v${boxUpdateInfo.version} because ${app.fqdn} has a maxBoxVersion of ${maxBoxVersion}`);
        }
    }
}

async function updateToLatest(options, auditSource) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof auditSource, 'object');

    const boxUpdateInfo = updateChecker.getUpdateInfo().box;
    if (!boxUpdateInfo) throw new BoxError(BoxError.NOT_FOUND, 'No update available');
    if (!boxUpdateInfo.sourceTarballUrl) throw new BoxError(BoxError.BAD_STATE, 'No automatic update available');

    await canUpdate(boxUpdateInfo);

    const error = locker.lock(locker.OP_BOX_UPDATE);
    if (error) throw new BoxError(BoxError.BAD_STATE, `Cannot update now: ${error.message}`);

    const [getError, backupConfig] = await safe(settings.getBackupConfig());
    if (getError) throw getError;

    const memoryLimit = 'memoryLimit' in backupConfig ? Math.max(backupConfig.memoryLimit/1024/1024, 400) : 400;

    const [taskError, taskId] = await safe(tasks.add(tasks.TASK_UPDATE, [ boxUpdateInfo, options ]));
    if (taskError) throw taskError;

    await eventlog.add(eventlog.ACTION_UPDATE, auditSource, { taskId, boxUpdateInfo });

    tasks.startTask(taskId, { timeout: 20 * 60 * 60 * 1000 /* 20 hours */, nice: 15, memoryLimit }, async (error) => {
        locker.unlock(locker.OP_BOX_UPDATE);

        debug('Update failed with error', error);

        const timedOut = error.code === tasks.ETIMEOUT;
        await safe(eventlog.add(eventlog.ACTION_UPDATE_FINISH, auditSource, { taskId, errorMessage: error.message, timedOut }));
    });

    return taskId;
}
