'use strict';

exports = module.exports = {
    resolve,
};

const assert = require('assert'),
    constants = require('./constants.js'),
    dns = require('dns'),
    safe = require('safetydance'),
    _ = require('underscore');

// a note on TXT records. It doesn't have quotes ("") at the DNS level. Those quotes
// are added for DNS server software to enclose spaces. Such quotes may also be returned
// by the DNS REST API of some providers
async function resolve(hostname, rrtype, options) {
    assert.strictEqual(typeof hostname, 'string');
    assert.strictEqual(typeof rrtype, 'string');
    assert(options && typeof options === 'object');

    const defaultOptions = { server: '127.0.0.1', timeout: 5000 }; // unbound runs on 127.0.0.1
    const resolver = new dns.promises.Resolver();
    options = _.extend({ }, defaultOptions, options);

    // Only use unbound on a Cloudron
    if (constants.CLOUDRON) resolver.setServers([ options.server ]);

    // should callback with ECANCELLED but looks like we might hit https://github.com/nodejs/node/issues/14814
    const timerId = setTimeout(resolver.cancel.bind(resolver), options.timeout || 5000);

    const [error, result] = await safe(resolver.resolve(hostname, rrtype));
    clearTimeout(timerId);

    if (error && error.code === 'ECANCELLED') error.code = 'TIMEOUT';
    if (error) throw error;

    // when you query a random record, it errors with ENOTFOUND. But, if you query a record which has a different type
    // we sometimes get empty array and sometimes ENODATA. for TXT records, result is 2d array of strings
    return result;
}
