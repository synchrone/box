'use strict';

exports = module.exports = {
    proxy,
    restart,
    queueProxy,

    setLocation,
    getLocation
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    debug = require('debug')('box:routes/mailserver'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    mail = require('../mail.js'),
    middleware = require('../middleware/index.js'),
    safe = require('safetydance'),
    services = require('../services.js'),
    url = require('url');

// because of how the proxy middleware works, the http response is already sent by the time this function is called
async function restart(req, res, next) {
    await safe(mail.restartMail(), { debug });
    next();
}

async function proxyToMailContainer(port, pathname, req, res, next) {
    const parsedUrl = url.parse(req.url, true /* parseQueryString */);

    // do not proxy protected values
    delete parsedUrl.query['access_token'];
    delete req.headers['authorization'];
    delete req.headers['cookies'];

    const [error, addonDetails] = await safe(services.getContainerDetails('mail', 'CLOUDRON_MAIL_TOKEN'));
    if (error) return next(BoxError.toHttpError(error));

    parsedUrl.query['access_token'] = addonDetails.token;
    req.url = url.format({ pathname, query: parsedUrl.query });

    const proxyOptions = url.parse(`http://${addonDetails.ip}:${port}`);
    const mailserverProxy = middleware.proxy(proxyOptions);

    req.clearTimeout(); // TODO: add timeout to mail server proxy logic instead of this
    mailserverProxy(req, res, function (error) {
        if (!error) return next();

        if (error.code === 'ECONNREFUSED') return next(new HttpError(424, 'Unable to connect to mail server'));
        if (error.code === 'ECONNRESET') return next(new HttpError(424, 'Unable to query mail server'));

        next(new HttpError(500, error));
    });
}

async function proxy(req, res, next) {
    const pathname = req.path.split('/').pop();

    proxyToMailContainer(3000, pathname, req, res, next);
}

async function queueProxy(req, res, next) {
    proxyToMailContainer(6000, req.path.replace('/', '/queue/'), req, res, next);
}

async function getLocation(req, res, next) {
    const [error, result] = await safe(mail.getLocation());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { domain: result.domain, subdomain: result.subdomain }));
}

async function setLocation(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.domain !== 'string') return next(new HttpError(400, 'domain must be a string'));
    if (typeof req.body.subdomain !== 'string') return next(new HttpError(400, 'subdomain must be a string'));

    const [error, taskId] = await safe(mail.setLocation(req.body.subdomain, req.body.domain, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, { taskId }));
}
