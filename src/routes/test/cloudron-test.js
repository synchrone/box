'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const constants = require('../../constants.js'),
    common = require('./common.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    http = require('http'),
    os = require('os'),
    paths = require('../../paths.js'),
    safe = require('safetydance'),
    superagent = require('superagent'),
    settings = require('../../settings.js');

describe('Cloudron API', function () {
    const { setup, cleanup, serverUrl, owner, user, waitForTask } = common;

    before(setup);
    after(cleanup);

    describe('config', function () {
        it('cannot get config without token', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/config`)
                .ok(() => true);

            expect(response.statusCode).to.equal(401);
        });

        it('can get config (admin)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/config`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body.apiServerOrigin).to.eql('http://localhost:6060');
            expect(response.body.webServerOrigin).to.eql('https://cloudron.io');
            expect(response.body.adminFqdn).to.eql(settings.dashboardFqdn());
            expect(response.body.version).to.eql(constants.VERSION);
            expect(response.body.cloudronName).to.be.a('string');
        });

        it('can get config (non-admin)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/config`)
                .query({ access_token: user.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body.apiServerOrigin).to.eql('http://localhost:6060');
            expect(response.body.webServerOrigin).to.eql('https://cloudron.io');
            expect(response.body.adminFqdn).to.eql(settings.dashboardFqdn());
            expect(response.body.version).to.eql(constants.VERSION);
            expect(response.body.cloudronName).to.be.a('string');
        });
    });

    describe('account setup', function () {
        it('succeeds without pre-set username and display name', async function () {
            const USER = {
                email: 'setup1@account.com',
                password: 'test?!3434543534',
                username: 'setupuser1',
                displayName: 'setup user1',
            };

            const response = await superagent.post(`${serverUrl}/api/v1/users`)
                .query({ access_token: owner.token })
                .send({ email: USER.email });
            expect(response.statusCode).to.equal(201);
            USER.id = response.body.id;

            const response2 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}/invite_link`)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response2.statusCode).to.equal(200);

            const response3 = await superagent.post(`${serverUrl}/api/v1/cloudron/setup_account`)
                .send({
                    inviteToken: require('url').parse(response2.body.inviteLink, true).query.inviteToken,
                    password: USER.password,
                    username: USER.username,
                    displayName: USER.displayName
                })
                .ok(() => true);
            expect(response3.statusCode).to.equal(201);
            expect(response3.body.accessToken).to.be.a('string');

            const response4 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response4.statusCode).to.equal(200);
            expect(response4.body.username).to.equal(USER.username);
            expect(response4.body.displayName).to.equal(USER.displayName);

            const response5 = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: USER.username, password: USER.password });
            expect(response5.statusCode).to.equal(200);
        });

        it('succeeds and overwrites with pre-set username and display name', async function () {
            const USER = {
                email: 'setup2@account.com',
                password: 'test?!3434543534',
                username: 'presetup2',
                displayName: 'setup user2',
            };

            const response = await superagent.post(`${serverUrl}/api/v1/users`)
                .query({ access_token: owner.token })
                .send({ email: USER.email, username: 'presetup2', displayName: 'pre setup' });
            expect(response.statusCode).to.equal(201);
            USER.id = response.body.id;

            const response2 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}/invite_link`)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response2.statusCode).to.equal(200);

            const response3 = await superagent.post(`${serverUrl}/api/v1/cloudron/setup_account`)
                .send({
                    inviteToken: require('url').parse(response2.body.inviteLink, true).query.inviteToken,
                    password: USER.password,
                    username: 'setupuser2', // this will cause a conflict. cannot change username
                    displayName: USER.displayName
                })
                .ok(() => true);
            expect(response3.statusCode).to.equal(409);

            const response4 = await superagent.post(`${serverUrl}/api/v1/cloudron/setup_account`)
                .send({
                    inviteToken: require('url').parse(response2.body.inviteLink, true).query.inviteToken,
                    password: USER.password,
                    displayName: USER.displayName
                })
                .ok(() => true);
            expect(response4.statusCode).to.equal(201);
            expect(response4.body.accessToken).to.be.a('string');

            const response5 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response5.statusCode).to.equal(200);
            expect(response5.body.username).to.equal(USER.username);
            expect(response5.body.displayName).to.equal(USER.displayName);

            const response6 = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: USER.username, password: USER.password });
            expect(response6.statusCode).to.equal(200);
        });

        it('succeeds and does not overwrite pre-set username and display name if profiles are locked', async function () {
            const USER = {
                email: 'setup3@account.com',
                password: 'test?!3434543534',
                username: 'setupuser3',
                displayName: 'setup user3',
            };

            const response0 = await superagent.post(`${serverUrl}/api/v1/settings/profile_config`)
                .query({ access_token: owner.token })
                .send({ lockUserProfiles: true, mandatory2FA: false });
            expect(response0.statusCode).to.equal(200);

            const response = await superagent.post(`${serverUrl}/api/v1/users`)
                .query({ access_token: owner.token })
                .send({ email: USER.email, username: 'presetup3', displayName: 'pre setup3' });
            expect(response.statusCode).to.equal(201);
            USER.id = response.body.id;

            const response2 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}/invite_link`)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response2.statusCode).to.equal(200);

            const response3 = await superagent.post(`${serverUrl}/api/v1/cloudron/setup_account`)
                .send({
                    inviteToken: require('url').parse(response2.body.inviteLink, true).query.inviteToken,
                    password: USER.password,
                    username: USER.username, // ignored
                    displayName: USER.displayName // ignored
                })
                .ok(() => true);
            expect(response3.statusCode).to.equal(201);
            expect(response3.body.accessToken).to.be.a('string');

            const response4 = await superagent.get(`${serverUrl}/api/v1/users/${USER.id}`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response4.statusCode).to.equal(200);
            expect(response4.body.username).to.equal('presetup3'); // what the admin provided
            expect(response4.body.displayName).to.equal('pre setup3'); // what the admin provided

            const response5 = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: 'presetup3', password: USER.password });
            expect(response5.statusCode).to.equal(200);
        });
    });

    describe('login', function () {
        it('cannot login without body', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .ok(() => true);
            expect(response.statusCode).to.equal(400);
        });

        it('cannot login without username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ password: owner.password })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot login without password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: owner.username })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot login with empty username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: '', password: owner.password })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot login with empty password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: owner.username, password: '' })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot login with unknown username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: 'somethingrandom', password: owner.password })
                .ok(() => true);

            expect(response.statusCode).to.equal(401);
        });

        it('cannot login with unknown email', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: 'randomgemail', password: owner.password })
                .ok(() => true);

            expect(response.statusCode).to.equal(401);
        });

        it('cannot login with wrong password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: owner.username, password: owner.password.toUpperCase() })
                .ok(() => true);

            expect(response.statusCode).to.equal(401);
        });

        it('can login with username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: owner.username, password: owner.password });

            expect(response.statusCode).to.equal(200);
            expect(new Date(response.body.expires).toString()).to.not.be('Invalid Date');
            expect(response.body.accessToken).to.be.a('string');
        });

        it('can login with uppercase username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: owner.username.toUpperCase(), password: owner.password });

            expect(response.statusCode).to.equal(200);
            expect(new Date(response.body.expires).toString()).to.not.be('Invalid Date');
            expect(response.body.accessToken).to.be.a('string');
        });

        it('can login with email', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: owner.email, password: owner.password });

            expect(response.statusCode).to.equal(200);
            expect(new Date(response.body.expires).toString()).to.not.be('Invalid Date');
            expect(response.body.accessToken).to.be.a('string');
        });

        it('can login with uppercase email', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/login`)
                .send({ username: owner.email.toUpperCase(), password: owner.password });

            expect(response.statusCode).to.equal(200);
            expect(new Date(response.body.expires).toString()).to.not.be('Invalid Date');
            expect(response.body.accessToken).to.be.a('string');
        });
    });

    describe('logs', function () {
        before(function () {
            console.log(paths.BOX_LOG_FILE);
            fs.writeFileSync(paths.BOX_LOG_FILE, '2022-11-06T15:06:20.009Z box:apphealthmonitor app health: 0 alive / 0 dead.\n', 'utf8');
        });

        it('logStream - requires event-stream accept header', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/logstream/box`)
                .query({ access_token: owner.token, fromLine: 0 })
                .ok(() => true);

            expect(response.statusCode).to.be(400);
        });

        it('logStream - stream logs', function (done) {
            const options = {
                host: 'localhost',
                port: constants.PORT,
                path: '/api/v1/cloudron/logstream/box?lines=10&access_token=' + owner.token,
                headers: { 'Accept': 'text/event-stream', 'Connection': 'keep-alive' }
            };

            // superagent doesn't work. maybe https://github.com/visionmedia/superagent/issues/420
            const req = http.get(options, function (res) {
                let data = '';
                res.on('data', function (d) { data += d.toString('utf8'); });
                setTimeout(function checkData() {
                    let dataMessageFound = false;

                    expect(data.length).to.not.be(0);
                    data.split('\n').forEach(function (line) {
                        if (line.indexOf('id: ') === 0) {
                            expect(parseInt(line.substr('id: '.length), 10)).to.be.a('number');
                        } else if (line.indexOf('data: ') === 0) {
                            const message = JSON.parse(line.slice('data: '.length)).message;
                            if (Array.isArray(message) || typeof message === 'string') dataMessageFound = true;
                        }
                    });

                    expect(dataMessageFound).to.be.ok();

                    res.destroy();
                    req.destroy();
                    done();
                }, 1000);
                res.on('error', done);
            });

            req.on('error', done);
        });
    });

    describe('memory', function () {
        it('cannot get without token', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/memory`)
                .ok(() => true);

            expect(response.statusCode).to.equal(401);
        });

        it('succeeds (admin)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/memory`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body.memory).to.eql(os.totalmem());
            expect(response.body.swap).to.be.a('number');
        });

        it('fails (non-admin)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/memory`)
                .query({ access_token: user.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(403);
        });
    });

    describe('disks', function () {
        it('succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/disks`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body.disks).to.be.ok();
            expect(Object.keys(response.body.disks).some(fs => response.body.disks[fs].mountpoint === '/')).to.be(true);
        });
    });

    describe('disk usage', function () {
        it('get succeeds with no cache', async function () {
            safe.fs.unlinkSync(paths.DISK_USAGE_FILE);

            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/disk_usage`)
                .query({ access_token: owner.token })
                .send({});

            expect(response.statusCode).to.equal(200);
            expect(response.body).to.eql({ usage: null });
        });

        it('update the cache', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/disk_usage`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(201);
            expect(response.body.taskId).to.be.ok();
            await waitForTask(response.body.taskId);
        });

        it('get succeeds with cache', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/disk_usage`)
                .query({ access_token: owner.token })
                .send({});

            expect(response.statusCode).to.equal(200);
            expect(response.body.usage.ts).to.be.a('number');

            const filesystems = Object.keys(response.body.usage.disks);
            let dockerUsage = null;
            for (const fs of filesystems) {
                for (const content of response.body.usage.disks[fs].contents) {
                    if (content.id === 'docker') dockerUsage = content;
                }
            }
            expect(dockerUsage).to.be.ok();
            expect(dockerUsage.usage).to.be.a('number');
        });
    });

    describe('languages', function () {
        it('succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/languages`);

            expect(response.statusCode).to.equal(200);
            expect(response.body.languages).to.be.an('array');
            expect(response.body.languages.indexOf('en')).to.not.equal(-1);
        });
    });
});
