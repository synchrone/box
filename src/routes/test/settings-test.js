'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    constants = require('../../constants.js'),
    expect = require('expect.js'),
    superagent = require('superagent');

const BACKUP_FOLDER = '/tmp/backup_test';

describe('Settings API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    describe('autoupdate_pattern', function () {
        it('can get app auto update pattern (default)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/settings/autoupdate_pattern`)
                .query({ access_token: owner.token });
            expect(response.statusCode).to.equal(200);
            expect(response.body.pattern).to.be.ok();
        });

        it('cannot set autoupdate_pattern without pattern', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/settings/autoupdate_pattern`)
                .query({ access_token: owner.token })
                .ok(() => true);
            expect(response.statusCode).to.equal(400);
        });

        it('can set autoupdate_pattern', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/settings/autoupdate_pattern`)
                .query({ access_token: owner.token })
                .send({ pattern: '00 30 11 * * 1-5' });
            expect(response.statusCode).to.equal(200);
        });

        it('can get auto update pattern', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/settings/autoupdate_pattern`)
                .query({ access_token: owner.token });
            expect(response.statusCode).to.equal(200);
            expect(response.body.pattern).to.be('00 30 11 * * 1-5');
        });

        it('can set autoupdate_pattern to never', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/settings/autoupdate_pattern`)
                .query({ access_token: owner.token })
                .send({ pattern: constants.AUTOUPDATE_PATTERN_NEVER });
            expect(response.statusCode).to.equal(200);
        });

        it('can get auto update pattern', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/settings/autoupdate_pattern`)
                .query({ access_token: owner.token });
            expect(response.statusCode).to.equal(200);
            expect(response.body.pattern).to.be(constants.AUTOUPDATE_PATTERN_NEVER);
        });

        it('cannot set invalid autoupdate_pattern', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/settings/autoupdate_pattern`)
                .query({ access_token: owner.token })
                .send({ pattern: '1 3 x 5 6' })
                .ok(() => true);
            expect(response.statusCode).to.equal(400);
        });
    });

    describe('time_zone', function () {
        it('succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/settings/time_zone`)
                .query({ access_token: owner.token });
            expect(response.statusCode).to.equal(200);
            expect(response.body.timeZone).to.be('America/Los_Angeles');
        });
    });

    describe('user_directory_config', function () {
        // keep in sync with defaults in settings.js
        let defaultConfig = {
            enabled: false,
            secret: '',
            allowlist: ''
        };

        it('can get user_directory_config (default)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/settings/user_directory_config`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body).to.eql(defaultConfig);
        });

        it('cannot set user_directory_config without enabled boolean', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.enabled;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/user_directory_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set user_directory_config without secret', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.secret;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/user_directory_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot enable user_directory_config with empty secret', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = true;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/user_directory_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot enable user_directory_config with empty allowlist', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = true;
            tmp.secret = 'ldapsecret';

            const response = await superagent.post(`${serverUrl}/api/v1/settings/user_directory_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('can enable user_directory_config', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = true;
            tmp.secret = 'ldapsecret';
            tmp.allowlist = '1.2.3.4';

            const response = await superagent.post(`${serverUrl}/api/v1/settings/user_directory_config`)
                .query({ access_token: owner.token })
                .send(tmp);

            expect(response.statusCode).to.equal(200);
        });

        it('can get user_directory_config', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.enabled = true;

            const response = await superagent.get(`${serverUrl}/api/v1/settings/user_directory_config`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body).to.eql({ enabled: true, secret: 'ldapsecret', allowlist: '1.2.3.4' });
        });
    });

    describe('backup_config', function () {
        // keep in sync with defaults in settings.js
        let defaultConfig = {
            provider: 'filesystem',
            backupFolder: '/var/backups',
            format: 'tgz',
            encryption: null,
            retentionPolicy: { keepWithinSecs: 2 * 24 * 60 * 60 }, // 2 days
            schedulePattern: '00 00 23 * * *' // every day at 11pm
        };

        it('can get backup_config (default)', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body).to.eql(defaultConfig);
        });

        it('cannot set backup_config without provider', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.provider;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with invalid provider', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.provider = 'invalid provider';

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config without schedulePattern', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.schedulePattern;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with invalid schedulePattern', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.schedulePattern = 'not a pattern';

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config without format', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.format;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with invalid format', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.format = 'invalid format';

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config without retentionPolicy', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            delete tmp.retentionPolicy;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with invalid retentionPolicy', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.retentionPolicy = 'not an object';

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with empty retentionPolicy', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.retentionPolicy = {};

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with retentionPolicy missing properties', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.retentionPolicy = { foo: 'bar' };

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with retentionPolicy with invalid keepWithinSecs', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.retentionPolicy = { keepWithinSecs: 'not a number' };

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with invalid password', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.password = 1234;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with invalid syncConcurrency', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.syncConcurrency = 'not a number';

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with invalid syncConcurrency', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.syncConcurrency = 0;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set backup_config with invalid acceptSelfSignedCerts', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.acceptSelfSignedCerts = 'not a boolean';

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('can set backup_config', async function () {
            let tmp = JSON.parse(JSON.stringify(defaultConfig));
            tmp.format = 'rsync';
            tmp.backupFolder = BACKUP_FOLDER;

            const response = await superagent.post(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token })
                .send(tmp);

            expect(response.statusCode).to.equal(200);
        });

        it('can get backup_config', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/settings/backup_config`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body.format).to.equal('rsync');
            expect(response.body.backupFolder).to.equal(BACKUP_FOLDER);
        });
    });

    describe('language', function () {
        it('can get default language', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/settings/language`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body.language).to.equal('en');
        });

        it('cannot set language with missing language', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/settings/language`)
                .query({ access_token: owner.token })
                .send({ foo: 'bar' })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set language with invalid language', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/settings/language`)
                .query({ access_token: owner.token })
                .send({ language: 'doesnotexist' })
                .ok(() => true);

            expect(response.statusCode).to.equal(404);
        });

        it('can set language', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/settings/language`)
                .query({ access_token: owner.token })
                .send({ language: 'de' });

            expect(response.statusCode).to.equal(200);
        });

        it('can get language', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/settings/language`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body.language).to.equal('de');
        });
    });
});
