/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const appstore = require('../../appstore.js'),
    common = require('./common.js'),
    constants = require('../../constants.js'),
    expect = require('expect.js'),
    nock = require('nock'),
    settings = require('../../settings.js'),
    superagent = require('superagent');

const { setup, cleanup, serverUrl, owner, appstoreToken } = common;

describe('Appstore Apps API', function () {
    before(setup);
    after(cleanup);

    it('cannot list apps when appstore is down', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/appstore/apps`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.statusCode).to.be(424);
    });

    it('cannot get app with bad token', async function () {
        const scope1 = nock(settings.apiServerOrigin())
            .get(`/api/v1/apps/org.wordpress.cloudronapp?accessToken=${appstoreToken}`)
            .reply(403, {});

        const response = await superagent.get(`${serverUrl}/api/v1/appstore/apps/org.wordpress.cloudronapp`)
            .query({ access_token: owner.token })
            .ok(() => true);

        expect(response.statusCode).to.be(412);
        expect(scope1.isDone()).to.be.ok();
    });

    it('can list apps', async function () {
        const scope1 = nock(settings.apiServerOrigin())
            .get(`/api/v1/apps?accessToken=${appstoreToken}&boxVersion=${constants.VERSION}&unstable=true`, () => true)
            .reply(200, { apps: [] });

        const response = await superagent.get(`${serverUrl}/api/v1/appstore/apps`)
            .query({ access_token: owner.token });

        expect(response.statusCode).to.equal(200);
        expect(scope1.isDone()).to.be.ok();
    });

    it('can get app', async function () {
        const scope1 = nock(settings.apiServerOrigin())
            .get(`/api/v1/apps/org.wordpress.cloudronapp?accessToken=${appstoreToken}`, () => true)
            .reply(200, { apps: [] });

        const response = await superagent.get(`${serverUrl}/api/v1/appstore/apps/org.wordpress.cloudronapp`)
            .query({ access_token: owner.token });

        expect(response.statusCode).to.equal(200);
        expect(scope1.isDone()).to.be.ok();
    });

    it('can get app version', async function () {
        const scope1 = nock(settings.apiServerOrigin())
            .get(`/api/v1/apps/org.wordpress.cloudronapp/versions/3.4.2?accessToken=${appstoreToken}`, () => true)
            .reply(200, { apps: [] });

        const response = await superagent.get(`${serverUrl}/api/v1/appstore/apps/org.wordpress.cloudronapp/versions/3.4.2`)
            .query({ access_token: owner.token });

        expect(response.statusCode).to.equal(200);
        expect(scope1.isDone()).to.be.ok();
    });
});

describe('Appstore Cloudron Registration API - existing user', function () {
    before(async function () {
        await setup();
        await appstore._unregister();
    });
    after(cleanup);

    it('can setup subscription', async function () {
        const scope1 = nock(settings.apiServerOrigin())
            .post('/api/v1/register_user', (body) => body.email && body.password && body.utmSource)
            .reply(201, {});

        const scope2 = nock(settings.apiServerOrigin())
            .post('/api/v1/login', (body) => body.email && body.password)
            .reply(200, { userId: 'userId', accessToken: 'SECRET_TOKEN' });

        const scope3 = nock(settings.apiServerOrigin())
            .post('/api/v1/register_cloudron', (body) => !!body.domain && body.accessToken === 'SECRET_TOKEN')
            .reply(201, { cloudronId: 'cid', cloudronToken: 'CLOUDRON_TOKEN' });

        const response = await superagent.post(`${serverUrl}/api/v1/appstore/register_cloudron`)
            .send({ email: 'test@cloudron.io', password: 'secret', signup: false })
            .query({ access_token: owner.token });

        expect(response.statusCode).to.equal(201);
        expect(scope1.isDone()).to.not.be.ok(); // should not have called register_user since signup is false
        expect(scope2.isDone()).to.be.ok();
        expect(scope3.isDone()).to.be.ok();
        expect(await settings.getAppstoreApiToken()).to.be('CLOUDRON_TOKEN');
        expect(await settings.getAppstoreWebToken()).to.be('SECRET_TOKEN');
        nock.cleanAll();
    });

    it('can get subscription', async function () {
        const scope1 = nock(settings.apiServerOrigin())
            .get('/api/v1/subscription?accessToken=CLOUDRON_TOKEN', () => true)
            .reply(200, { subscription: { plan: { id: 'free' } }, email: 'test@cloudron.io' });

        const response = await superagent.get(`${serverUrl}/api/v1/appstore/subscription`)
            .query({ access_token: owner.token });

        expect(response.statusCode).to.equal(200);
        expect(response.body.email).to.be('test@cloudron.io');
        expect(response.body.subscription).to.be.an('object');
        expect(scope1.isDone()).to.be.ok();
    });
});

describe('Appstore Cloudron Registration API - new user signup', function () {
    before(async function () {
        await setup();
        await appstore._unregister();
    });
    after(cleanup);

    it('can setup subscription', async function () {
        const scope1 = nock(settings.apiServerOrigin())
            .post('/api/v1/register_user', (body) => body.email && body.password && body.utmSource)
            .reply(201, {});

        const scope2 = nock(settings.apiServerOrigin())
            .post('/api/v1/login', (body) => body.email && body.password)
            .reply(200, { userId: 'userId', accessToken: 'SECRET_TOKEN' });

        const scope3 = nock(settings.apiServerOrigin())
            .post('/api/v1/register_cloudron', (body) => !!body.domain && body.accessToken === 'SECRET_TOKEN')
            .reply(201, { cloudronId: 'cid', cloudronToken: 'CLOUDRON_TOKEN' });

        const response = await superagent.post(`${serverUrl}/api/v1/appstore/register_cloudron`)
            .send({ email: 'test@cloudron.io', password: 'secret', signup: true })
            .query({ access_token: owner.token });

        expect(response.statusCode).to.equal(201);
        expect(scope1.isDone()).to.be.ok();
        expect(scope2.isDone()).to.be.ok();
        expect(scope3.isDone()).to.be.ok();
        expect(await settings.getAppstoreApiToken()).to.be('CLOUDRON_TOKEN');
        expect(await settings.getAppstoreWebToken()).to.be('SECRET_TOKEN');
    });

    it('can get subscription', async function () {
        const scope1 = nock(settings.apiServerOrigin())
            .get('/api/v1/subscription?accessToken=CLOUDRON_TOKEN', () => true)
            .reply(200, { subscription: { plan: { id: 'free' } }, email: 'test@cloudron.io' });

        const response = await superagent.get(`${serverUrl}/api/v1/appstore/subscription`)
            .query({ access_token: owner.token });

        expect(response.statusCode).to.equal(200);
        expect(response.body.email).to.be('test@cloudron.io');
        expect(response.body.subscription).to.be.an('object');
        expect(scope1.isDone()).to.be.ok();
    });
});
