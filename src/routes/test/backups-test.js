/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    settings = require('../../settings.js'),
    superagent = require('superagent');

describe('Backups API', function () {
    const { setup, cleanup, waitForTask, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    describe('create', function () {
        before(async function () {
            await settings.setBackupConfig({
                provider: 'filesystem',
                backupFolder: '/tmp/backups',
                format: 'tgz',
                encryption: null,
                retentionPolicy: { keepWithinSecs: 2 * 24 * 60 * 60 }, // 2 days
                schedulePattern: '00 00 23 * * *' // every day at 11pm
            });
        });

        it('fails due to mising token', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/backups/create`)
                .ok(() => true);
            expect(response.statusCode).to.equal(401);
        });

        it('fails due to wrong token', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/backups/create`)
                .query({ access_token: 'randomtoken' })
                .ok(() => true);
            expect(response.statusCode).to.equal(401);
        });

        it('succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/backups/create`)
                .query({ access_token: owner.token });
            expect(response.statusCode).to.equal(202);
            expect(response.body.taskId).to.be.a('string');
            await waitForTask(response.body.taskId);
        });
    });

    describe('list', function () {
        it('succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/backups`)
                .query({ access_token: owner.token });
            expect(response.statusCode).to.equal(200);
            expect(response.body.backups.length).to.be(1);
        });
    });

    describe('update', function () {
        let someBackup;

        before(async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/backups`)
                .query({ access_token: owner.token });
            expect(response.statusCode).to.equal(200);
            expect(response.body.backups.length).to.be(1);
            someBackup = response.body.backups[0];
            console.log(someBackup);
        });

        it('fails for bad param', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/backups/bad_id`)
                .query({ access_token: owner.token })
                .send({ preserveSecs: 'not-a-number', label: 'some string' })
                .ok(() => true);
            expect(response.statusCode).to.equal(400);
        });

        it('fails for unknown backup', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/backups/bad_id`)
                .query({ access_token: owner.token })
                .send({ preserveSecs: 30, label: 'NewOrleans' })
                .ok(() => true);

            expect(response.statusCode).to.equal(404);
        });

        it('succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/backups/${someBackup.id}`)
                .query({ access_token: owner.token })
                .send({ preserveSecs: 30, label: 'NewOrleans' });
            expect(response.statusCode).to.equal(200);
        });
    });
});
