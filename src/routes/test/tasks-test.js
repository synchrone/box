'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('superagent'),
    tasks = require('../../tasks.js');

describe('Tasks API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    it('can get task', async function () {
        const taskId = await tasks.add(tasks._TASK_IDENTITY, [ 'ping' ]);

        return new Promise((resolve) => {
            tasks.startTask(taskId, {}, async function () {
                const response = await superagent.get(`${serverUrl}/api/v1/tasks/${taskId}`)
                    .query({ access_token: owner.token });

                expect(response.statusCode).to.equal(200);
                expect(response.body.percent).to.be(100);
                expect(response.body.args).to.be(undefined);
                expect(response.body.active).to.be(false); // finished
                expect(response.body.success).to.be(true);
                expect(response.body.result).to.be('ping');
                expect(response.body.error).to.be(null);
                resolve();
            });
        });
    });

    it('can get logs', async function () {
        const taskId = await tasks.add(tasks._TASK_CRASH, [ 'ping' ]);

        return new Promise((resolve) => {
            tasks.startTask(taskId, {}, async function () {
                const response = await superagent.get(`${serverUrl}/api/v1/tasks/${taskId}/logs`)
                    .query({ access_token: owner.token });

                expect(response.statusCode).to.equal(200);
                resolve();
            });
        });
    });

    it('cannot stop inactive task', async function () {
        const taskId = await tasks.add(tasks._TASK_IDENTITY, [ 'ping' ]);

        return new Promise((resolve) => {
            tasks.startTask(taskId, {}, async function () {
                const response = await superagent.post(`${serverUrl}/api/v1/tasks/${taskId}/stop`)
                    .query({ access_token: owner.token })
                    .ok(() => true);

                expect(response.statusCode).to.equal(409);
                resolve();
            });
        });
    });


    it('can stop task', async function () {
        const taskId = await tasks.add(tasks._TASK_SLEEP, [ 10000 ]);

        return new Promise((resolve) => {
            tasks.startTask(taskId, {}, async function () {
                const response = await superagent.get(`${serverUrl}/api/v1/tasks/${taskId}`)
                    .query({ access_token: owner.token });

                expect(response.statusCode).to.equal(200);
                expect(response.body.percent).to.be(100);
                expect(response.body.active).to.be(false); // finished
                expect(response.body.success).to.be(false);
                expect(response.body.result).to.be(null);
                expect(response.body.error.message).to.contain('stopped');
                resolve();
            });

            setTimeout(async function () {
                const response = await superagent.post(`${serverUrl}/api/v1/tasks/${taskId}/stop`)
                    .query({ access_token: owner.token });

                expect(response.statusCode).to.equal(204);
            }, 100);
        });
    });

    it('can list tasks', async function () {
        const taskId = await tasks.add(tasks._TASK_IDENTITY, [ 'ping' ]);

        return new Promise((resolve) => {
            tasks.startTask(taskId, {}, async function () {
                const response = await superagent.get(`${serverUrl}/api/v1/tasks?type=${tasks._TASK_IDENTITY}`)
                    .query({ access_token: owner.token });

                expect(response.statusCode).to.equal(200);
                expect(response.body.tasks.length >= 1).to.be(true);
                expect(response.body.tasks[0].id).to.be(taskId);
                expect(response.body.tasks[0].percent).to.be(100);
                expect(response.body.tasks[0].args).to.be(undefined);
                expect(response.body.tasks[0].active).to.be(false); // finished
                expect(response.body.tasks[0].success).to.be(true); // finished
                expect(response.body.tasks[0].result).to.be('ping');
                expect(response.body.tasks[0].error).to.be(null);
                resolve();
            });
        });
    });
});
