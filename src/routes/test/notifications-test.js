'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    expect = require('expect.js'),
    notifications = require('../../notifications.js'),
    superagent = require('superagent');

describe('Notifications API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);
    let notificationIds = [];

    it('can add notifications', async function () {
        for (let i = 0; i < 3; i++) {
            const id = await notifications._add(null, `title ${i}`, `message ${i}`);
            notificationIds.push(id);
        }
    });

    it('cannot get non-existent notification', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/notifications/random`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.statusCode).to.equal(404);
    });

    it('can get notification by id', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/notifications/${notificationIds[0]}`)
            .query({ access_token: owner.token });
        expect(response.statusCode).to.equal(200);
        expect(response.body.id).to.be(notificationIds[0]);
        expect(response.body.title).to.be('title 0');
        expect(response.body.message).to.be('message 0');
        expect(response.body.acknowledged).to.be(false);
    });

    it('can ack notification', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/notifications/${notificationIds[0]}`)
            .query({ access_token: owner.token })
            .send({ acknowledged: true })
            .ok(() => true);
        expect(response.statusCode).to.equal(204);

        const result = await notifications.get(notificationIds[0]);
        expect(result.acknowledged).to.be(true);
    });

    it('can ack non-existent notification', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/notifications/random`)
            .query({ access_token: owner.token })
            .send({ acknowledged: true })
            .ok(() => true);
        expect(response.statusCode).to.equal(404);
    });

    it('can list unread notifications', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/notifications`) // ?acknowledged=false is default
            .query({ access_token: owner.token });
        expect(response.statusCode).to.equal(200);
        expect(response.body.notifications.length).to.be.greaterThan(2);
    });

    it('can list read notifications', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/notifications?acknowledged=true`)
            .query({ access_token: owner.token });
        expect(response.statusCode).to.equal(200);
        expect(response.body.notifications.length).to.be(1);
        expect(response.body.notifications[0].id).to.be(notificationIds[0]);
        expect(response.body.notifications[0].title).to.be('title 0');
    });
});
