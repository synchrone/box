'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const child_process = require('child_process'),
    common = require('./common.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    paths = require('../../paths.js'),
    superagent = require('superagent');

const DOMAIN_0 = {
    domain: 'domain0.com',
    zoneName: 'domain0.com',
    provider: 'noop',
    config: { },
    tlsConfig: {
        provider: 'fallback'
    }
};

const DOMAIN_1 = {
    domain: 'domain1.com',
    provider: 'noop',
    config: { },
    tlsConfig: {
        provider: 'fallback'
    }
};

describe('Domains API', function () {
    const { setup, cleanup, serverUrl, owner, dashboardDomain } = common;

    before(setup);
    after(cleanup);

    describe('add', function () {
        it('fails with missing domain', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ access_token: owner.token })
                .send({})
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails with invalid domain', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ access_token: owner.token })
                .send({ domain: 'abc' })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails with unknown provider', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ access_token: owner.token })
                .send({ domain: 'cloudron.com', provider: 'doesnotexist', config: { }})
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails with invalid tlsConfig', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ access_token: owner.token })
                .send({ domain: 'cloudron.com', provider: 'noop', config: { }, tlsConfig: 'foobar' })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails with unknown tls provider', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ access_token: owner.token })
                .send({ domain: 'cloudron.com', provider: 'noop', config: { }, tlsConfig: { provider: 'hello' }})
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails without token', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ })
                .send(DOMAIN_0)
                .ok(() => true);

            expect(response.statusCode).to.equal(401);
        });

        it('succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ access_token: owner.token })
                .send(DOMAIN_0);

            expect(response.statusCode).to.equal(201);
        });

        it('succeeds for second domain without zoneName', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ access_token: owner.token })
                .send(DOMAIN_1);

            expect(response.statusCode).to.equal(201);
        });

        it('fails for already added domain', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains`)
                .query({ access_token: owner.token })
                .send(DOMAIN_0)
                .ok(() => true);

            expect(response.statusCode).to.equal(409);
        });
    });

    describe('list', function () {
        it('succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/domains`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body.domains).to.be.an(Array);
            expect(response.body.domains.length).to.equal(3);

            expect(response.body.domains[0].domain).to.equal(DOMAIN_0.domain);
            expect(response.body.domains[1].domain).to.equal(DOMAIN_1.domain);
            expect(response.body.domains[2].domain).to.equal(dashboardDomain);
        });
    });

    describe('get', function () {
        it('fails for non-existing domain', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/domains/random`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(404);
        });

        it('succeeds', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(200);
            expect(response.body.domain).to.equal(DOMAIN_0.domain);
        });
    });

    describe('update', function () {
        it('config fails for non-existing domain', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains/whatever/update`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(404);
        });

        it('config succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}/config`)
                .query({ access_token: owner.token })
                .send(DOMAIN_0);

            expect(response.statusCode).to.equal(204);
        });

        it('wellknown succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}/wellknown`)
                .query({ access_token: owner.token })
                .send({ wellKnown: null });

            expect(response.statusCode).to.equal(204);
        });

        it('wellknown succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}/wellknown`)
                .query({ access_token: owner.token })
                .send({ wellKnown: { service: 'some.service' } });

            expect(response.statusCode).to.equal(204);
        });
    });

    describe('Certificates API', function () {
        let validCert0, validKey0, // example.com
            validCert1, validKey1; // *.example.com

        before(async function () {
            child_process.execSync(`openssl req -subj "/CN=${DOMAIN_0.domain}/O=My Company Name LTD./C=US" -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout /tmp/server.key -out /tmp/server.crt`);
            validKey0 = fs.readFileSync('/tmp/server.key', 'utf8');
            validCert0 = fs.readFileSync('/tmp/server.crt', 'utf8');

            child_process.execSync(`openssl req -subj "/CN=*.${DOMAIN_0.domain}/O=My Company Name LTD./C=US" -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout /tmp/server.key -out /tmp/server.crt`);
            validKey1 = fs.readFileSync('/tmp/server.key', 'utf8');
            validCert1 = fs.readFileSync('/tmp/server.crt', 'utf8');
        });

        it('cannot set certificate without certificate', async function () {
            let d = Object.assign({}, DOMAIN_0);
            d.fallbackCertificate = { key: validKey1 };

            const response = await superagent.post(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}/config`)
                .query({ access_token: owner.token })
                .send(d)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set certificate without key', async function () {
            let d = Object.assign({}, DOMAIN_0);
            d.fallbackCertificate = { cert: validCert1 };

            const response = await superagent.post(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}/config`)
                .query({ access_token: owner.token })
                .send(d)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set certificate with cert not being a string', async function () {
            let d = Object.assign({}, DOMAIN_0);
            d.fallbackCertificate = { cert: 1234, key: validKey1 };

            const response = await superagent.post(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}/config`)
                .query({ access_token: owner.token })
                .send(d)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set certificate with key not being a string', async function () {
            let d = Object.assign({}, DOMAIN_0);
            d.fallbackCertificate = { cert: validCert1, key: true };

            const response = await superagent.post(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}/config`)
                .query({ access_token: owner.token })
                .send(d)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('cannot set non-fallback certificate', async function () {
            let d = Object.assign({}, DOMAIN_0);
            d.fallbackCertificate = { cert: validCert0, key: validKey0 };

            const response = await superagent.post(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}/config`)
                .query({ access_token: owner.token })
                .send(d)
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('can set fallback certificate', async function () {
            let d = Object.assign({}, DOMAIN_0);
            d.fallbackCertificate = { cert: validCert1, key: validKey1 };

            const response = await superagent.post(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}/config`)
                .query({ access_token: owner.token })
                .send(d);

            expect(response.statusCode).to.equal(204);
        });

        it('did set the certificate', async function () {
            const cert = fs.readFileSync(path.join(paths.NGINX_CERT_DIR, `${DOMAIN_0.domain}.host.cert`), 'utf-8');
            expect(cert).to.eql(validCert1);

            const key = fs.readFileSync(path.join(paths.NGINX_CERT_DIR, `${DOMAIN_0.domain}.host.key`), 'utf-8');
            expect(key).to.eql(validKey1);
        });
    });

    describe('delete', function () {
        it('fails for non-existing domain', async function () {
            const response = await superagent.del(`${serverUrl}/api/v1/domains/random`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(404);
        });

        it('succeeds', async function () {
            const response = await superagent.del(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}`)
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(204);

            const response2 = await superagent.get(`${serverUrl}/api/v1/domains/${DOMAIN_0.domain}`)
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response2.statusCode).to.equal(404);
        });
    });
});
