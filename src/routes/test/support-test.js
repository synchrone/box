/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const common = require('./common.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    nock = require('nock'),
    support = require('../../support.js'),
    superagent = require('superagent');

describe('Support API', function () {
    const { setup, cleanup, serverUrl, owner, mockApiServerOrigin, appstoreToken } = common;

    before(setup);
    after(cleanup);

    const authorizedKeysFile = support._sshInfo().filePath;

    describe('remote support', function () {
        it('get remote support', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/support/remote_support`)
                .query({ access_token: owner.token });
            expect(response.statusCode).to.equal(200);
            expect(response.body.enabled).to.be(false);
        });

        it('enable remote support', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/remote_support`)
                .query({ access_token: owner.token })
                .send({ enable: true });

            expect(response.statusCode).to.equal(202);

            let data = await fs.promises.readFile(authorizedKeysFile, 'utf8');
            let count = (data.match(/support@cloudron.io/g) || []).length;
            expect(count).to.be(1);
        });

        it('returns true when remote support enabled', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/support/remote_support`)
                .query({ access_token: owner.token });
            expect(response.statusCode).to.equal(200);
            expect(response.body.enabled).to.be(true);
        });

        it('enable remote support (again)', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/remote_support`)
                .query({ access_token: owner.token })
                .send({ enable: true });
            expect(response.statusCode).to.equal(202);

            let data = await fs.promises.readFile(authorizedKeysFile, 'utf8');
            let count = (data.match(/support@cloudron.io/g) || []).length;
            expect(count).to.be(1);
        });

        it('disable remote support', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/remote_support`)
                .query({ access_token: owner.token })
                .send({ enable: false });

            expect(response.statusCode).to.equal(202);

            let data = await fs.promises.readFile(authorizedKeysFile, 'utf8');
            let count = (data.match(/support@cloudron.io/g) || []).length;
            expect(count).to.be(0);
        });

        it('disable remote support (again)', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/remote_support`)
                .query({ access_token: owner.token })
                .send({ enable: false });

            expect(response.statusCode).to.equal(202);

            const data = await fs.promises.readFile(authorizedKeysFile, 'utf8');
            const count = (data.match(/support@cloudron.io/g) || []).length;
            expect(count).to.be(0);
        });
    });

    describe('ticket', function () {
        it('fails without token', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ type: 'ticket', subject: 'some subject', description: 'some description' })
                .ok(() => true);

            expect(response.statusCode).to.equal(401);
        });

        it('fails without type', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ subject: 'some subject', description: 'some description' })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails with empty type', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ type: '', subject: 'some subject', description: 'some description' })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails with unknown type', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ type: 'foobar', subject: 'some subject', description: 'some description' })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails without description', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ type: 'ticket', subject: 'some subject' })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails with empty subject', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ type: 'ticket', subject: '', description: 'some description' })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails with empty description', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ type: 'ticket', subject: 'some subject', description: '' })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('fails without subject', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ type: 'ticket', description: 'some description' })
                .query({ access_token: owner.token })
                .ok(() => true);

            expect(response.statusCode).to.equal(400);
        });

        it('succeeds with ticket type', async function () {
            const scope2 = nock(mockApiServerOrigin)
                .filteringRequestBody(function (/* unusedBody */) { return ''; }) // strip out body
                .post(`/api/v1/ticket?accessToken=${appstoreToken}`)
                .reply(201, { });

            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ type: 'ticket', subject: 'some subject', description: 'some description' })
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(201);
            expect(scope2.isDone()).to.be.ok();
        });

        it('succeeds with app type', async function () {
            const scope2 = nock(mockApiServerOrigin)
                .filteringRequestBody(function (/* unusedBody */) { return ''; }) // strip out body
                .post(`/api/v1/ticket?accessToken=${appstoreToken}`)
                .reply(201, { });

            const response = await superagent.post(`${serverUrl}/api/v1/support/ticket`)
                .send({ type: 'app_missing', subject: 'some subject', description: 'some description' })
                .query({ access_token: owner.token });

            expect(response.statusCode).to.equal(201);
            expect(scope2.isDone()).to.be.ok();
        });
    });
});
