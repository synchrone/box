'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js');

const delay = require('../../delay.js'),
    expect = require('expect.js'),
    superagent = require('superagent');

const DOMAIN = 'example-server-test.com';

describe('Provision', function () {
    const { setupServer, cleanup, serverUrl, owner } = common;

    before(setupServer);
    after(cleanup);

    async function waitForSetup() {
        let response;
        for (let times = 0; times < 5; ++times) {
            response = await superagent.get(`${serverUrl}/api/v1/cloudron/status`)
                .ok(() => true);

            if (response.status === 200 && !response.body.setup.active && response.body.setup.errorMessage === '' && response.body.adminFqdn) return;

            await delay(4000);
        }

        throw new Error(`Setup did not complete. status: ${response.status} body: ${JSON.stringify(response.body)}`);
    }

    describe('DNS Setup', async function () {
        it('fails without provider', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { domain: DOMAIN, config: {} } })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails with invalid provider', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'foobar', domain: DOMAIN, config: {} } })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails with missing domain', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'noop', config: {} } })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails with invalid domain', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'noop', domain: '.foo', config: {} } })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails with invalid config', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'noop', domain: DOMAIN, config: 'not an object' } })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails with invalid zoneName', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'noop', domain: DOMAIN, config: {}, zoneName: 1337 } })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails with invalid tlsConfig', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'noop', domain: DOMAIN, config: {}, tlsConfig: 'foobar' } })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails with invalid tlsConfig provider', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'noop', domain: DOMAIN, config: {}, tlsConfig: { provider: 1337 } } })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'noop', domain: DOMAIN, adminFqdn: 'my.' + DOMAIN, config: {}, tlsConfig: { provider: 'fallback' } } })
                .ok(() => true);

            expect(response.statusCode).to.eql(200);

            await waitForSetup();
        });

        it('twice succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'noop', domain: DOMAIN, adminFqdn: 'my.' + DOMAIN, config: {}, tlsConfig: { provider: 'fallback' } } })
                .ok(() => true);

            expect(response.statusCode).to.eql(200);

            await waitForSetup();
        });
    });

    describe('Activation', function () {
        it('device is in first time mode', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/status`);
            expect(response.statusCode).to.equal(200);
            expect(response.body.activated).to.not.be.ok();
        });

        it('fails without username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ password: owner.password, email: owner.email })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails with invalid username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: '?this.is-not!valid', password: owner.password, email: owner.email })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails due to empty username', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: '', password: owner.password, email: owner.email })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails without email', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: owner.username, password: owner.password })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails due to empty email', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: owner.username, password: owner.password, email: '' })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails due to invalid email', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: owner.username, password: owner.password, email: 'invalidemail' })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails without password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: owner.password.username, email: owner.email })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails due to empty password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: owner.username, password: '', email: owner.email })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails with invalid password', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: owner.username, password: 'short', email: owner.email })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('fails due to wrong displayName type', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: owner.username, password: owner.password, email: owner.email, displayName: 1234 })
                .ok(() => true);

            expect(response.statusCode).to.eql(400);
        });

        it('succeeds', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: owner.username, password: owner.password, email: owner.email, displayName: owner.displayName });

            expect(response.statusCode).to.equal(201);
            expect(response.body.token).to.be.a('string');
        });

        it('fails the second time', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
                .send({ username: owner.username, password: owner.password, email: owner.email, displayName: owner.displayName })
                .ok(() => true);

            expect(response.statusCode).to.eql(409);
        });

        it('after fails', async function () {
            const response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
                .send({ domainConfig: { provider: 'noop', domain: DOMAIN, adminFqdn: 'my.' + DOMAIN, config: {}, tlsConfig: { provider: 'fallback' } } })
                .ok(() => true);

            expect(response.statusCode).to.eql(409);
        });

        it('device left first time mode', async function () {
            const response = await superagent.get(`${serverUrl}/api/v1/cloudron/status`);
            expect(response.statusCode).to.equal(200);
            expect(response.body.activated).to.be.ok();
        });
    });
});

