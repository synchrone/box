'use strict';

const constants = require('../../constants.js'),
    database = require('../../database.js'),
    delay = require('../../delay.js'),
    expect = require('expect.js'),
    fs = require('fs'),
    mailer = require('../../mailer.js'),
    safe = require('safetydance'),
    server = require('../../server.js'),
    settings = require('../../settings.js'),
    support = require('../../support.js'),
    superagent = require('superagent'),
    tasks = require('../../tasks.js'),
    tokens = require('../../tokens.js');

exports = module.exports = {
    setup,
    setupServer,
    cleanup,
    clearMailQueue,
    checkMails,
    waitForTask,

    owner: {
        id: null,
        username: 'superadmin',
        password: 'Foobar?1337',
        email: 'superadmin@cloudron.local',
        displayName: 'Super Admin',
        token: null
    },

    user: {
        id: null,
        username: 'user',
        password: 'Foobar?1338',
        email: 'user@cloudron.local',
        token: null
    },

    mockApiServerOrigin: 'http://localhost:6060',
    dashboardDomain: 'test.example.com',
    dashboardFqdn: 'my.test.example.com',
    appstoreToken: 'toktok',

    serverUrl: `http://localhost:${constants.PORT}`,
};

async function setupServer() {
    await server.start();
    await database._clear();
    await settings._setApiServerOrigin(exports.mockApiServerOrigin);
}

async function setup() {
    const owner = exports.owner, serverUrl = exports.serverUrl, user = exports.user;

    await setupServer();
    await safe(fs.promises.unlink(support._sshInfo().filePath));

    // setup
    let response = await superagent.post(`${serverUrl}/api/v1/cloudron/setup`)
        .send({ domainConfig: { provider: 'noop', domain: exports.dashboardDomain, config: {}, tlsConfig: { provider: 'fallback' } } });
    expect(response.status).to.eql(200);

    await delay(2000);

    // create admin
    response = await superagent.post(`${serverUrl}/api/v1/cloudron/activate`)
        .query({ setupToken: 'somesetuptoken' })
        .send({ username: owner.username, password: owner.password, email: owner.email });
    expect(response.status).to.eql(201);
    owner.token = response.body.token;
    owner.id = response.body.userId;

    // create user
    response = await superagent.post(`${serverUrl}/api/v1/users`)
        .query({ access_token: owner.token })
        .send({ username: user.username, email: user.email, password: user.password });
    expect(response.status).to.equal(201);
    user.id = response.body.id;
    // HACK to get a token for second user (passwords are generated and the user should have gotten a password setup link...)
    const token = await tokens.add({ identifier: user.id, clientId: 'test-client-id', expires: Date.now() + (60 * 60 * 1000), name: 'fromtest' });
    user.token = token.accessToken;

    await settings._set(settings.APPSTORE_API_TOKEN_KEY, exports.appstoreToken); // appstore token
}

async function cleanup() {
    await database._clear();
    await server.stop();
}

function clearMailQueue() {
    mailer._mailQueue = [];
}

async function checkMails(number) {
    await delay(1000);
    expect(mailer._mailQueue.length).to.equal(number);
    clearMailQueue();
}

async function waitForTask(taskId) {
    // eslint-disable-next-line no-constant-condition
    for (let i = 0; i < 10; i++) {
        const result = await tasks.get(taskId);
        expect(result).to.not.be(null);
        if (!result.active) {
            if (result.success) return result;
            throw new Error(`Task ${taskId} failed: ${result.error.message} - ${result.error.stack}`);
        }
        await delay(2000);
        console.log(`Waiting for task to ${taskId} finish`);
    }
    throw new Error(`Task ${taskId} never finished`);
}
