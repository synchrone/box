'use strict';

/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

const common = require('./common.js'),
    expect = require('expect.js'),
    superagent = require('superagent');

describe('Tokens API', function () {
    const { setup, cleanup, serverUrl, owner } = common;

    before(setup);
    after(cleanup);

    let token, readOnlyToken;

    it('cannot create token with bad name', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
            .query({ access_token: owner.token })
            .send({ name: new Array(128).fill('s').join('') })
            .ok(() => true);
        expect(response.statusCode).to.equal(400);
    });

    it('can create token', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
            .query({ access_token: owner.token })
            .send({ name: 'mytoken1' });

        expect(response.status).to.equal(201);
        expect(response.body).to.be.a('object');
        token = response.body;
    });

    it('can create read-only token', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
            .query({ access_token: owner.token })
            .send({ name: 'mytoken1', scope: { '*': 'r' }});

        expect(response.status).to.equal(201);
        expect(response.body).to.be.a('object');
        readOnlyToken = response.body;
    });

    it('cannot create read-only token with invalid scope', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
            .query({ access_token: owner.token })
            .send({ name: 'mytoken1', scope: { 'foobar': 'rw' }})
            .ok(() => true);

        expect(response.status).to.equal(400);
    });

    it('can list tokens', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/tokens`)
            .query({ access_token: owner.token });
        expect(response.statusCode).to.equal(200);
        expect(response.body.tokens.length).to.be(3); // one is owner token on activation
        const tokenIds = response.body.tokens.map(t => t.id);
        expect(tokenIds).to.contain(token.id);
        expect(tokenIds).to.contain(readOnlyToken.id);
    });

    it('cannot create token with read only token', async function () {
        const response = await superagent.post(`${serverUrl}/api/v1/tokens`)
            .query({ access_token: readOnlyToken.accessToken })
            .send({ name: 'somename' })
            .ok(() => true);

        expect(response.status).to.equal(403);
    });

    it('cannot get non-existent token', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/tokens/foobar`)
            .query({ access_token: owner.token })
            .ok(() => true);
        expect(response.statusCode).to.equal(404);
    });

    it('can get token', async function () {
        const response = await superagent.get(`${serverUrl}/api/v1/tokens/${token.id}`)
            .query({ access_token: owner.token });
        expect(response.statusCode).to.equal(200);
        expect(response.body.id).to.be(token.id);
    });

    it('can delete token', async function () {
        const response = await superagent.del(`${serverUrl}/api/v1/tokens/${token.id}`)
            .query({ access_token: owner.token });
        expect(response.statusCode).to.equal(204);
    });
});
