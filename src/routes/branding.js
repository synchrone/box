'use strict';

exports = module.exports = {
    get,
    set,

    getCloudronAvatar
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance'),
    settings = require('../settings.js'),
    _ = require('underscore');

async function getFooter(req, res, next) {
    const [error, footer] = await safe(settings.getFooter());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { footer }));
}

async function setFooter(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.footer !== 'string') return next(new HttpError(400, 'footer is required'));

    const [error] = await safe(settings.setFooter(req.body.footer));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function setCloudronName(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name is required'));

    const [error] = await safe(settings.setCloudronName(req.body.name));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getCloudronName(req, res, next) {
    const [error, name] = await safe(settings.getCloudronName());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { name }));
}

async function setAppstoreListingConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    const listingConfig = _.pick(req.body, 'whitelist', 'blacklist');
    if (Object.keys(listingConfig).length === 0) return next(new HttpError(400, 'blacklist or whitelist is required'));

    if ('whitelist' in listingConfig) {
        if (listingConfig.whitelist !== null && !Array.isArray(listingConfig.whitelist)) return next(new HttpError(400, 'whitelist is null or an array of strings'));

        if (listingConfig.whitelist && !listingConfig.whitelist.every(id => typeof id === 'string')) return next(new HttpError(400, 'whitelist must be array of strings'));
    }

    if ('blacklist' in listingConfig) {
        if (!Array.isArray(listingConfig.blacklist)) return next(new HttpError(400, 'blacklist an array of strings'));

        if (!listingConfig.blacklist.every(id => typeof id === 'string')) return next(new HttpError(400, 'blacklist must be array of strings'));
    }

    const [error] = await safe(settings.setAppstoreListingConfig(listingConfig));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getAppstoreListingConfig(req, res, next) {
    const [error, listingConfig] = await safe(settings.getAppstoreListingConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, listingConfig));
}

async function setCloudronAvatar(req, res, next) {
    assert.strictEqual(typeof req.files, 'object');

    if (!req.files.avatar) return next(new HttpError(400, 'avatar must be provided'));
    const avatar = safe.fs.readFileSync(req.files.avatar.path);
    if (!avatar) return next(500, safe.error.message);

    const [error] = await safe(settings.setCloudronAvatar(avatar));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getCloudronAvatar(req, res, next) {
    const [error, avatar] = await safe(settings.getCloudronAvatar());
    if (error) return next(BoxError.toHttpError(error));

    // avoid caching the avatar on the client to see avatar changes immediately
    res.set('Cache-Control', 'no-cache');

    res.set('Content-Type', 'image/png');
    res.status(200).send(avatar);
}

async function get(req, res, next) {
    assert.strictEqual(typeof req.params.setting, 'string');

    switch (req.params.setting) {
    case settings.APPSTORE_LISTING_CONFIG_KEY: return await getAppstoreListingConfig(req, res, next);
    case settings.CLOUDRON_AVATAR_KEY: return await getCloudronAvatar(req, res, next);
    case settings.CLOUDRON_NAME_KEY: return await getCloudronName(req, res, next);
    case settings.FOOTER_KEY: return await getFooter(req, res, next);

    default: return next(new HttpError(404, 'No such setting'));
    }
}

async function set(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    switch (req.params.setting) {
    case settings.APPSTORE_LISTING_CONFIG_KEY: return await setAppstoreListingConfig(req, res, next);
    case settings.CLOUDRON_AVATAR_KEY: return await setCloudronAvatar(req, res, next);
    case settings.CLOUDRON_NAME_KEY: return await setCloudronName(req, res, next);
    case settings.FOOTER_KEY: return await setFooter(req, res, next);

    default: return next(new HttpError(404, 'No such branding'));
    }
}
