'use strict';

exports = module.exports = {
    set,
    get,

    // owner only settings
    setBackupConfig,
};

const assert = require('assert'),
    backups = require('../backups.js'),
    BoxError = require('../boxerror.js'),
    docker = require('../docker.js'),
    externalLdap = require('../externalldap.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance'),
    settings = require('../settings.js');

async function getAutoupdatePattern(req, res, next) {
    const [error, pattern] = await safe(settings.getAutoupdatePattern());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { pattern }));
}

async function setAutoupdatePattern(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.pattern !== 'string') return next(new HttpError(400, 'pattern is required'));

    const [error] = await safe(settings.setAutoupdatePattern(req.body.pattern));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getTimeZone(req, res, next) {
    const [error, timeZone] = await safe(settings.getTimeZone());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { timeZone }));
}

async function setTimeZone(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.timeZone !== 'string') return next(new HttpError(400, 'timeZone is required'));

    const [error] = await safe(settings.setTimeZone(req.body.timeZone));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getSupportConfig(req, res, next) {
    const [error, supportConfig] = await safe(settings.getSupportConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, supportConfig));
}

async function getBackupConfig(req, res, next) {
    const [error, backupConfig] = await safe(settings.getBackupConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, backups.removePrivateFields(backupConfig)));
}

async function setBackupConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider is required'));
    if (typeof req.body.schedulePattern !== 'string') return next(new HttpError(400, 'schedulePattern is required'));
    if ('password' in req.body && typeof req.body.password !== 'string') return next(new HttpError(400, 'password must be a string'));
    if ('encryptedFilenames' in req.body && typeof req.body.encryptedFilenames !== 'boolean') return next(new HttpError(400, 'encryptedFilenames must be a boolean'));

    if ('syncConcurrency' in req.body) {
        if (typeof req.body.syncConcurrency !== 'number') return next(new HttpError(400, 'syncConcurrency must be a positive integer'));
        if (req.body.syncConcurrency < 1) return next(new HttpError(400, 'syncConcurrency must be a positive integer'));
    }
    if ('copyConcurrency' in req.body) {
        if (typeof req.body.copyConcurrency !== 'number') return next(new HttpError(400, 'copyConcurrency must be a positive integer'));
        if (req.body.copyConcurrency < 1) return next(new HttpError(400, 'copyConcurrency must be a positive integer'));
    }
    if ('downloadConcurrency' in req.body) {
        if (typeof req.body.downloadConcurrency !== 'number') return next(new HttpError(400, 'downloadConcurrency must be a positive integer'));
        if (req.body.downloadConcurrency < 1) return next(new HttpError(400, 'downloadConcurrency must be a positive integer'));
    }
    if ('deleteConcurrency' in req.body) {
        if (typeof req.body.deleteConcurrency !== 'number') return next(new HttpError(400, 'deleteConcurrency must be a positive integer'));
        if (req.body.deleteConcurrency < 1) return next(new HttpError(400, 'deleteConcurrency must be a positive integer'));
    }
    if ('uploadPartSize' in req.body) {
        if (typeof req.body.uploadPartSize !== 'number') return next(new HttpError(400, 'uploadPartSize must be a positive integer'));
        if (req.body.uploadPartSize < 1) return next(new HttpError(400, 'uploadPartSize must be a positive integer'));
    }

    if ('memoryLimit' in req.body && typeof req.body.memoryLimit !== 'number') return next(new HttpError(400, 'memoryLimit must be a positive integer'));
    if (typeof req.body.format !== 'string') return next(new HttpError(400, 'format must be a string'));
    if ('acceptSelfSignedCerts' in req.body && typeof req.body.acceptSelfSignedCerts !== 'boolean') return next(new HttpError(400, 'format must be a boolean'));

    if (!req.body.retentionPolicy || typeof req.body.retentionPolicy !== 'object') return next(new HttpError(400, 'retentionPolicy is required'));

    if ('mountOptions' in req.body && typeof req.body.mountOptions !== 'object') return next(new HttpError(400, 'mountOptions must be a object'));

    // testing the backup using put/del takes a bit of time at times
    req.clearTimeout();

    const [error] = await safe(settings.setBackupConfig(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getExternalLdapConfig(req, res, next) {
    const [error, config] = await safe(settings.getExternalLdapConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, externalLdap.removePrivateFields(config)));
}

async function setExternalLdapConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.provider || typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider must be a string'));
    if ('url' in req.body && typeof req.body.url !== 'string') return next(new HttpError(400, 'url must be a string'));
    if ('baseDn' in req.body && typeof req.body.baseDn !== 'string') return next(new HttpError(400, 'baseDn must be a string'));
    if ('usernameField' in req.body && typeof req.body.usernameField !== 'string') return next(new HttpError(400, 'usernameField must be a string'));
    if ('filter' in req.body && typeof req.body.filter !== 'string') return next(new HttpError(400, 'filter must be a string'));
    if ('groupBaseDn' in req.body && typeof req.body.groupBaseDn !== 'string') return next(new HttpError(400, 'groupBaseDn must be a string'));
    if ('bindDn' in req.body && typeof req.body.bindDn !== 'string') return next(new HttpError(400, 'bindDn must be a non empty string'));
    if ('bindPassword' in req.body && typeof req.body.bindPassword !== 'string') return next(new HttpError(400, 'bindPassword must be a string'));

    const [error] = await safe(settings.setExternalLdapConfig(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getDirectoryServerConfig(req, res, next) {
    const [error, config] = await safe(settings.getDirectoryServerConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, config));
}

async function setDirectoryServerConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enabled !== 'boolean') return next(new HttpError(400, 'enabled must be a boolean'));
    if (typeof req.body.secret !== 'string') return next(new HttpError(400, 'secret must be a string'));
    if ('allowlist' in req.body && typeof req.body.allowlist !== 'string') return next(new HttpError(400, 'allowlist must be a string'));

    const [error] = await safe(settings.setDirectoryServerConfig(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getDynamicDnsConfig(req, res, next) {
    const [error, enabled] = await safe(settings.getDynamicDnsConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { enabled }));
}

async function setDynamicDnsConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enabled !== 'boolean') return next(new HttpError(400, 'enabled boolean is required'));

    const [error] = await safe(settings.setDynamicDnsConfig(req.body.enabled));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getIPv6Config(req, res, next) {
    const [error, ipv6Config] = await safe(settings.getIPv6Config());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, ipv6Config));
}

async function setIPv6Config(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.provider || typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider is required'));

    const [error] = await safe(settings.setIPv6Config(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getUnstableAppsConfig(req, res, next) {
    const [error, enabled] = await safe(settings.getUnstableAppsConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { enabled }));
}

async function setUnstableAppsConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enabled !== 'boolean') return next(new HttpError(400, 'enabled boolean is required'));

    const [error] = await safe(settings.setUnstableAppsConfig(req.body.enabled));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getRegistryConfig(req, res, next) {
    const [error, registryConfig] = await safe(settings.getRegistryConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, docker.removePrivateFields(registryConfig)));
}

async function setRegistryConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.provider || typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider is required'));
    if (req.body.provider !== 'noop') {
        if (typeof req.body.serverAddress !== 'string') return next(new HttpError(400, 'serverAddress is required'));
        if ('username' in req.body && typeof req.body.username !== 'string') return next(new HttpError(400, 'username is required'));
        if ('email' in req.body && typeof req.body.email !== 'string') return next(new HttpError(400, 'email is required'));
        if ('password' in req.body && typeof req.body.password !== 'string') return next(new HttpError(400, 'password is required'));
    }

    const [error] = await safe(settings.setRegistryConfig(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200));
}

async function getProfileConfig(req, res, next) {
    const [error, directoryConfig] = await safe(settings.getProfileConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, directoryConfig));
}

async function setProfileConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.lockUserProfiles !== 'boolean') return next(new HttpError(400, 'lockUserProfiles is required'));
    if (typeof req.body.mandatory2FA !== 'boolean') return next(new HttpError(400, 'mandatory2FA is required'));

    const [error] = await safe(settings.setProfileConfig(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getSysinfoConfig(req, res, next) {
    const [error, sysinfoConfig] = await safe(settings.getSysinfoConfig());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, sysinfoConfig));
}

async function setSysinfoConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.provider || typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider is required'));

    const [error] = await safe(settings.setSysinfoConfig(req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getLanguage(req, res, next) {
    const [error, language] = await safe(settings.getLanguage());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { language }));
}

async function setLanguage(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.language || typeof req.body.language !== 'string') return next(new HttpError(400, 'language is required'));

    const [error] = await safe(settings.setLanguage(req.body.language));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

function get(req, res, next) {
    assert.strictEqual(typeof req.params.setting, 'string');

    switch (req.params.setting) {
    case settings.DYNAMIC_DNS_KEY: return getDynamicDnsConfig(req, res, next);
    case settings.IPV6_CONFIG_KEY: return getIPv6Config(req, res, next);
    case settings.BACKUP_CONFIG_KEY: return getBackupConfig(req, res, next);
    case settings.EXTERNAL_LDAP_KEY: return getExternalLdapConfig(req, res, next);
    case settings.DIRECTORY_SERVER_KEY: return getDirectoryServerConfig(req, res, next);
    case settings.UNSTABLE_APPS_KEY: return getUnstableAppsConfig(req, res, next);
    case settings.REGISTRY_CONFIG_KEY: return getRegistryConfig(req, res, next);
    case settings.SYSINFO_CONFIG_KEY: return getSysinfoConfig(req, res, next);
    case settings.LANGUAGE_KEY: return getLanguage(req, res, next);

    case settings.AUTOUPDATE_PATTERN_KEY: return getAutoupdatePattern(req, res, next);
    case settings.TIME_ZONE_KEY: return getTimeZone(req, res, next);

    case settings.PROFILE_CONFIG_KEY: return getProfileConfig(req, res, next);
    case settings.SUPPORT_CONFIG_KEY: return getSupportConfig(req, res, next);

    default: return next(new HttpError(404, 'No such setting'));
    }
}

function set(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    switch (req.params.setting) {
    case settings.DYNAMIC_DNS_KEY: return setDynamicDnsConfig(req, res, next);
    case settings.IPV6_CONFIG_KEY: return setIPv6Config(req, res, next);
    case settings.EXTERNAL_LDAP_KEY: return setExternalLdapConfig(req, res, next);
    case settings.DIRECTORY_SERVER_KEY: return setDirectoryServerConfig(req, res, next);
    case settings.UNSTABLE_APPS_KEY: return setUnstableAppsConfig(req, res, next);
    case settings.REGISTRY_CONFIG_KEY: return setRegistryConfig(req, res, next);
    case settings.SYSINFO_CONFIG_KEY: return setSysinfoConfig(req, res, next);
    case settings.LANGUAGE_KEY: return setLanguage(req, res, next);

    case settings.AUTOUPDATE_PATTERN_KEY: return setAutoupdatePattern(req, res, next);
    case settings.TIME_ZONE_KEY: return setTimeZone(req, res, next);

    case settings.PROFILE_CONFIG_KEY: return setProfileConfig(req, res, next);

    default: return next(new HttpError(404, 'No such setting'));
    }
}
