'use strict';

exports = module.exports = {
    authorize,
    get,
    update,
    getAvatar,
    setAvatar,
    getBackgroundImage,
    setBackgroundImage,
    setPassword,
    setTwoFactorAuthenticationSecret,
    enableTwoFactorAuthentication,
    disableTwoFactorAuthentication,
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    constants = require('../constants.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance'),
    settings = require('../settings.js'),
    users = require('../users.js'),
    _ = require('underscore');

async function authorize(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error, profileConfig] = await safe(settings.getProfileConfig());
    if (error) return next(BoxError.toHttpError(error));

    if (profileConfig.lockUserProfiles) return next(new HttpError(403, 'admin has disallowed users from editing profiles'));

    next();
}

async function get(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    let [error, avatarUrl] = await safe(users.getAvatarUrl(req.user));
    if (error) return next(BoxError.toHttpError(error));
    if (!avatarUrl) return next(new HttpError(404, 'User not found'));

    let backgroundImage;
    [error, backgroundImage] = await safe(users.getBackgroundImage(req.user.id));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {
        id: req.user.id,
        username: req.user.username,
        email: req.user.email,
        fallbackEmail: req.user.fallbackEmail,
        displayName: req.user.displayName,
        twoFactorAuthenticationEnabled: req.user.twoFactorAuthenticationEnabled,
        role: req.user.role,
        source: req.user.source,
        hasBackgroundImage: !!backgroundImage,
        avatarUrl
    }));
}

async function update(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if ('email' in req.body && typeof req.body.email !== 'string') return next(new HttpError(400, 'email must be string'));
    if ('fallbackEmail' in req.body && typeof req.body.fallbackEmail !== 'string') return next(new HttpError(400, 'fallbackEmail must be string'));
    if ('displayName' in req.body && typeof req.body.displayName !== 'string') return next(new HttpError(400, 'displayName must be string'));

    const data = _.pick(req.body, 'email', 'fallbackEmail', 'displayName');

    // for fallbackEmail we check the password
    if (data.fallbackEmail) {
        if (!req.body.password || typeof req.body.password !== 'string') return next(new HttpError(400, 'password must be non empty string'));

        const [verifyError] = await safe(users.verify(req.user.id, req.body.password, users.AP_WEBADMIN));
        if (verifyError) return next(BoxError.toHttpError(verifyError));

        req.body.password = '<redacted>'; // this will prevent logs from displaying plain text password
    }

    const [error] = await safe(users.update(req.user, data, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}

async function setAvatar(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    let avatar = typeof req.body.avatar === 'string' ? Buffer.from(req.body.avatar, 'utf8') : null;

    if (req.files && req.files.avatar) {
        avatar = safe.fs.readFileSync(req.files.avatar.path);
        if (!avatar) return next(BoxError.toHttpError(new BoxError(BoxError.FS_ERROR, safe.error.message)));
    } else if (!avatar || (!avatar.equals(constants.AVATAR_GRAVATAR) && !avatar.equals(constants.AVATAR_NONE))) {
        return next(new HttpError(400, `avatar must be a file, ${constants.AVATAR_GRAVATAR} or ${constants.AVATAR_NONE}`));
    }

    const [error] = await safe(users.setAvatar(req.user.id, avatar));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, {}));
}

async function getAvatar(req, res, next) {
    assert.strictEqual(typeof req.params.identifier, 'string');

    const [error, avatar] = await safe(users.getAvatar(req.params.identifier));
    if (error) return next(BoxError.toHttpError(error));

    res.send(avatar);
}

async function setBackgroundImage(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    let backgroundImage = null;

    if (req.files && req.files.backgroundImage) {
        backgroundImage = safe.fs.readFileSync(req.files.backgroundImage.path);
        if (!backgroundImage) return next(BoxError.toHttpError(new BoxError(BoxError.FS_ERROR, safe.error.message)));
    }

    const [error] = await safe(users.setBackgroundImage(req.user.id, backgroundImage));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, {}));
}

async function getBackgroundImage(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error, backgroundImage] = await safe(users.getBackgroundImage(req.user.id));
    if (error) return next(BoxError.toHttpError(error));

    res.send(backgroundImage);
}

async function setPassword(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.user, 'object');

    if (typeof req.body.newPassword !== 'string') return next(new HttpError(400, 'newPassword must be a string'));

    const [error] = await safe(users.setPassword(req.user, req.body.newPassword, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}

async function setTwoFactorAuthenticationSecret(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error, result] = await safe(users.setTwoFactorAuthenticationSecret(req.user.id, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(201, { secret: result.secret, qrcode: result.qrcode }));
}

async function enableTwoFactorAuthentication(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.user, 'object');

    if (!req.body.totpToken || typeof req.body.totpToken !== 'string') return next(new HttpError(400, 'totpToken must be a nonempty string'));

    const [error] = await safe(users.enableTwoFactorAuthentication(req.user.id, req.body.totpToken, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, {}));
}

async function disableTwoFactorAuthentication(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const [error] = await safe(users.disableTwoFactorAuthentication(req.user.id, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, {}));
}
