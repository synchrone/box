'use strict';

exports = module.exports = {
    add,
    get,
    del,
    list,
    load,
    remount,
    getStatus
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    volumes = require('../volumes.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function load(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    const [error, result] = await safe(volumes.get(req.params.id));
    if (error) return next(BoxError.toHttpError(error));
    if (!result) return next(new HttpError(404, 'Volume not found'));
    req.volume = result;
    next();
}

async function add(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be a string'));
    if (typeof req.body.mountType !== 'string') return next(new HttpError(400, 'mountType must be a string'));
    if (typeof req.body.mountOptions !== 'object') return next(new HttpError(400, 'mountOptions must be a object'));

    if ('hostPath' in req.body && typeof req.body.hostPath !== 'string') return next(new HttpError(400, 'hostPath must be a string'));

    req.clearTimeout(); // waiting for mount can take time

    const [error, id] = await safe(volumes.add(req.body, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));
    next(new HttpSuccess(201, { id }));
}

async function get(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    next(new HttpSuccess(200, volumes.removePrivateFields(req.volume)));
}

async function del(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    const [error] = await safe(volumes.del(req.volume, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));
    next(new HttpSuccess(204));
}

async function list(req, res, next) {
    const [error, result] = await safe(volumes.list());
    if (error) return next(BoxError.toHttpError(error));

    const allVolumes = result.map(volumes.removePrivateFields);

    next(new HttpSuccess(200, { volumes: allVolumes }));
}

async function remount(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    const [error] = await safe(volumes.remount(req.volume, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));
    next(new HttpSuccess(202));
}

async function getStatus(req, res, next) {
    assert.strictEqual(typeof req.params.id, 'string');

    const [error, status] = await safe(volumes.getStatus(req.volume));
    if (error) return next(BoxError.toHttpError(error));
    next(new HttpSuccess(200, status));
}
