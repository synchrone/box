'use strict';

exports = module.exports = {
    getBlocklist,
    setBlocklist
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    network = require('../network.js'),
    safe = require('safetydance');

async function getBlocklist(req, res, next) {
    const [error, blocklist] = await safe(network.getBlocklist());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { blocklist }));
}

async function setBlocklist(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.blocklist !== 'string') return next(new HttpError(400, 'blocklist must be a string'));

    req.clearTimeout(); // can take a while if there is a lot of network ranges

    const [error] = await safe(network.setBlocklist(req.body.blocklist, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}
