'use strict';

exports = module.exports = {
    providerTokenAuth,
    setup,
    activate,
    restore,
    getStatus,
    setupTokenAuth
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    paths = require('../paths.js'),
    provision = require('../provision.js'),
    safe = require('safetydance'),
    settings = require('../settings.js'),
    superagent = require('superagent');

function setupTokenAuth(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    const setupToken = safe.fs.readFileSync(paths.SETUP_TOKEN_FILE, 'utf8');
    if (!setupToken) return next();

    if (!req.body.setupToken) return next(new HttpError(400, 'setup token required'));
    if (setupToken.trim() !== req.body.setupToken) return next(new HttpError(422, 'setup token does not match'));

    return next();
}

async function providerTokenAuth(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (settings.provider() === 'ami') {
        if (typeof req.body.providerToken !== 'string' || !req.body.providerToken) return next(new HttpError(400, 'providerToken must be a non empty string'));

        const [error, response] = await safe(superagent.get('http://169.254.169.254/latest/meta-data/instance-id').timeout(30 * 1000).ok(() => true));
        if (error) return next(new HttpError(422, `Network error getting EC2 metadata: ${error.message}`));
        if (response.status !== 200) return next(new HttpError(422, `Unable to get EC2 meta data. statusCode: ${response.status}`));
        if (response.text !== req.body.providerToken) return next(new HttpError(422, 'Instance ID does not match'));

        next();
    } else {
        next();
    }
}

async function setup(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.domainConfig || typeof req.body.domainConfig !== 'object') return next(new HttpError(400, 'domainConfig is required'));

    const domainConfig = req.body.domainConfig;

    if (typeof domainConfig.provider !== 'string' || !domainConfig.provider) return next(new HttpError(400, 'provider is required'));
    if (typeof domainConfig.domain !== 'string' || !domainConfig.domain) return next(new HttpError(400, 'domain is required'));

    if ('zoneName' in domainConfig && typeof domainConfig.zoneName !== 'string') return next(new HttpError(400, 'zoneName must be a string'));
    if (!domainConfig.config || typeof domainConfig.config !== 'object') return next(new HttpError(400, 'config must be an object'));

    if ('tlsConfig' in domainConfig && typeof domainConfig.tlsConfig !== 'object') return next(new HttpError(400, 'tlsConfig must be an object'));
    if (domainConfig.tlsConfig && (!domainConfig.tlsConfig.provider || typeof domainConfig.tlsConfig.provider !== 'string')) return next(new HttpError(400, 'tlsConfig.provider must be a string'));

    if ('sysinfoConfig' in req.body && typeof req.body.sysinfoConfig !== 'object') return next(new HttpError(400, 'sysinfoConfig must be an object'));

    // it can take sometime to setup DNS, register cloudron
    req.clearTimeout();

    const [error] = await safe(provision.setup(domainConfig, req.body.sysinfoConfig || { provider: 'generic' }, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function activate(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.username !== 'string') return next(new HttpError(400, 'username must be string'));
    if (typeof req.body.password !== 'string') return next(new HttpError(400, 'password must be string'));
    if (typeof req.body.email !== 'string') return next(new HttpError(400, 'email must be string'));
    if ('displayName' in req.body && typeof req.body.displayName !== 'string') return next(new HttpError(400, 'displayName must be string'));

    const { username, password, email } = req.body;
    const displayName = req.body.displayName || '';

    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    const [error, info] = await safe(provision.activate(username, password, email, displayName, ip, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(201, info));
}

async function restore(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.backupConfig || typeof req.body.backupConfig !== 'object') return next(new HttpError(400, 'backupConfig is required'));

    const backupConfig = req.body.backupConfig;
    if (typeof backupConfig.provider !== 'string') return next(new HttpError(400, 'provider is required'));
    if ('password' in backupConfig && typeof backupConfig.password !== 'string') return next(new HttpError(400, 'password must be a string'));
    if ('encryptedFilenames' in req.body && typeof req.body.encryptedFilenames !== 'boolean') return next(new HttpError(400, 'encryptedFilenames must be a boolean'));

    if (typeof backupConfig.format !== 'string') return next(new HttpError(400, 'format must be a string'));
    if ('acceptSelfSignedCerts' in backupConfig && typeof backupConfig.acceptSelfSignedCerts !== 'boolean') return next(new HttpError(400, 'format must be a boolean'));

    if (typeof req.body.remotePath !== 'string') return next(new HttpError(400, 'remotePath must be a string'));
    if (typeof req.body.version !== 'string') return next(new HttpError(400, 'version must be a string'));

    if ('sysinfoConfig' in req.body && typeof req.body.sysinfoConfig !== 'object') return next(new HttpError(400, 'sysinfoConfig must be an object'));
    if ('skipDnsSetup' in req.body && typeof req.body.skipDnsSetup !== 'boolean') return next(new HttpError(400, 'skipDnsSetup must be a boolean'));

    const options = {
        skipDnsSetup: req.body.skipDnsSetup || false
    };

    const [error] = await safe(provision.restore(backupConfig, req.body.remotePath, req.body.version, req.body.sysinfoConfig || { provider: 'generic' }, options, AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function getStatus(req, res, next) {
    const [error, status] = await safe(provision.getStatus());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, status));
}
