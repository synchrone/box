'use strict';

exports = module.exports = {
    get
};

const HttpError = require('connect-lastmile').HttpError,
    safe = require('safetydance'),
    wellknown = require('../wellknown.js');

async function get(req, res, next) {
    const host = req.headers['host'];
    const location = req.params[0];

    const [error, result] = await safe(wellknown.get(host, location));
    if (error) return next(new HttpError(404, error.message));

    res.status(200).set('content-type', result.type).send(result.body);
}
