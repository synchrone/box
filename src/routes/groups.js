'use strict';

exports = module.exports = {
    get,
    list,
    add,
    update,
    remove,
    setMembers
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    groups = require('../groups.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function add(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be string'));

    const [error, group] = await safe(groups.add({ name: req.body.name, source : '' }));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(201, { id: group.id, name: group.name }));
}

async function get(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');

    const [error, result] = await safe(groups.getWithMembers(req.params.groupId));
    if (error) return next(BoxError.toHttpError(error));
    if (!result) return next(new HttpError(404, 'Group not found'));

    next(new HttpSuccess(200, result));
}

async function update(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');
    assert.strictEqual(typeof req.body, 'object');

    if ('name' in req.body && typeof req.body.name !== 'string') return next(new HttpError(400, 'name must be a string'));

    const [error] = await safe(groups.update(req.params.groupId, req.body));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { }));
}

async function setMembers(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');

    if (!req.body.userIds) return next(new HttpError(404, 'missing or invalid userIds fields'));
    if (!Array.isArray(req.body.userIds)) return next(new HttpError(404, 'userIds must be an array'));
    if (req.body.userIds.some((u) => typeof u !== 'string')) return next(new HttpError(400, 'userIds array must contain strings'));

    const [error] = await safe(groups.setMembers(req.params.groupId, req.body.userIds));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { }));
}

async function list(req, res, next) {
    const [error, result] = await safe(groups.listWithMembers());
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { groups: result }));
}

async function remove(req, res, next) {
    assert.strictEqual(typeof req.params.groupId, 'string');

    const [error] = await safe(groups.remove(req.params.groupId));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(204));
}
