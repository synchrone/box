'use strict';

exports = module.exports = {
    list,
    update,
    create,
    cleanup,
    remount,
};

const assert = require('assert'),
    AuditSource = require('../auditsource.js'),
    backups = require('../backups.js'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    safe = require('safetydance');

async function list(req, res, next) {
    const page = typeof req.query.page !== 'undefined' ? parseInt(req.query.page) : 1;
    if (!page || page < 0) return next(new HttpError(400, 'page query param has to be a postive number'));

    const perPage = typeof req.query.per_page !== 'undefined'? parseInt(req.query.per_page) : 25;
    if (!perPage || perPage < 0) return next(new HttpError(400, 'per_page query param has to be a postive number'));

    const [error, result] = await safe(backups.getByIdentifierAndStatePaged(backups.BACKUP_IDENTIFIER_BOX, backups.BACKUP_STATE_NORMAL, page, perPage));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, { backups: result }));
}

async function update(req, res, next) {
    assert.strictEqual(typeof req.params.backupId, 'string');
    assert.strictEqual(typeof req.body, 'object');

    const { label, preserveSecs } = req.body;
    if (typeof label !== 'string') return next(new HttpError(400, 'label must be a string'));
    if (typeof preserveSecs !== 'number') return next(new HttpError(400, 'preserveSecs must be a number'));

    const [error] = await safe(backups.update(req.params.backupId, { label, preserveSecs }));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(200, {}));
}

async function create(req, res, next) {
    const [error, taskId] = await safe(backups.startBackupTask(AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, { taskId }));
}

async function cleanup(req, res, next) {
    const [error, taskId] = await safe(backups.startCleanupTask(AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, { taskId }));
}

async function remount(req, res, next) {
    const [error] = await safe(backups.remount(AuditSource.fromRequest(req)));
    if (error) return next(BoxError.toHttpError(error));

    next(new HttpSuccess(202, {}));
}
