'use strict';

exports = module.exports = {
    load,
    get,
    list,
    update
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    notifications = require('../notifications.js'),
    safe = require('safetydance');

async function load(req, res, next) {
    assert.strictEqual(typeof req.params.notificationId, 'string');

    const [error, result] = await safe(notifications.get(req.params.notificationId));
    if (error) return next(BoxError.toHttpError(error));
    if (!result) return next(new HttpError(404, 'Notification not found'));

    req.notification = result;
    next();
}

function get(req, res, next) {
    assert.strictEqual(typeof req.notification, 'object');

    next(new HttpSuccess(200, req.notification));
}

async function list(req, res, next) {
    const page = typeof req.query.page !== 'undefined' ? parseInt(req.query.page) : 1;
    if (!page || page < 0) return next(new HttpError(400, 'page query param has to be a postive number'));

    const perPage = typeof req.query.per_page !== 'undefined'? parseInt(req.query.per_page) : 25;
    if (!perPage || perPage < 0) return next(new HttpError(400, 'per_page query param has to be a postive number'));

    if (req.query.acknowledged && !(req.query.acknowledged === 'true' || req.query.acknowledged === 'false')) return next(new HttpError(400, 'acknowledged must be a true or false'));

    let filters = {};
    if (req.query.acknowledged) filters.acknowledged = req.query.acknowledged === 'true';

    const [error, result] = await safe(notifications.list(filters, page, perPage));
    if (error) return next(BoxError.toHttpError(error));
    next(new HttpSuccess(200, { notifications: result }));
}

async function update(req, res, next) {
    assert.strictEqual(typeof req.notification, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.acknowledged !== 'boolean') return next(new HttpError(400, 'acknowledged must be a boolean'));

    const [error] = await safe(notifications.update(req.notification, { acknowledged: req.body.acknowledged }));
    if (error) return next(BoxError.toHttpError(error));
    next(new HttpSuccess(204, {}));
}
