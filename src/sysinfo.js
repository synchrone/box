'use strict';

exports = module.exports = {
    getServerIPv4,
    getServerIPv6,
    testIPv4Config,
    testIPv6Config,

    hasIPv6
};

const assert = require('assert'),
    fs = require('fs'),
    ipaddr = require('ipaddr.js'),
    settings = require('./settings.js');

function api(provider) {
    assert.strictEqual(typeof provider, 'string');

    switch (provider) {
    case 'noop': return require('./sysinfo/noop.js');
    case 'fixed': return require('./sysinfo/fixed.js');
    case 'network-interface': return require('./sysinfo/network-interface.js');
    default: return require('./sysinfo/generic.js');
    }
}

async function getServerIPv4() {
    const config = await settings.getSysinfoConfig();

    return await api(config.provider).getServerIPv4(config);
}

// returns RFC 5952 formatted address (https://datatracker.ietf.org/doc/html/rfc5952)
async function getServerIPv6() {
    const config = await settings.getIPv6Config();
    const result = await api(config.provider).getServerIPv6(config);
    if (!result) return null;
    return ipaddr.parse(result).toRFC5952String();
}

function hasIPv6() {
    const IPV6_PROC_FILE = '/proc/net/if_inet6';
    // on contabo, /proc/net/if_inet6 is an empty file. so just exists is not enough
    return fs.existsSync(IPV6_PROC_FILE) && fs.readFileSync(IPV6_PROC_FILE, 'utf8').trim().length !== 0;
}

async function testIPv4Config(config) {
    assert.strictEqual(typeof config, 'object');

    return await api(config.provider).testIPv4Config(config);
}

async function testIPv6Config(config) {
    assert.strictEqual(typeof config, 'object');

    return await api(config.provider).testIPv6Config(config);
}
