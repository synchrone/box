'use strict';

// -------------------------------------------
//  This file just describes the interface
//
//  New backends can start from here
// -------------------------------------------

exports = module.exports = {
    getServerIPv4,
    getServerIPv6,
    testIPv4Config,
    testIPv6Config
};

const assert = require('assert'),
    BoxError = require('../boxerror.js');

async function getServerIPv4(config) {
    assert.strictEqual(typeof config, 'object');

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'getServerIPv4 is not implemented');
}

async function getServerIPv6(config) {
    assert.strictEqual(typeof config, 'object');

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'getServerIPv6 is not implemented');
}

async function testIPv4Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function testIPv6Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}
