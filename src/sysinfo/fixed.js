'use strict';

exports = module.exports = {
    getServerIPv4,
    getServerIPv6,
    testIPv4Config,
    testIPv6Config
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    net = require('net');

async function getServerIPv4(config) {
    assert.strictEqual(typeof config, 'object');

    return config.ipv4;
}

async function getServerIPv6(config) {
    assert.strictEqual(typeof config, 'object');

    if ('ipv6' in config) return config.ipv6;

    throw new BoxError(BoxError.NETWORK_ERROR, 'No IPv6 configured');
}

async function testIPv4Config(config) {
    assert.strictEqual(typeof config, 'object');

    if (typeof config.ipv4 !== 'string') return new BoxError(BoxError.BAD_FIELD, 'ipv4 must be a string');
    if (!net.isIPv4(config.ipv4)) return new BoxError(BoxError.BAD_FIELD, 'invalid IPv4');

    return null;
}

async function testIPv6Config(config) {
    assert.strictEqual(typeof config, 'object');

    if (typeof config.ipv6 !== 'string') return new BoxError(BoxError.BAD_FIELD, 'ipv6 must be a string');
    if (!net.isIPv6(config.ipv6)) return new BoxError(BoxError.BAD_FIELD, 'invalid IPv6');

    return null;
}
