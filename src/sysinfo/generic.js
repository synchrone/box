'use strict';

exports = module.exports = {
    getServerIPv4,
    getServerIPv6,
    testIPv4Config,
    testIPv6Config
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    debug = require('debug')('box:sysinfo/generic'),
    safe = require('safetydance'),
    superagent = require('superagent');

async function getServerIPv4(config) {
    assert.strictEqual(typeof config, 'object');

    if (process.env.BOX_ENV === 'test') return '127.0.0.1';

    debug('getServerIPv4: getting server IP');

    const [networkError, response] = await safe(superagent.get('https://ipv4.api.cloudron.io/api/v1/helper/public_ip')
        .timeout(30 * 1000)
        .retry(2)
        .ok(() => true));

    if (networkError || response.status !== 200) {
        debug('getServerIPv4: Error getting IP', networkError);
        throw new BoxError(BoxError.EXTERNAL_ERROR, 'Unable to detect IPv4. API server (ipv4.api.cloudron.io) unreachable');
    }

    if (!response.body && !response.body.ip) {
        debug('getServerIPv4: Unexpected answer. No "ip" found in response body.', response.body);
        throw new BoxError(BoxError.EXTERNAL_ERROR, 'Unable to detect IPv4. No IP found in response');
    }

    return response.body.ip;
}

async function getServerIPv6(config) {
    assert.strictEqual(typeof config, 'object');

    if (process.env.BOX_ENV === 'test') return '::1';

    debug('getServerIPv6: getting server IP');

    const [networkError, response] = await safe(superagent.get('https://ipv6.api.cloudron.io/api/v1/helper/public_ip')
        .timeout(30 * 1000)
        .retry(2)
        .ok(() => true));

    if (networkError || response.status !== 200) {
        debug('getServerIPv6: Error getting IP', networkError);
        throw new BoxError(BoxError.EXTERNAL_ERROR, 'Unable to detect IPv6. API server (ipv6.api.cloudron.io) unreachable');
    }

    if (!response.body && !response.body.ip) {
        debug('getServerIPv6: Unexpected answer. No "ip" found in response body.', response.body);
        throw new BoxError(BoxError.EXTERNAL_ERROR, 'Unable to detect IPv6. No IP found in response');
    }

    return response.body.ip;
}

async function testIPv4Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function testIPv6Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}
