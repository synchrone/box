'use strict';

exports = module.exports = {
    getServerIPv4,
    getServerIPv6,
    testIPv4Config,
    testIPv6Config
};

const assert = require('assert');

async function getServerIPv4(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function getServerIPv6(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function testIPv4Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}

async function testIPv6Config(config) {
    assert.strictEqual(typeof config, 'object');

    return null;
}
