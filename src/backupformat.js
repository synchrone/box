'use strict';

exports = module.exports = {
    api
};

function api(format) {
    switch (format) {
    case 'tgz': return require('./backupformat/tgz.js');
    case 'rsync': return require('./backupformat/rsync.js');
    }
}
