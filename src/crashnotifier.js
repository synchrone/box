'use strict';

exports = module.exports = {
    sendFailureLogs
};

const assert = require('assert'),
    AuditSource = require('./auditsource.js'),
    child_process = require('child_process'),
    eventlog = require('./eventlog.js'),
    safe = require('safetydance'),
    path = require('path'),
    paths = require('./paths.js'),
    util = require('util');

const COLLECT_LOGS_CMD = path.join(__dirname, 'scripts/collectlogs.sh');

const CRASH_LOG_TIMESTAMP_OFFSET = 1000 * 60 * 60; // 60 min
const CRASH_LOG_TIMESTAMP_FILE = '/tmp/crashlog.timestamp';

async function collectLogs(unitName) {
    assert.strictEqual(typeof unitName, 'string');

    const logs = child_process.execSync(`sudo ${COLLECT_LOGS_CMD} ${unitName}`, { encoding: 'utf8' });
    return logs;
}

async function sendFailureLogs(unitName) {
    assert.strictEqual(typeof unitName, 'string');

    // check if we already sent a mail in the last CRASH_LOG_TIME_OFFSET window
    const timestamp = safe.fs.readFileSync(CRASH_LOG_TIMESTAMP_FILE, 'utf8');
    if (timestamp && (parseInt(timestamp) + CRASH_LOG_TIMESTAMP_OFFSET) > Date.now()) {
        console.log('Crash log already sent within window');
        return;
    }

    let [error, logs] = await safe(collectLogs(unitName));
    if (error) {
        console.error('Failed to collect logs.', error);
        logs = util.format('Failed to collect logs.', error);
    }

    const crashId = `${new Date().toISOString()}`;
    console.log(`Creating crash log for ${unitName} with id ${crashId}`);

    if (!safe.fs.writeFileSync(path.join(paths.CRASH_LOG_DIR, `${crashId}.log`), logs)) console.log(`Failed to stash logs to ${crashId}.log:`, safe.error);

    [error] = await safe(eventlog.add(eventlog.ACTION_PROCESS_CRASH, AuditSource.HEALTH_MONITOR, { processName: unitName, crashId: crashId }));
    if (error) console.log(`Error sending crashlog. Logs stashed at ${crashId}.log`);

    safe.fs.writeFileSync(CRASH_LOG_TIMESTAMP_FILE, String(Date.now()));
}
