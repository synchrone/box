'use strict';

exports = module.exports = {
    renderFooter
};

const assert = require('assert'),
    constants = require('./constants.js');

function renderFooter(footer) {
    assert.strictEqual(typeof footer, 'string');

    const year = new Date().getFullYear();

    return footer.replace(/%YEAR%/g, year)
        .replace(/%VERSION%/g, constants.VERSION);
}

