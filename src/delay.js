'use strict';

exports = module.exports = delay;

const assert = require('assert');

function delay(msecs) {
    assert.strictEqual(typeof msecs, 'number');

    return new Promise(function (resolve) {
        setTimeout(resolve, msecs);
    });
}
