/* jslint node:true */

'use strict';

exports = module.exports = {
    get,
    getString,
    set,
    setString,
    del,

    ACME_ACCOUNT_KEY: 'acme_account_key',
    ADDON_TURN_SECRET: 'addon_turn_secret',
    SFTP_PUBLIC_KEY: 'sftp_public_key',
    SFTP_PRIVATE_KEY: 'sftp_private_key',
    PROXY_AUTH_TOKEN_SECRET: 'proxy_auth_token_secret',

    CERT_PREFIX: 'cert',

    _clear: clear
};

const assert = require('assert'),
    database = require('./database.js');

const BLOBS_FIELDS = [ 'id', 'value' ].join(',');

async function get(id) {
    assert.strictEqual(typeof id, 'string');

    const result = await database.query(`SELECT ${BLOBS_FIELDS} FROM blobs WHERE id = ?`, [ id ]);
    if (result.length === 0) return null;
    return result[0].value;
}

async function getString(id) {
    assert.strictEqual(typeof id, 'string');

    const result = await database.query(`SELECT ${BLOBS_FIELDS} FROM blobs WHERE id = ?`, [ id ]);
    if (result.length === 0) return null;
    return result[0].value.toString('utf8');
}

async function set(id, value) {
    assert.strictEqual(typeof id, 'string');
    assert(value === null || Buffer.isBuffer(value));

    await database.query('INSERT INTO blobs (id, value) VALUES (?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)', [ id, value ]);
}

async function setString(id, value) {
    assert.strictEqual(typeof id, 'string');
    assert(value === null || typeof value === 'string');

    await database.query('INSERT INTO blobs (id, value) VALUES (?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)', [ id, Buffer.from(value) ]);
}

async function del(id) {
    await database.query('DELETE FROM blobs WHERE id=?', [ id ]);
}

async function clear() {
    await database.query('DELETE FROM blobs');
}
