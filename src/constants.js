'use strict';

const fs = require('fs'),
    path = require('path');

const CLOUDRON = process.env.BOX_ENV === 'cloudron',
    TEST = process.env.BOX_ENV === 'test';

exports = module.exports = {
    SMTP_SUBDOMAIN: 'smtp',
    IMAP_SUBDOMAIN: 'imap',

    // These are combined into one array because users and groups become mailboxes
    RESERVED_NAMES: [
        // Reserved usernames
        // https://github.com/gogits/gogs/blob/52c8f691630548fe091d30bcfe8164545a05d3d5/models/repo.go#L393
        // apps like wordpress, gogs don't like these
        // postmaster is used in dovecot and haraka
        'admin', 'no-reply', 'postmaster', 'mailer-daemon',

        // Reserved groups
        'admins', 'users'         // ldap code uses 'users' pseudo group
    ],

    DASHBOARD_SUBDOMAIN: 'my',

    PORT: CLOUDRON ? 3000 : 5454,
    INTERNAL_SMTP_PORT: 2525, // this value comes from the mail container
    AUTHWALL_PORT: 3001,
    LDAP_PORT: 3002,
    DOCKER_PROXY_PORT: 3003,
    USER_DIRECTORY_LDAPS_PORT: 3004, // user directory LDAP with TLS rerouting in iptables, public port is 636

    NGINX_DEFAULT_CONFIG_FILE_NAME: 'default.conf',

    DEFAULT_TOKEN_EXPIRATION_MSECS: 365 * 24 * 60 * 60 * 1000, // 1 year
    DEFAULT_TOKEN_EXPIRATION_DAYS: 365,

    DEFAULT_MEMORY_LIMIT: (256 * 1024 * 1024), // see also client.js

    DEMO_USERNAME: 'cloudron',
    DEMO_BLACKLISTED_APPS: [
        'com.github.cloudtorrent',
        'net.alltubedownload.cloudronapp',
        'com.adguard.home.cloudronapp',
        'com.transmissionbt.cloudronapp',
        'io.github.sickchill.cloudronapp',
        'to.couchpota.cloudronapp'
    ],
    DEMO_APP_LIMIT: 20,

    PROXY_APP_APPSTORE_ID: 'io.cloudron.builtin.appproxy',

    AUTOUPDATE_PATTERN_NEVER: 'never',

    // the db field is a blob so we make this explicit
    AVATAR_NONE: Buffer.from('', 'utf8'),
    AVATAR_GRAVATAR: Buffer.from('gravatar', 'utf8'),
    AVATAR_CUSTOM: Buffer.from('custom', 'utf8'),   // this is not used here just for reference. The field will contain a byte buffer instead of the type string

    SECRET_PLACEHOLDER: String.fromCharCode(0x25CF).repeat(8), // also used in dashboard client.js

    CLOUDRON: CLOUDRON,
    TEST: TEST,

    PORT25_CHECK_SERVER: 'port25check.cloudron.io',

    FORUM_URL: 'https://forum.cloudron.io',

    SUPPORT_USERNAME: 'cloudron-support',
    SUPPORT_EMAIL: 'support@cloudron.io',

    USER_DIRECTORY_LDAP_DN: 'cn=admin,ou=system,dc=cloudron',

    FOOTER: '&copy; %YEAR%  &nbsp;  [Cloudron](https://cloudron.io) &nbsp; &nbsp; &nbsp;  [Forum <i class="fa fa-comments"></i>](https://forum.cloudron.io)',

    VERSION: process.env.BOX_ENV === 'cloudron' ? fs.readFileSync(path.join(__dirname, '../VERSION'), 'utf8').trim() : '7.2.0-test'
};

