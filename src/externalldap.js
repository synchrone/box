'use strict';

exports = module.exports = {
    verifyPassword,
    verifyPasswordAndTotpToken,
    maybeCreateUser,

    testConfig,
    startSyncer,

    injectPrivateFields,
    removePrivateFields,

    sync
};

const assert = require('assert'),
    AuditSource = require('./auditsource.js'),
    BoxError = require('./boxerror.js'),
    constants = require('./constants.js'),
    debug = require('debug')('box:externalldap'),
    groups = require('./groups.js'),
    ldap = require('ldapjs'),
    once = require('./once.js'),
    safe = require('safetydance'),
    settings = require('./settings.js'),
    tasks = require('./tasks.js'),
    users = require('./users.js'),
    util = require('util');

function injectPrivateFields(newConfig, currentConfig) {
    if (newConfig.bindPassword === constants.SECRET_PLACEHOLDER) newConfig.bindPassword = currentConfig.bindPassword;
}

function removePrivateFields(ldapConfig) {
    assert.strictEqual(typeof ldapConfig, 'object');
    if (ldapConfig.bindPassword) ldapConfig.bindPassword = constants.SECRET_PLACEHOLDER;
    return ldapConfig;
}

function translateUser(ldapConfig, ldapUser) {
    assert.strictEqual(typeof ldapConfig, 'object');

    // RFC: https://datatracker.ietf.org/doc/html/rfc2798
    return {
        username: ldapUser[ldapConfig.usernameField].toLowerCase(),
        email: ldapUser.mail || ldapUser.mailPrimaryAddress,
        twoFactorAuthenticationEnabled: !!ldapUser.twoFactorAuthenticationEnabled,
        displayName: ldapUser.displayName || ldapUser.cn // user.giveName + ' ' + user.sn
    };
}

function validUserRequirements(user) {
    if (!user.username || !user.email || !user.displayName) {
        debug(`[Invalid LDAP user] username=${user.username} email=${user.email} displayName=${user.displayName}`);
        return false;
    } else {
        return true;
    }
}

// performs service bind if required
async function getClient(externalLdapConfig, options) {
    assert.strictEqual(typeof externalLdapConfig, 'object');
    assert.strictEqual(typeof options, 'object');

    // basic validation to not crash
    try { ldap.parseDN(externalLdapConfig.baseDn); } catch (e) { throw new BoxError(BoxError.BAD_FIELD, 'invalid baseDn'); }
    try { ldap.parseFilter(externalLdapConfig.filter); } catch (e) { throw new BoxError(BoxError.BAD_FIELD, 'invalid filter'); }

    const config = {
        url: externalLdapConfig.url,
        tlsOptions: {
            rejectUnauthorized: externalLdapConfig.acceptSelfSignedCerts ? false : true
        }
    };

    let client;
    try {
        client = ldap.createClient(config);
    } catch (e) {
        if (e instanceof ldap.ProtocolError) throw new BoxError(BoxError.BAD_FIELD, 'url protocol is invalid');
        throw new BoxError(BoxError.INTERNAL_ERROR, e);
    }

    // skip bind auth if none exist or if not wanted
    if (!externalLdapConfig.bindDn || !options.bind) return client;

    return await new Promise((resolve, reject) => {
        reject = once(reject);

        // ensure we don't just crash
        client.on('error', function (error) {
            reject(new BoxError(BoxError.EXTERNAL_ERROR, error));
        });

        client.bind(externalLdapConfig.bindDn, externalLdapConfig.bindPassword, function (error) {
            if (error instanceof ldap.InvalidCredentialsError) return reject(new BoxError(BoxError.INVALID_CREDENTIALS));
            if (error) return reject(new BoxError(BoxError.EXTERNAL_ERROR, error));

            resolve(client);
        });
    });
}

async function clientSearch(client, dn, searchOptions) {
    assert.strictEqual(typeof client, 'object');
    assert.strictEqual(typeof dn, 'string');
    assert.strictEqual(typeof searchOptions, 'object');

    // basic validation to not crash
    try { ldap.parseDN(dn); } catch (e) { throw new BoxError(BoxError.BAD_FIELD, 'invalid DN'); }

    return await new Promise((resolve, reject) => {
        client.search(dn, searchOptions, function (error, result) {
            if (error instanceof ldap.NoSuchObjectError) return reject(new BoxError(BoxError.NOT_FOUND));
            if (error) return reject(new BoxError(BoxError.EXTERNAL_ERROR, error));

            let ldapObjects = [];

            result.on('searchEntry', entry => ldapObjects.push(entry.object));
            result.on('error', error => reject(new BoxError(BoxError.EXTERNAL_ERROR, error)));

            result.on('end', function (result) {
                if (result.status !== 0) return reject(new BoxError(BoxError.EXTERNAL_ERROR, 'Server returned status ' + result.status));

                resolve(ldapObjects);
            });
        });
    });
}

async function ldapGetByDN(externalLdapConfig, dn) {
    assert.strictEqual(typeof externalLdapConfig, 'object');
    assert.strictEqual(typeof dn, 'string');

    const searchOptions = {
        paged: true,
        scope: 'sub'    // We may have to make this configurable
    };

    debug(`ldapGetByDN: Get object at ${dn}`);

    const client = await getClient(externalLdapConfig, { bind: true });
    const result = await clientSearch(client, dn, searchOptions);
    client.unbind();
    if (result.length === 0) throw new BoxError(BoxError.NOT_FOUND);
    return result[0];
}

// TODO support search by email
async function ldapUserSearch(externalLdapConfig, options) {
    assert.strictEqual(typeof externalLdapConfig, 'object');
    assert.strictEqual(typeof options, 'object');

    const searchOptions = {
        paged: true,
        filter: ldap.parseFilter(externalLdapConfig.filter),
        scope: 'sub'    // We may have to make this configurable
    };

    if (options.filter) { // https://github.com/ldapjs/node-ldapjs/blob/master/docs/filters.md
        const extraFilter = ldap.parseFilter(options.filter);
        searchOptions.filter = new ldap.AndFilter({ filters: [ extraFilter, searchOptions.filter ] });
    }

    const client = await getClient(externalLdapConfig, { bind: true });
    const result = await clientSearch(client, externalLdapConfig.baseDn, searchOptions);
    client.unbind();
    return result;
}

async function ldapGroupSearch(externalLdapConfig, options) {
    assert.strictEqual(typeof externalLdapConfig, 'object');
    assert.strictEqual(typeof options, 'object');

    const searchOptions = {
        paged: true,
        scope: 'sub'    // We may have to make this configurable
    };

    if (externalLdapConfig.groupFilter) searchOptions.filter = ldap.parseFilter(externalLdapConfig.groupFilter);

    if (options.filter) { // https://github.com/ldapjs/node-ldapjs/blob/master/docs/filters.md
        const extraFilter = ldap.parseFilter(options.filter);
        searchOptions.filter = new ldap.AndFilter({ filters: [ extraFilter, searchOptions.filter ] });
    }

    const client = await getClient(externalLdapConfig, { bind: true });
    const result = await clientSearch(client, externalLdapConfig.groupBaseDn, searchOptions);
    client.unbind();
    return result;
}

async function testConfig(config) {
    assert.strictEqual(typeof config, 'object');

    if (config.provider === 'noop') return null;

    if (!config.url) return new BoxError(BoxError.BAD_FIELD, 'url must not be empty');
    if (!config.url.startsWith('ldap://') && !config.url.startsWith('ldaps://')) return new BoxError(BoxError.BAD_FIELD, 'url is missing ldap:// or ldaps:// prefix');
    if (!config.usernameField) config.usernameField = 'uid';

    // bindDn may not be a dn!
    if (!config.baseDn) return new BoxError(BoxError.BAD_FIELD, 'basedn must not be empty');
    try { ldap.parseDN(config.baseDn); } catch (e) { return new BoxError(BoxError.BAD_FIELD, 'invalid baseDn'); }

    if (!config.filter) return new BoxError(BoxError.BAD_FIELD, 'filter must not be empty');
    try { ldap.parseFilter(config.filter); } catch (e) { return new BoxError(BoxError.BAD_FIELD, 'invalid filter'); }

    if ('syncGroups' in config && typeof config.syncGroups !== 'boolean') return new BoxError(BoxError.BAD_FIELD, 'syncGroups must be a boolean');
    if ('acceptSelfSignedCerts' in config && typeof config.acceptSelfSignedCerts !== 'boolean') return new BoxError(BoxError.BAD_FIELD, 'acceptSelfSignedCerts must be a boolean');

    if (config.syncGroups) {
        if (!config.groupBaseDn) return new BoxError(BoxError.BAD_FIELD, 'groupBaseDn must not be empty');
        try { ldap.parseDN(config.groupBaseDn); } catch (e) { return new BoxError(BoxError.BAD_FIELD, 'invalid groupBaseDn'); }

        if (!config.groupFilter) return new BoxError(BoxError.BAD_FIELD, 'groupFilter must not be empty');
        try { ldap.parseFilter(config.groupFilter); } catch (e) { return new BoxError(BoxError.BAD_FIELD, 'invalid groupFilter'); }

        if (!config.groupnameField || typeof config.groupnameField !== 'string') return new BoxError(BoxError.BAD_FIELD, 'groupFilter must not be empty');
    }

    const [error, client] = await safe(getClient(config, { bind: true }));
    if (error) return error;

    const opts = {
        filter: config.filter,
        scope: 'sub'
    };

    const [searchError, ] = await safe(clientSearch(client, config.baseDn, opts));
    client.unbind();
    if (searchError) return searchError;

    return null;
}

async function maybeCreateUser(identifier) {
    assert.strictEqual(typeof identifier, 'string');

    const externalLdapConfig = await settings.getExternalLdapConfig();
    if (externalLdapConfig.provider === 'noop') throw new BoxError(BoxError.BAD_STATE, 'not enabled');
    if (!externalLdapConfig.autoCreate) throw new BoxError(BoxError.BAD_STATE, 'auto create not enabled');

    const ldapUsers = await ldapUserSearch(externalLdapConfig, { filter: `${externalLdapConfig.usernameField}=${identifier}` });
    if (ldapUsers.length === 0) throw new BoxError(BoxError.NOT_FOUND);
    if (ldapUsers.length > 1) throw new BoxError(BoxError.CONFLICT);

    const user = translateUser(externalLdapConfig, ldapUsers[0]);
    if (!validUserRequirements(user)) throw new BoxError(BoxError.BAD_FIELD);

    const [error, userId] = await safe(users.add(user.email, { username: user.username, password: null, displayName: user.displayName, source: 'ldap' }, AuditSource.EXTERNAL_LDAP_AUTO_CREATE));
    if (error) {
        debug(`maybeCreateUser: failed to auto create user ${user.username}`, error);
        throw error;
    }

    // fetch the full record and amend potential twoFA settings
    const newUser = await users.get(userId);
    if (user.twoFactorAuthenticationEnabled) newUser.twoFactorAuthenticationEnabled = true;

    return newUser;
}

async function verifyPassword(user, password) {
    assert.strictEqual(typeof user, 'object');
    assert.strictEqual(typeof password, 'string');

    const externalLdapConfig = await settings.getExternalLdapConfig();
    if (externalLdapConfig.provider === 'noop') throw new BoxError(BoxError.BAD_STATE, 'not enabled');

    const ldapUsers = await ldapUserSearch(externalLdapConfig, { filter: `${externalLdapConfig.usernameField}=${user.username}` });
    if (ldapUsers.length === 0) throw new BoxError(BoxError.NOT_FOUND);
    if (ldapUsers.length > 1) throw new BoxError(BoxError.CONFLICT);

    const client = await getClient(externalLdapConfig, { bind: false });

    const [error] = await safe(util.promisify(client.bind.bind(client))(ldapUsers[0].dn, password));
    client.unbind();
    if (error instanceof ldap.InvalidCredentialsError) throw new BoxError(BoxError.INVALID_CREDENTIALS);
    if (error) throw new BoxError(BoxError.EXTERNAL_ERROR, error);

    return translateUser(externalLdapConfig, ldapUsers[0]);
}

async function verifyPasswordAndTotpToken(user, password, totpToken) {
    assert.strictEqual(typeof user, 'object');
    assert.strictEqual(typeof password, 'string');
    assert.strictEqual(typeof totpToken, 'string');

    const externalLdapConfig = await settings.getExternalLdapConfig();
    if (externalLdapConfig.provider === 'noop') throw new BoxError(BoxError.BAD_STATE, 'not enabled');

    const ldapUsers = await ldapUserSearch(externalLdapConfig, { filter: `${externalLdapConfig.usernameField}=${user.username}` });
    if (ldapUsers.length === 0) throw new BoxError(BoxError.NOT_FOUND);
    if (ldapUsers.length > 1) throw new BoxError(BoxError.CONFLICT);

    const client = await getClient(externalLdapConfig, { bind: false });

    // inject totptoken into first attribute
    const rdns = ldapUsers[0].dn.split(',');
    const totpTokenDn = `${rdns[0]}+totptoken=${totpToken},` + rdns.slice(1).join(',');

    const [error] = await safe(util.promisify(client.bind.bind(client))(totpTokenDn, password));
    client.unbind();
    if (error instanceof ldap.InvalidCredentialsError) throw new BoxError(BoxError.INVALID_CREDENTIALS);
    if (error) throw new BoxError(BoxError.EXTERNAL_ERROR, error);

    return translateUser(externalLdapConfig, ldapUsers[0]);
}

async function startSyncer() {
    const externalLdapConfig = await settings.getExternalLdapConfig();
    if (externalLdapConfig.provider === 'noop') throw new BoxError(BoxError.BAD_STATE, 'not enabled');

    const taskId = await tasks.add(tasks.TASK_SYNC_EXTERNAL_LDAP, []);

    tasks.startTask(taskId, {}, function (error, result) {
        debug('sync: done', error, result);
    });

    return taskId;
}

async function syncUsers(externalLdapConfig, progressCallback) {
    assert.strictEqual(typeof externalLdapConfig, 'object');
    assert.strictEqual(typeof progressCallback, 'function');

    const ldapUsers = await ldapUserSearch(externalLdapConfig, {});

    debug(`syncUsers: Found ${ldapUsers.length} users`);

    let percent = 10;
    let step = 30/(ldapUsers.length+1); // ensure no divide by 0

    // we ignore all errors here and just log them for now
    for (let i = 0; i < ldapUsers.length; i++) {
        let ldapUser = translateUser(externalLdapConfig, ldapUsers[i]);
        if (!validUserRequirements(ldapUser)) continue;

        percent += step;
        progressCallback({ percent, message: `Syncing... ${ldapUser.username}` });

        const user = await users.getByUsername(ldapUser.username);

        if (!user) {
            debug(`syncUsers: [adding user] username=${ldapUser.username} email=${ldapUser.email} displayName=${ldapUser.displayName}`);

            const [userAddError] = await safe(users.add(ldapUser.email, { username: ldapUser.username, password: null, displayName: ldapUser.displayName, source: 'ldap' }, AuditSource.EXTERNAL_LDAP_TASK));
            if (userAddError) debug('syncUsers: Failed to create user', ldapUser, userAddError);
        } else if (user.source !== 'ldap') {
            debug(`syncUsers: [mapping user] username=${ldapUser.username} email=${ldapUser.email} displayName=${ldapUser.displayName}`);

            const [userMappingError] = await safe(users.update(user, { email: ldapUser.email, fallbackEmail: ldapUser.email, displayName: ldapUser.displayName, source: 'ldap' }, AuditSource.EXTERNAL_LDAP_TASK));
            if (userMappingError) debug('Failed to map user', ldapUser, userMappingError);
        } else if (user.email !== ldapUser.email || user.displayName !== ldapUser.displayName) {
            debug(`syncUsers: [updating user] username=${ldapUser.username} email=${ldapUser.email} displayName=${ldapUser.displayName}`);

            const [userUpdateError] = await safe(users.update(user, { email: ldapUser.email, fallbackEmail: ldapUser.email, displayName: ldapUser.displayName }, AuditSource.EXTERNAL_LDAP_TASK));
            if (userUpdateError) debug('Failed to update user', ldapUser, userUpdateError);
        } else {
            // user known and up-to-date
            debug(`syncUsers: [up-to-date user] username=${ldapUser.username} email=${ldapUser.email} displayName=${ldapUser.displayName}`);
        }
    }

    debug('syncUsers: done');
}

async function syncGroups(externalLdapConfig, progressCallback) {
    assert.strictEqual(typeof externalLdapConfig, 'object');
    assert.strictEqual(typeof progressCallback, 'function');

    if (!externalLdapConfig.syncGroups) {
        debug('syncGroups: Group sync is disabled');
        progressCallback({ percent: 70, message: 'Skipping group sync...' });
        return [];
    }

    const ldapGroups = await ldapGroupSearch(externalLdapConfig, {});

    debug(`syncGroups: Found ${ldapGroups.length} groups`);

    let percent = 40;
    let step = 30/(ldapGroups.length+1); // ensure no divide by 0

    // we ignore all non internal errors here and just log them for now
    for (const ldapGroup of ldapGroups) {
        let groupName = ldapGroup[externalLdapConfig.groupnameField];
        if (!groupName) return;
        // some servers return empty array for unknown properties :-/
        if (typeof groupName !== 'string') return;

        // groups are lowercase
        groupName = groupName.toLowerCase();

        percent += step;
        progressCallback({ percent, message: `Syncing... ${groupName}` });

        const result = await groups.getByName(groupName);

        if (!result) {
            debug(`syncGroups: [adding group] groupname=${groupName}`);

            const [error] = await safe(groups.add({ name: groupName, source: 'ldap' }));
            if (error) debug('syncGroups: Failed to create group', groupName, error);
        } else {
            debug(`syncGroups: [up-to-date group] groupname=${groupName}`);
        }
    }

    debug('syncGroups: sync done');
}

async function syncGroupUsers(externalLdapConfig, progressCallback) {
    assert.strictEqual(typeof externalLdapConfig, 'object');
    assert.strictEqual(typeof progressCallback, 'function');

    if (!externalLdapConfig.syncGroups) {
        debug('syncGroupUsers: Group users sync is disabled');
        progressCallback({ percent: 99, message: 'Skipping group users sync...' });
        return [];
    }

    const allGroups = await groups.list();
    const ldapGroups = allGroups.filter(function (g) { return g.source === 'ldap'; });
    debug(`syncGroupUsers: Found ${ldapGroups.length} groups to sync users`);

    for (const group of ldapGroups) {
        debug(`syncGroupUsers: Sync users for group ${group.name}`);

        const result = await ldapGroupSearch(externalLdapConfig, {});
        if (!result || result.length === 0) {
            debug(`syncGroupUsers: Unable to find group ${group.name} ignoring for now.`);
            continue;
        }

        // since our group names are lowercase we cannot use potentially case matching ldap filters
        let found = result.find(function (r) {
            if (!r[externalLdapConfig.groupnameField]) return false;
            return r[externalLdapConfig.groupnameField].toLowerCase() === group.name;
        });

        if (!found) {
            debug(`syncGroupUsers: Unable to find group ${group.name} ignoring for now.`);
            continue;
        }

        let ldapGroupMembers = found.member || found.uniqueMember || [];

        // if only one entry is in the group ldap returns a string, not an array!
        if (typeof ldapGroupMembers === 'string') ldapGroupMembers = [ ldapGroupMembers ];

        debug(`syncGroupUsers: Group ${group.name} has ${ldapGroupMembers.length} members.`);

        for (const memberDn of ldapGroupMembers) {
            const [ldapError, result] = await safe(ldapGetByDN(externalLdapConfig, memberDn));
            if (ldapError) {
                debug(`syncGroupUsers: Failed to get ${memberDn}:`, ldapError);
                continue;
            }

            debug(`syncGroupUsers: Found member object at ${memberDn} adding to group ${group.name}`);

            const username = result[externalLdapConfig.usernameField].toLowerCase();
            if (!username) continue;

            const [getError, userObject] = await safe(users.getByUsername(username));
            if (getError || !userObject) {
                debug(`syncGroupUsers: Failed to get user by username ${username}`, getError ? getError : 'User not found');
                continue;
            }

            const [addError] = await safe(groups.addMember(group.id, userObject.id));
            if (addError && addError.reason !== BoxError.ALREADY_EXISTS) debug('syncGroupUsers: Failed to add member', addError);
        }
    }

    debug('syncGroupUsers: done');
}

async function sync(progressCallback) {
    assert.strictEqual(typeof progressCallback, 'function');

    progressCallback({ percent: 10, message: 'Starting ldap user sync' });

    const externalLdapConfig = await settings.getExternalLdapConfig();
    if (externalLdapConfig.provider === 'noop') throw new BoxError(BoxError.BAD_STATE, 'not enabled');

    await syncUsers(externalLdapConfig, progressCallback);
    await syncGroups(externalLdapConfig, progressCallback);
    await syncGroupUsers(externalLdapConfig, progressCallback);

    progressCallback({ percent: 100, message: 'Done' });

    debug('sync: done');
}
