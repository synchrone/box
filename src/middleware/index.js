'use strict';

exports = module.exports = {
    cookieParser: require('cookie-parser'),
    cors: require('./cors'),
    json: require('body-parser').json,
    morgan: require('morgan'),
    proxy: require('./proxy-middleware.js'),
    lastMile: require('connect-lastmile'),
    multipart: require('./multipart.js'),
    timeout: require('connect-timeout'),
    urlencoded: require('body-parser').urlencoded
};
