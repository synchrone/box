'use strict';

// -------------------------------------------
//  This file just describes the interface
//
//  New backends can start from here
// -------------------------------------------

exports = module.exports = {
    removePrivateFields,
    injectPrivateFields,
    upsert,
    get,
    del,
    wait,
    verifyDomainConfig
};

const assert = require('assert'),
    BoxError = require('../boxerror.js');

function removePrivateFields(domainObject) {
    // in-place removal of tokens and api keys with constants.SECRET_PLACEHOLDER
    return domainObject;
}

// eslint-disable-next-line no-unused-vars
function injectPrivateFields(newConfig, currentConfig) {
    // in-place injection of tokens and api keys which came in with constants.SECRET_PLACEHOLDER
}

async function upsert(domainObject, subdomain, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    // Result: none

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'upsert is not implemented');
}

async function get(domainObject, subdomain, type) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');

    // Result: Array of matching DNS records in string format

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'get is not implemented');
}

async function del(domainObject, subdomain, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    // Result: none

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'del is not implemented');
}

async function wait(domainObject, subdomain, type, value, options) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert.strictEqual(typeof value, 'string');
    assert(options && typeof options === 'object'); // { interval: 5000, times: 50000 }
}

async function verifyDomainConfig(domainObject) {
    assert.strictEqual(typeof domainObject, 'object');

    // Result: domainConfig object

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'verifyDomainConfig is not implemented');
}
