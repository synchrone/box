'use strict';

exports = module.exports = {
    removePrivateFields,
    injectPrivateFields,
    upsert,
    get,
    del,
    wait,
    verifyDomainConfig
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    constants = require('../constants.js'),
    debug = require('debug')('box:dns/gandi'),
    dig = require('../dig.js'),
    dns = require('../dns.js'),
    safe = require('safetydance'),
    superagent = require('superagent'),
    util = require('util'),
    waitForDns = require('./waitfordns.js');

const GANDI_API = 'https://dns.api.gandi.net/api/v5';

function formatError(response) {
    return util.format(`Gandi DNS error [${response.statusCode}] ${response.body.message}`);
}

function removePrivateFields(domainObject) {
    domainObject.config.token = constants.SECRET_PLACEHOLDER;
    return domainObject;
}

function injectPrivateFields(newConfig, currentConfig) {
    if (newConfig.token === constants.SECRET_PLACEHOLDER) newConfig.token = currentConfig.token;
}

async function upsert(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug(`upsert: ${name} in zone ${zoneName} of type ${type} with values ${JSON.stringify(values)}`);

    const data = {
        'rrset_ttl': 300, // this is the minimum allowed
        'rrset_values': values // for mx records, value is already of the '<priority> <server>' format
    };

    const [error, response] = await safe(superagent.put(`${GANDI_API}/domains/${zoneName}/records/${name}/${type}`)
        .set('X-Api-Key', domainConfig.token)
        .timeout(30 * 1000)
        .send(data)
        .ok(() => true));

    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error.message);
    if (response.statusCode === 403 || response.statusCode === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.statusCode === 400) throw new BoxError(BoxError.BAD_FIELD, formatError(response));
    if (response.statusCode !== 201) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
}

async function get(domainObject, location, type) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug(`get: ${name} in zone ${zoneName} of type ${type}`);

    const [error, response] = await safe(superagent.get(`${GANDI_API}/domains/${zoneName}/records/${name}/${type}`)
        .set('X-Api-Key', domainConfig.token)
        .timeout(30 * 1000)
        .ok(() => true));

    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error.message);
    if (response.statusCode === 403 || response.statusCode === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.statusCode === 404) return [];
    if (response.statusCode !== 200) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));

    return response.body.rrset_values;
}

async function del(domainObject, location, type, values) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof type, 'string');
    assert(Array.isArray(values));

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName,
        name = dns.getName(domainObject, location, type) || '@';

    debug(`del: ${name} in zone ${zoneName} of type ${type} with values ${JSON.stringify(values)}`);

    const [error, response] = await safe(superagent.del(`${GANDI_API}/domains/${zoneName}/records/${name}/${type}`)
        .set('X-Api-Key', domainConfig.token)
        .timeout(30 * 1000)
        .ok(() => true));

    if (error) throw new BoxError(BoxError.NETWORK_ERROR, error.message);
    if (response.statusCode === 404) return;
    if (response.statusCode === 403 || response.statusCode === 401) throw new BoxError(BoxError.ACCESS_DENIED, formatError(response));
    if (response.statusCode !== 204) throw new BoxError(BoxError.EXTERNAL_ERROR, formatError(response));
}

async function wait(domainObject, subdomain, type, value, options) {
    assert.strictEqual(typeof domainObject, 'object');
    assert.strictEqual(typeof subdomain, 'string');
    assert.strictEqual(typeof type, 'string');
    assert.strictEqual(typeof value, 'string');
    assert(options && typeof options === 'object'); // { interval: 5000, times: 50000 }

    const fqdn = dns.fqdn(subdomain, domainObject);

    await waitForDns(fqdn, domainObject.zoneName, type, value, options);
}

async function verifyDomainConfig(domainObject) {
    assert.strictEqual(typeof domainObject, 'object');

    const domainConfig = domainObject.config,
        zoneName = domainObject.zoneName;

    if (!domainConfig.token || typeof domainConfig.token !== 'string') throw new BoxError(BoxError.BAD_FIELD, 'token must be a non-empty string');

    const credentials = {
        token: domainConfig.token
    };

    const ip = '127.0.0.1';

    if (process.env.BOX_ENV === 'test') return credentials; // this shouldn't be here

    const [error, nameservers] = await safe(dig.resolve(zoneName, 'NS', { timeout: 5000 }));
    if (error && error.code === 'ENOTFOUND') throw new BoxError(BoxError.BAD_FIELD, 'Unable to resolve nameservers for this domain');
    if (error || !nameservers) throw new BoxError(BoxError.BAD_FIELD, error ? error.message : 'Unable to get nameservers');

    if (!nameservers.every(function (n) { return n.toLowerCase().indexOf('.gandi.net') !== -1; })) {
        debug('verifyDomainConfig: %j does not contain Gandi NS', nameservers);
        throw new BoxError(BoxError.BAD_FIELD, 'Domain nameservers are not set to Gandi');
    }

    const location = 'cloudrontestdns';

    await upsert(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record added');

    await del(domainObject, location, 'A', [ ip ]);
    debug('verifyDomainConfig: Test A record removed again');

    return credentials;
}
