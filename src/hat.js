'use strict';

exports = module.exports = hat;

const crypto = require('crypto');

function hat(bits) {
    return crypto.randomBytes(bits / 8).toString('hex');
}
