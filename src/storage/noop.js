'use strict';

exports = module.exports = {
    getBackupRootPath,
    getBackupProviderStatus,
    getAvailableSize,

    upload,
    exists,
    download,
    copy,

    listDir,

    remove,
    removeDir,

    remount,

    testConfig,
    removePrivateFields,
    injectPrivateFields
};

const assert = require('assert'),
    BoxError = require('../boxerror.js'),
    debug = require('debug')('box:storage/noop');

function getBackupRootPath(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');
    return '';
}

async function getBackupProviderStatus(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');

    return { state: 'active' };
}

async function getAvailableSize(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');

    return Number.POSITIVE_INFINITY;
}

function upload(apiConfig, backupFilePath, sourceStream, callback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof backupFilePath, 'string');
    assert.strictEqual(typeof sourceStream, 'object');
    assert.strictEqual(typeof callback, 'function');

    debug('upload: %s', backupFilePath);

    callback(null);
}

async function exists(apiConfig, backupFilePath) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof backupFilePath, 'string');

    debug(`exists: ${backupFilePath}`);

    return false;
}

function download(apiConfig, backupFilePath, callback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof backupFilePath, 'string');
    assert.strictEqual(typeof callback, 'function');

    debug('download: %s', backupFilePath);

    callback(new BoxError(BoxError.NOT_IMPLEMENTED, 'Cannot download from noop backend'));
}

function listDir(apiConfig, dir, batchSize, iteratorCallback, callback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof dir, 'string');
    assert.strictEqual(typeof batchSize, 'number');
    assert.strictEqual(typeof iteratorCallback, 'function');
    assert.strictEqual(typeof callback, 'function');

    callback();
}

async function copy(apiConfig, oldFilePath, newFilePath, progressCallback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof oldFilePath, 'string');
    assert.strictEqual(typeof newFilePath, 'string');
    assert.strictEqual(typeof progressCallback, 'function');

    debug(`copy: ${oldFilePath} -> ${newFilePath}`);
}

async function remove(apiConfig, filename) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof filename, 'string');

    debug(`remove: ${filename}`);
}

async function removeDir(apiConfig, pathPrefix, progressCallback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof pathPrefix, 'string');
    assert.strictEqual(typeof progressCallback, 'function');

    debug(`removeDir: ${pathPrefix}`);
}

async function remount(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');
}

async function testConfig(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');
}

function removePrivateFields(apiConfig) {
    return apiConfig;
}

// eslint-disable-next-line no-unused-vars
function injectPrivateFields(newConfig, currentConfig) {
}
