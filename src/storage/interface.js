'use strict';

// -------------------------------------------
//  This file just describes the interface
//
//  New backends can start from here
// -------------------------------------------

// Implementation note:
//     retry logic for upload() comes from the syncer since it is stream based
//     for the other API calls we leave it to the backend to retry. this allows
//     them to tune the concurrency based on failures/rate limits accordingly
exports = module.exports = {
    getBackupRootPath,
    getBackupProviderStatus,
    getAvailableSize,

    upload,

    exists,

    download,
    copy,

    listDir,

    remove,
    removeDir,

    remount,

    testConfig,
    removePrivateFields,
    injectPrivateFields
};

const assert = require('assert'),
    BoxError = require('../boxerror.js');

function removePrivateFields(apiConfig) {
    // in-place removal of tokens and api keys with constants.SECRET_PLACEHOLDER
    return apiConfig;
}

// eslint-disable-next-line no-unused-vars
function injectPrivateFields(newConfig, currentConfig) {
    // in-place injection of tokens and api keys which came in with constants.SECRET_PLACEHOLDER
}

function getBackupRootPath(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');

    // Result: path at the backup storage
    return '/';
}

async function getBackupProviderStatus(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');

    return { state: 'active' };
}

async function getAvailableSize(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');

    return Number.POSITIVE_INFINITY;
}

function upload(apiConfig, backupFilePath, sourceStream, callback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof backupFilePath, 'string');
    assert.strictEqual(typeof sourceStream, 'object');
    assert.strictEqual(typeof callback, 'function');

    // Result: none
    // sourceStream errors are handled upstream

    callback(new BoxError(BoxError.NOT_IMPLEMENTED, 'upload is not implemented'));
}

async function exists(apiConfig, backupFilePath) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof backupFilePath, 'string');

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'exists is not implemented');
}

function download(apiConfig, backupFilePath, callback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof backupFilePath, 'string');
    assert.strictEqual(typeof callback, 'function');

    // Result: download stream
    callback(new BoxError(BoxError.NOT_IMPLEMENTED, 'download is not implemented'));
}

async function copy(apiConfig, oldFilePath, newFilePath, progressCallback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof oldFilePath, 'string');
    assert.strictEqual(typeof newFilePath, 'string');
    assert.strictEqual(typeof progressCallback, 'function');

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'copy is not implemented');
}

function listDir(apiConfig, dir, batchSize, iteratorCallback, callback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof dir, 'string');
    assert.strictEqual(typeof batchSize, 'number');
    assert.strictEqual(typeof iteratorCallback, 'function');
    assert.strictEqual(typeof callback, 'function');

    callback(new BoxError(BoxError.NOT_IMPLEMENTED, 'listDir is not implemented'));
}

async function remove(apiConfig, filename) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof filename, 'string');

    // Result: none
    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'remove is not implemented');
}

async function removeDir(apiConfig, pathPrefix, progressCallback) {
    assert.strictEqual(typeof apiConfig, 'object');
    assert.strictEqual(typeof pathPrefix, 'string');
    assert.strictEqual(typeof progressCallback, 'function');

    // Result: none
    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'removeDir is not implemented');
}

async function remount(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');

    // Result: none

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'remount is not implemented');
}

async function testConfig(apiConfig) {
    assert.strictEqual(typeof apiConfig, 'object');

    // Result: none - first callback argument error if config does not pass the test

    throw new BoxError(BoxError.NOT_IMPLEMENTED, 'testConfig is not implemented');
}
