'use strict';

const constants = require('./constants.js'),
    path = require('path');

function baseDir() {
    const homeDir = process.env.HOME;
    if (constants.CLOUDRON) return homeDir;
    if (constants.TEST) return path.join(homeDir, '.cloudron_test');
    // cannot reach
}

// keep these values in sync with start.sh
exports = module.exports = {
    baseDir: baseDir,

    CLOUDRON_DEFAULT_AVATAR_FILE: path.join(__dirname + '/../assets/avatar.png'),
    INFRA_VERSION_FILE: path.join(baseDir(), 'platformdata/INFRA_VERSION'),
    CRON_SEED_FILE: path.join(baseDir(), 'platformdata/CRON_SEED'),
    DASHBOARD_DIR: constants.TEST ? path.join(__dirname, '../../dashboard/src') : path.join(baseDir(), 'box/dashboard/dist'),

    PROVIDER_FILE: '/etc/cloudron/PROVIDER',
    SETUP_TOKEN_FILE: '/etc/cloudron/SETUP_TOKEN',

    VOLUMES_MOUNT_DIR: '/mnt/volumes',
    MANAGED_BACKUP_MOUNT_DIR: '/mnt/cloudronbackup',

    PLATFORM_DATA_DIR: path.join(baseDir(), 'platformdata'),
    APPS_DATA_DIR: path.join(baseDir(), 'appsdata'),

    ACME_CHALLENGES_DIR: path.join(baseDir(), 'platformdata/acme'),
    ADDON_CONFIG_DIR: path.join(baseDir(), 'platformdata/addons'),
    MAIL_CONFIG_DIR: path.join(baseDir(), 'platformdata/addons/mail'),
    LOGROTATE_CONFIG_DIR: path.join(baseDir(), 'platformdata/logrotate.d'),
    NGINX_CONFIG_DIR: path.join(baseDir(), 'platformdata/nginx'),
    NGINX_APPCONFIG_DIR: path.join(baseDir(), 'platformdata/nginx/applications'),
    NGINX_CERT_DIR: path.join(baseDir(), 'platformdata/nginx/cert'),
    BACKUP_INFO_DIR: path.join(baseDir(), 'platformdata/backup'),
    UPDATE_DIR: path.join(baseDir(), 'platformdata/update'),
    UPDATE_CHECKER_FILE: path.join(baseDir(), 'platformdata/update/updatechecker.json'),
    DISK_USAGE_FILE: path.join(baseDir(), 'platformdata/diskusage.json'),
    SNAPSHOT_INFO_FILE: path.join(baseDir(), 'platformdata/backup/snapshot-info.json'),
    DYNDNS_INFO_FILE: path.join(baseDir(), 'platformdata/dyndns-info.json'),
    DHPARAMS_FILE: path.join(baseDir(), 'platformdata/dhparams.pem'),
    FEATURES_INFO_FILE: path.join(baseDir(), 'platformdata/features-info.json'),
    VERSION_FILE: path.join(baseDir(), 'platformdata/VERSION'),
    CIFS_CREDENTIALS_DIR: path.join(baseDir(), 'platformdata/cifs'),
    SSHFS_KEYS_DIR: path.join(baseDir(), 'platformdata/sshfs'),
    SFTP_KEYS_DIR: path.join(baseDir(), 'platformdata/sftp/ssh'),
    SFTP_PUBLIC_KEY_FILE: path.join(baseDir(), 'platformdata/sftp/ssh/ssh_host_rsa_key.pub'),
    SFTP_PRIVATE_KEY_FILE: path.join(baseDir(), 'platformdata/sftp/ssh/ssh_host_rsa_key'),
    FIREWALL_BLOCKLIST_FILE: path.join(baseDir(), 'platformdata/firewall/blocklist.txt'),
    LDAP_ALLOWLIST_FILE: path.join(baseDir(), 'platformdata/firewall/ldap_allowlist.txt'),

    BOX_DATA_DIR: path.join(baseDir(), 'boxdata/box'),
    MAIL_DATA_DIR: path.join(baseDir(), 'boxdata/mail'),

    LOG_DIR: path.join(baseDir(), 'platformdata/logs'),
    TASKS_LOG_DIR: path.join(baseDir(), 'platformdata/logs/tasks'),
    CRASH_LOG_DIR: path.join(baseDir(), 'platformdata/logs/crash'),
    BOX_LOG_FILE: path.join(baseDir(), 'platformdata/logs/box.log'),

    GHOST_USER_FILE: path.join(baseDir(), 'platformdata/cloudron_ghost.json'),

    SWAP_RATIO_FILE: path.join(baseDir(), 'platformdata/swap-ratio'),

    // this pattern is for the cloudron logs API route to work
    BACKUP_LOG_FILE: path.join(baseDir(), 'platformdata/logs/backup/app.log'),
    UPDATER_LOG_FILE: path.join(baseDir(), 'platformdata/logs/updater/app.log'),
};
