'use strict';

exports = module.exports = {
    disks,
    file
};

const assert = require('assert'),
    BoxError = require('./boxerror.js'),
    safe = require('safetydance');

function parseLine(line) {
    const parts = line.split(/\s+/, 7); // this way the mountpoint can have spaces in it

    return {
        filesystem: parts[0],
        type: parts[1],
        size: Number.parseInt(parts[2], 10),
        used: Number.parseInt(parts[3], 10),
        available: Number.parseInt(parts[4], 10),
        capacity: Number.parseInt(parts[5], 10) / 100, // note: this has a trailing %
        mountpoint: parts[6]
    };
}

async function disks() {
    const output = safe.child_process.execSync('df -B1 --output=source,fstype,size,used,avail,pcent,target', { encoding: 'utf8' });
    if (!output) throw new BoxError(BoxError.FS_ERROR, `Error running df: ${safe.error.message}`);

    const lines = output.trim().split('\n').slice(1); // discard header
    const result = [];
    for (const line of lines) {
        result.push(parseLine(line));
    }
    return result;
}

async function file(filename) {
    assert.strictEqual(typeof filename, 'string');

    const output = safe.child_process.execSync(`df -B1 --output=source,fstype,size,used,avail,pcent,target ${filename}`, { encoding: 'utf8' });
    if (!output) throw new BoxError(BoxError.FS_ERROR, `Error running df: ${safe.error.message}`);

    const lines = output.trim().split('\n').slice(1); // discard header
    return parseLine(lines[0]);
}
