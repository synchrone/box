'use strict';

class AuditSource {
    constructor(username, userId, ip) {
        this.username = username;
        this.userId = userId || null;
        this.ip = ip || null;
    }

    static fromRequest(req) {
        const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || null;
        return new AuditSource(req.user?.username, req.user?.id, ip);
    }
}

// these can be static variables but see https://stackoverflow.com/questions/60046847/eslint-does-not-allow-static-class-properties#comment122122927_60464446
AuditSource.CRON =  new AuditSource('cron');
AuditSource.HEALTH_MONITOR = new AuditSource('healthmonitor');
AuditSource.EXTERNAL_LDAP_TASK = new AuditSource('externalldap');
AuditSource.EXTERNAL_LDAP_AUTO_CREATE = new AuditSource('externalldap');
AuditSource.APPTASK = new AuditSource('apptask');
AuditSource.PLATFORM = new AuditSource('platform');

exports = module.exports = AuditSource;
