#!/usr/bin/env node

'use strict';

const database = require('./src/database.js');

const crashNotifier = require('./src/crashnotifier.js');

// This is triggered by systemd with the crashed unit name as argument
async function main() {
    if (process.argv.length !== 3) return console.error('Usage: crashnotifier.js <unitName>');

    const unitName = process.argv[2];
    console.log('Started crash notifier for', unitName);

    // eventlog api needs the db
    await database.initialize();

    await crashNotifier.sendFailureLogs(unitName);
}

main();
