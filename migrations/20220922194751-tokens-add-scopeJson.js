'use strict';

exports.up = async function (db) {
    await db.runSql('ALTER TABLE tokens DROP COLUMN scope');
    await db.runSql('ALTER TABLE tokens ADD COLUMN scopeJson TEXT');

    await db.runSql('UPDATE tokens SET scopeJson = ?', [ JSON.stringify({'*':'rw'})]);
};

exports.down = async function (db) {
    await db.runSql('ALTER TABLE tokens ADD COLUMN scope VARCHAR(512) NOT NULL DEFAULT ""');
    await db.runSql('ALTER TABLE tokens DROP COLUMN scopeJson');
};
