'use strict';

var async = require('async');

exports.up = function(db, callback) {

    // http://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address

    async.series([
        db.runSql.bind(db, 'ALTER TABLE appPasswords DROP INDEX name'),
        db.runSql.bind(db, 'ALTER TABLE appPasswords ADD CONSTRAINT appPasswords_name_userId_identifier UNIQUE (name, userId, identifier)'),
    ], callback);
};

exports.down = function(db, callback) {
    callback();
};
