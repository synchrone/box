'use strict';

const superagent = require('superagent');

exports.up = function(db, callback) {
    db.all('SELECT value FROM settings WHERE name="api_server_origin"', function (error, results) {
        if (error || results.length === 0) return callback(error);
        const apiServerOrigin = results[0].value;

        db.all('SELECT value FROM settings WHERE name="appstore_api_token"', function (error, results) {
            if (error || results.length === 0) return callback(error);
            const apiToken = results[0].value;

            console.log(`Getting appstore web token from ${apiServerOrigin}`);

            superagent.post(`${apiServerOrigin}/api/v1/user_token`)
                .send({})
                .query({ accessToken: apiToken })
                .timeout(30 * 1000).end(function (error, response) {
                    if (error && !error.response) {
                        console.log('Network error getting web token', error);
                        return callback();
                    }
                    if (response.statusCode !== 201 || !response.body.accessToken) {
                        console.log(`Bad status getting web token: ${response.status} ${response.text}`);
                        return callback();
                    }

                    db.runSql('INSERT settings (name, value) VALUES(?, ?)', [ 'appstore_web_token', response.body.accessToken ], callback);
                });
        });
    });
};

exports.down = function(db, callback) {
    callback();
};
