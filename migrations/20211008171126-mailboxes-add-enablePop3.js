'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE mailboxes ADD COLUMN enablePop3 BOOLEAN DEFAULT 0', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE mailboxes DROP COLUMN enablePop3', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

