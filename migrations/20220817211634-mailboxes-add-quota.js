'use strict';

const async = require('async');

exports.up = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'ALTER TABLE mailboxes ADD COLUMN storageQuota BIGINT DEFAULT 0'),
        db.runSql.bind(db, 'ALTER TABLE mailboxes ADD COLUMN messagesQuota BIGINT DEFAULT 0'),
    ], callback);
};

exports.down = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'ALTER TABLE mailboxes DROP COLUMN storageQuota'),
        db.runSql.bind(db, 'ALTER TABLE mailboxes DROP COLUMN messagesQuota')
    ], callback);
};
