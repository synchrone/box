'use strict';

exports.up = function(db, callback) {
    db.runSql('UPDATE settings SET name=? WHERE name=?', [ 'appstore_api_token', 'cloudron_token' ], callback);
};

exports.down = function(db, callback) {
  callback();
};
