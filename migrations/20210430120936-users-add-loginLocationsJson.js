'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE users ADD COLUMN loginLocationsJson TEXT', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE users DROP COLUMN loginLocationsJson', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
