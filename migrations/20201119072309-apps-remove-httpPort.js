'use strict';

var async = require('async');

exports.up = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'ALTER TABLE apps DROP COLUMN httpPort')
    ], callback);
};

exports.down = function(db, callback) {
    callback();
};
