'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE users ADD COLUMN ts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP', function (error) {
        if (error) console.error(error);

        db.runSql('ALTER TABLE users DROP COLUMN modifiedAt', callback);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE users DROP COLUMN ts', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
