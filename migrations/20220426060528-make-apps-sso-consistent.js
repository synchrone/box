'use strict';

const async = require('async');

exports.up = function(db, callback) {
    db.all('SELECT * FROM apps', function (error, apps) {
        if (error) return callback(error);

        async.eachSeries(apps, function (app, iteratorDone) {
            const manifest = JSON.parse(app.manifestJson);
            const hasSso = !!manifest.addons['proxyAuth'] || !!manifest.addons['ldap'];
            if (hasSso || !app.sso) return iteratorDone();

            console.log(`Unsetting sso flag of ${app.id}`);
            db.runSql('UPDATE apps SET sso=? WHERE id=?', [ 0, app.id ], iteratorDone);
        }, callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
