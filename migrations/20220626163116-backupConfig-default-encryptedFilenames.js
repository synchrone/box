'use strict';

exports.up = async function(db) {
    const result = await db.runSql('SELECT * FROM settings WHERE name=?', [ 'backup_config' ]);
    if (!result.length) return;

    const backupConfig = JSON.parse(result[0].value);

    if (backupConfig.encryption && backupConfig.format === 'rsync') backupConfig.encryptedFilenames = true;

    await db.runSql('UPDATE settings SET value=? WHERE name=?', [ JSON.stringify(backupConfig), 'backup_config',  ]);
};

exports.down = async function(/* db */) {
};
