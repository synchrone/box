'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE apps ADD COLUMN operatorsJson TEXT', callback);
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE apps DROP COLUMN operatorsJson', callback);
};
