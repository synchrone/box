'use strict';

const async = require('async'),
    fs = require('fs'),
    safe = require('safetydance');

const BOX_DATA_DIR = '/home/yellowtent/boxdata';
const PLATFORM_DATA_DIR = '/home/yellowtent/platformdata';

exports.up = function (db, callback) {
    if (!fs.existsSync(`${BOX_DATA_DIR}/firewall`)) return callback();

    const ports = safe.fs.readFileSync(`${BOX_DATA_DIR}/firewall/ports.json`);
    if (ports) {
        safe.fs.writeFileSync(`${PLATFORM_DATA_DIR}/firewall/ports.json`, ports);
    }

    const blocklist = safe.fs.readFileSync(`${BOX_DATA_DIR}/firewall/blocklist.txt`);
    async.series([
        (next) => {
            if (!blocklist) return next();
            db.runSql('INSERT INTO settings (name, valueBlob) VALUES (?, ?)', [ 'firewall_blocklist', blocklist ], next);
        },
        fs.writeFile.bind(fs, `${PLATFORM_DATA_DIR}/firewall/blocklist.txt`, blocklist || ''),
        fs.rmdir.bind(fs, `${BOX_DATA_DIR}/firewall`, { recursive: true })
    ], callback);
};

exports.down = function(db, callback) {
    callback();
};
