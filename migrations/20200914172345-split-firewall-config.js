'use strict';

const OLD_FIREWALL_CONFIG_JSON = '/home/yellowtent/boxdata/firewall-config.json';
const PORTS_FILE = '/home/yellowtent/boxdata/firewall/ports.json';
const BLOCKLIST_FILE = '/home/yellowtent/boxdata/firewall/blocklist.txt';

const fs = require('fs');

exports.up = function (db, callback) {
    if (!fs.existsSync(OLD_FIREWALL_CONFIG_JSON)) return callback();

    try {
        const dataJson = fs.readFileSync(OLD_FIREWALL_CONFIG_JSON, 'utf8');
        const data = JSON.parse(dataJson);
        fs.writeFileSync(BLOCKLIST_FILE, data.blocklist.join('\n') + '\n', 'utf8');
        fs.writeFileSync(PORTS_FILE, JSON.stringify({ allowed_tcp_ports: data.allowed_tcp_ports }, null, 4), 'utf8');
        fs.unlinkSync(OLD_FIREWALL_CONFIG_JSON);
    } catch (error) {
        console.log('Error migrating old firewall config', error);
    }

    callback();
};

exports.down = function (db, callback) {
    callback();
};
