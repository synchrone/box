'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE users ADD COLUMN inviteToken VARCHAR(128) DEFAULT ""', callback);
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE users DROP COLUMN inviteToken', callback);
};
