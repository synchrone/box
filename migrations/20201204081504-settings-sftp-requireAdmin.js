'use strict';

exports.up = function(db, callback) {
    db.all('SELECT * FROM settings WHERE name=?', ['platform_config'], function (error, results) {
        let value;
        if (error || results.length === 0) {
            value = { sftp: { requireAdmin: true } };
        } else {
            value = JSON.parse(results[0].value);
            if (!value.sftp) value.sftp = {};
            value.sftp.requireAdmin = true;
        }

        // existing installations may not even have the key. so use REPLACE instead of UPDATE
        db.runSql('REPLACE INTO settings (name, value) VALUES (?, ?)', [ 'platform_config', JSON.stringify(value) ], callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
