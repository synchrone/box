'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE backups ADD COLUMN label VARCHAR(128) DEFAULT ""', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE backups DROP COLUMN label', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

