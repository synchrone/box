'use strict';

exports.up = function(db, callback) {
    db.runSql('RENAME TABLE subdomains TO locations', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    callback();
};
