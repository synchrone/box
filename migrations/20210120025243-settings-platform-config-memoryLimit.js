'use strict';

exports.up = function(db, callback) {
    db.all('SELECT * FROM settings WHERE name=?', ['platform_config'], function (error, results) {
        if (error || results.length === 0) return callback(null);

        let value = JSON.parse(results[0].value);

        for (const serviceName of Object.keys(value)) {
            const service = value[serviceName];
            if (!service.memorySwap) continue;
            service.memoryLimit = service.memorySwap;
            delete service.memorySwap;
            delete service.memory;
        }

        db.runSql('UPDATE settings SET value=? WHERE name=?', [ JSON.stringify(value), 'platform_config' ], callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
