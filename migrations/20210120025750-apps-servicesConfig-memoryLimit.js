'use strict';

const async = require('async');

exports.up = function(db, callback) {
    db.all('SELECT * FROM apps', function (error, apps) {
        if (error) return callback(error);

        async.eachSeries(apps, function (app, iteratorDone) {
            if (!app.servicesConfigJson) return iteratorDone();

            let servicesConfig = JSON.parse(app.servicesConfigJson);
            for (const serviceName of Object.keys(servicesConfig)) {
                const service = servicesConfig[serviceName];
                if (!service.memorySwap) continue;
                service.memoryLimit = service.memorySwap;
                delete service.memorySwap;
                delete service.memory;
            }

            db.runSql('UPDATE apps SET servicesConfigJson=? WHERE id=?', [ JSON.stringify(servicesConfig), app.id ], iteratorDone);
        }, callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
