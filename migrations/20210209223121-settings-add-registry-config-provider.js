'use strict';

exports.up = function(db, callback) {
    db.all('SELECT value FROM settings WHERE name="registry_config"', function (error, results) {
        if (error || results.length === 0) return callback(error);

        var registryConfig = JSON.parse(results[0].value);
        if (!registryConfig.provider) registryConfig.provider = 'other';

        db.runSql('UPDATE settings SET value=? WHERE name="registry_config"', [ JSON.stringify(registryConfig) ], callback);
    });
};

exports.down = function(db, callback) {
    callback();
};
