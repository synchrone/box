'use strict';

var async = require('async');

exports.up = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'ALTER TABLE backups ADD INDEX creationTime_index (creationTime)'),
        db.runSql.bind(db, 'ALTER TABLE eventlog ADD INDEX creationTime_index (creationTime)'),
        db.runSql.bind(db, 'ALTER TABLE notifications ADD INDEX creationTime_index (creationTime)'),
        db.runSql.bind(db, 'ALTER TABLE tasks ADD INDEX creationTime_index (creationTime)'),
    ], callback);
};

exports.down = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'ALTER TABLE backups DROP INDEX creationTime_index'),
        db.runSql.bind(db, 'ALTER TABLE eventlog DROP INDEX creationTime_index'),
        db.runSql.bind(db, 'ALTER TABLE notifications DROP INDEX creationTime_index'),
        db.runSql.bind(db, 'ALTER TABLE tasks DROP INDEX creationTime_index'),
    ], callback);
};
