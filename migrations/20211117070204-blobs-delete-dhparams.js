'use strict';

exports.up = function(db, callback) {
    db.runSql('DELETE FROM blobs WHERE id=?', [ 'dhparams' ], callback);
};

exports.down = function(db, callback) {
    callback();
};
