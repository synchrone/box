'use strict';

var async = require('async');

exports.up = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'ALTER TABLE apps ADD COLUMN enableInbox BOOLEAN DEFAULT 0'),
        db.runSql.bind(db, 'ALTER TABLE apps ADD COLUMN inboxName VARCHAR(128)'),
        db.runSql.bind(db, 'ALTER TABLE apps ADD COLUMN inboxDomain VARCHAR(128)'),
    ], callback);
};

exports.down = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'ALTER TABLE apps DROP COLUMN enableInbox'),
        db.runSql.bind(db, 'ALTER TABLE apps DROP COLUMN inboxName'),
        db.runSql.bind(db, 'ALTER TABLE apps DROP COLUMN inboxDomain'),
    ], callback);
};
