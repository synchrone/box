'use strict';

const async = require('async');

exports.up = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'ALTER TABLE eventlog CHANGE source sourceJson TEXT', []),
        db.runSql.bind(db, 'ALTER TABLE eventlog CHANGE data dataJson TEXT', []),
    ], callback);
};

exports.down = function(db, callback) {
    async.series([
        db.runSql.bind(db, 'ALTER TABLE eventlog CHANGE sourceJson source TEXT', []),
        db.runSql.bind(db, 'ALTER TABLE eventlog CHANGE dataJson data TEXT', []),
    ], callback);
};
