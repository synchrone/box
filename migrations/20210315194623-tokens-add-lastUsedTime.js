'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE tokens ADD COLUMN lastUsedTime TIMESTAMP NULL', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE tokens DROP COLUMN lastUsedTime', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
