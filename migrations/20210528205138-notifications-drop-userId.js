'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE notifications DROP COLUMN userId', function (error) {
        if (error) return callback(error);

        db.runSql('DELETE FROM notifications', callback); // just clear notifications table
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE notifications ADD COLUMN userId VARCHAR(128) NOT NULL', callback);
};
